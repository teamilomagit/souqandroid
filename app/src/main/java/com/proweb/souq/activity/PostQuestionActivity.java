package com.proweb.souq.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

public class PostQuestionActivity extends BaseActivity {

    Context context;
    EditText edtUsername, edtEmail, edtQuestion;
    TextView txtQuestionSubmit;
    CustomProgressBar customProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_question);
        init();
    }

    private void init() {
        context = this;
        edtUsername = findViewById(R.id.edt_username);
        edtEmail = findViewById(R.id.edt_email);
        edtQuestion = findViewById(R.id.edt_question);
        txtQuestionSubmit = findViewById(R.id.txt_question_submit);
        customProgressBar = new CustomProgressBar(context);

        //Method calling
        setListeners();
        setToolBarTitleWithBack("");
    }

    private void setListeners() {
        txtQuestionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    postQuestionAPI();
                }
            }
        });
    }

    private boolean isValid() {
        if (edtUsername.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_username), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtEmail.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_email_address), Toast.LENGTH_SHORT).show();
            return false;
        }else if (!AppUtils.isValidEmail(edtEmail.getText().toString())){
            Toast.makeText(context, getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtQuestion.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_type_question), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void postQuestionAPI(){
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("name",edtUsername.getText().toString());
            params.put("email",edtEmail.getText().toString());
            params.put("enquiry",edtQuestion.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.POST_QUESTION_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                Toast.makeText(context,getString(R.string.msg_post_question_success), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                Log.e("TAG",error);
            }
        });
    }
}
