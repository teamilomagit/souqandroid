package com.proweb.souq.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassActivity extends BaseActivity {
    Context context;
     private TextView txtSubmit;
     private EditText edtEmail;
     private ProgressBar progressBar;
     View viewLine;
     ImageView imgSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        init();
    }

    private void init() {
        context = this;
        txtSubmit = findViewById(R.id.txt_submit);
        edtEmail = findViewById(R.id.edt_email);
        progressBar = findViewById(R.id.prgress_bar);
        viewLine = findViewById(R.id.view_line);
        imgSuccess = findViewById(R.id.img_success);

        setToolBarTitleWithBack(getString(R.string.tittle_forgot_password));
        setClickListener();
    }

    private void setClickListener() {
        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
                    forgotPassAPI();
                }
            }
        });
    }

    /**
     * FUNCTIONALITY
     */
    private boolean validate(){
        if (edtEmail.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_registered_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if (!AppUtils.isValidEmail(edtEmail.getText().toString())){
            Toast.makeText(context, getResources().getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    /**
     * API CALLING
     */
    private void forgotPassAPI() {
        progressBar.setVisibility(View.VISIBLE);
        //customizeScreen();
        JSONObject params = new JSONObject();
        try {
            params.put("email", edtEmail.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.FORGOT_PASS_API, params, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Toast.makeText(context, getString(R.string.msg_forgot_pass), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
