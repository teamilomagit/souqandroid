package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.dialog.AreaDialog;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.AreaModel;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.CustomFieldsModel;
import com.proweb.souq.model.GuestModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomEditText;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

public class GuestDetailsActivity extends BaseActivity {

    private Context context;
    private CustomEditText cedtFirstname, cedtLastname, cedtCity, cedtAddressLine1, cedtAddressLine2, cedtPostcode, cedtEmail, cedtMobile;
    private CustomEditText cedtDistrict, cedtStreetNo, cedtHouseNo;
    private TextView spinner, spinnerZone, txtContinue;
    private RadioButton radioButton1, radioButton2;
    private String country_id = "";
    private String zone_id = "";
    private String zone = "";
    private RelativeLayout rlSpinner, rlSpinnerZone;
    private AreaDialog areaDialog;

    private CustomProgressBar customProgressBar;
    private CartModel cartModel;
    private GuestModel guestModel;
    private String shippingAddress;

    private String selectedGender = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_details);

        init();
    }

    private void init() {
        context = this;
        cedtFirstname = findViewById(R.id.cedt_firstname);
        cedtLastname = findViewById(R.id.cedt_lastname);
        cedtCity = findViewById(R.id.cedt_city);
        cedtAddressLine1 = findViewById(R.id.cedt_address_line1);
        cedtAddressLine2 = findViewById(R.id.cedt_address_line2);
        cedtPostcode = findViewById(R.id.cedt_postcode);
        cedtDistrict = findViewById(R.id.cedt_district);
        cedtStreetNo = findViewById(R.id.cedt_street_no);
        cedtHouseNo = findViewById(R.id.cedt_house_no);
        cedtEmail = findViewById(R.id.cedt_email);
        cedtMobile = findViewById(R.id.cedt_mobile);
        spinner = findViewById(R.id.spinner);
        spinnerZone = findViewById(R.id.spinner_zone);
        radioButton1 = findViewById(R.id.radio1);
        radioButton2 = findViewById(R.id.radio2);
        rlSpinner = findViewById(R.id.rl_spinner);
        rlSpinnerZone = findViewById(R.id.rl_spinner_zone);
        txtContinue = findViewById(R.id.txt_continue);

        cedtPostcode.setInputType(InputType.TYPE_CLASS_NUMBER);
        cedtEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        cedtMobile.setInputType(InputType.TYPE_CLASS_PHONE);
        cedtMobile.setMaxCharacters(10);

        customProgressBar = new CustomProgressBar(context);
        cartModel = new Gson().fromJson(getIntent().getStringExtra("cart_model"), CartModel.class);
        guestModel = new AppPreference(context).getGuestModel();

        //Method calling
        setToolBarTitleWithBack("");
        setListeners();

        if (guestModel != null) {
            setData();
        }
    }

    private void setListeners() {

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender = "1";
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender = "2";
            }
        });

        rlSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryDialog();
            }
        });

        rlSpinnerZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showZoneDialog();
            }
        });

        txtContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    GuestModel guestModel = new GuestModel();
                    guestModel.setFirstname(cedtFirstname.getText());
                    guestModel.setLastname(cedtLastname.getText());
                    guestModel.setEmail(cedtEmail.getText());
                    guestModel.setTelephone(cedtMobile.getText());

                    guestModel.setCountry_id(country_id);
                    guestModel.setCountry_name(spinner.getText().toString());
                    guestModel.setZone_id(zone_id);
                    guestModel.setZone_name(spinnerZone.getText().toString());
                    guestModel.setCity(cedtCity.getText());
                    guestModel.setAddress_1(cedtAddressLine1.getText());
                    guestModel.setAddress_2(cedtAddressLine2.getText());
                    guestModel.setPostcode(cedtPostcode.getText());
                    guestModel.setCompany("string");
                    guestModel.setPostcode(cedtPostcode.getText());

                    CustomFieldsModel customFieldsModel = new CustomFieldsModel();
                    customFieldsModel.setDistrict(cedtDistrict.getText());
                    customFieldsModel.setStreet(cedtStreetNo.getText());
                    customFieldsModel.setHouse(cedtHouseNo.getText());

                    guestModel.setCustom_field(customFieldsModel);
                    guestModel.setPostcode(cedtPostcode.getText());
                    guestModel.setPostcode(cedtPostcode.getText());
                    guestModel.setSelectedGender(selectedGender);

                    shippingAddress = guestModel.getAddress_1() + " " + guestModel.getAddress_2();

                    createGuestUserAPI(guestModel);
                }

            }
        });

    }

    /**
     * FUNCTIONALITY
     */
    private void showCountryDialog() {
        areaDialog = new AreaDialog(context, 0, getString(R.string.lbl_select_country), new AreaDialog.AreaDialogInterface() {
            @Override
            public void onItemSelected(AreaModel areaModel) {
                country_id = areaModel.getId();
                spinner.setText(areaModel.getName());
                showZoneDialog();
            }
        });
        areaDialog.setCancelable(false);
        areaDialog.show();
    }

    private void showZoneDialog() {

        if (country_id.equalsIgnoreCase("")) {
            Toast.makeText(context, getString(R.string.err_select_country), Toast.LENGTH_SHORT).show();
            return;
        }

        areaDialog = new AreaDialog(context, Integer.parseInt(country_id), getString(R.string.lbl_select_zone), new AreaDialog.AreaDialogInterface() {
            @Override
            public void onItemSelected(AreaModel areaModel) {
                zone_id = areaModel.getId();
                zone = areaModel.getName();
                spinnerZone.setText(areaModel.getName());
            }
        });
        areaDialog.setCancelable(false);
        areaDialog.show();
    }

    private void setData() {
        country_id = guestModel.getCountry_id();
        zone_id = guestModel.getZone_id();
        zone = guestModel.getZone_name();
        cedtFirstname.setText(guestModel.getFirstname());
        cedtLastname.setText(guestModel.getLastname());
        cedtCity.setText(guestModel.getCity());
        cedtAddressLine1.setText(guestModel.getAddress_1());
        cedtAddressLine2.setText(guestModel.getAddress_2());
        cedtPostcode.setText(guestModel.getPostcode());
        cedtDistrict.setText(guestModel.getCustom_field().getDistrict());
        cedtStreetNo.setText(guestModel.getCustom_field().getStreet());
        cedtHouseNo.setText(guestModel.getCustom_field().getHouse());
        spinner.setText(guestModel.getCountry_name());
        spinnerZone.setText(guestModel.getZone_name());

        if (guestModel.getSelectedGender().equalsIgnoreCase("2")) {
            radioButton2.setChecked(true);
        } else {
            radioButton2.setChecked(false);
        }
    }

    public boolean isValid() {

        if (cedtFirstname.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_firstname), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtFirstname.getText().length() > 32) {
            Toast.makeText(context, getString(R.string.err_firstname_length), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtLastname.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_lastname), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtLastname.getText().toString().length() > 32) {
            Toast.makeText(context, getString(R.string.err_lastname_length), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (cedtEmail.getText().isEmpty()) {
            Toast.makeText(context, getResources().getString(R.string.err_email_address), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!AppUtils.isValidEmail(cedtEmail.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (cedtMobile.getText().length() < 10) {
            Toast.makeText(context, getResources().getString(R.string.err_valid_mobile), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spinner.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_country), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spinnerZone.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_zone), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtCity.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_city), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtDistrict.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_district), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtStreetNo.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_street_no), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtHouseNo.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_house_no), Toast.LENGTH_SHORT).show();
            return false;
        } else if (cedtAddressLine1.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_area), Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (cedtAddressLine2.getText().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_area), Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }


    /**
     * API CALLING
     **/

    private void createGuestUserAPI(final GuestModel guestModel) {
        customProgressBar.show();

        JSONObject customField = new JSONObject();
        JSONObject account = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        JSONObject paramCustom = new JSONObject();
        try {
            paramCustom.put("2", cedtDistrict.getText()); //District
            paramCustom.put("3", zone); //Area
            paramCustom.put("4", cedtStreetNo.getText()); //Street No
            paramCustom.put("5", cedtHouseNo.getText()); //House No
            paramCustom.put("6", "Nearest Landmark");
            //paramCustom.put("8",selectedDefaultOption);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            account.put("1", selectedGender);
            customField.put("account", account);
            customField.put("address",paramCustom);

            jsonObject.put("firstname", guestModel.getFirstname());
            jsonObject.put("lastname", guestModel.getLastname());
            jsonObject.put("email", guestModel.getEmail());
            jsonObject.put("telephone", guestModel.getTelephone());
            jsonObject.put("company", guestModel.getCompany());
            jsonObject.put("city", guestModel.getCity());
            jsonObject.put("address_1", guestModel.getAddress_1());
            jsonObject.put("address_2", guestModel.getAddress_2());
            jsonObject.put("country_id", guestModel.getCountry_id());
            jsonObject.put("zone_id", guestModel.getZone_id());
            jsonObject.put("postcode", guestModel.getPostcode());
            jsonObject.put("custom_field", customField);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.CREATE_GUEST_API, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                createGuestUserShippingAPI(guestModel);
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void createGuestUserShippingAPI(GuestModel guestModel) {
        customProgressBar.show();
        JSONObject jsonObject = new JSONObject();
        JSONObject customField = new JSONObject();
        JSONObject account = new JSONObject();

        JSONObject paramCustom = new JSONObject();
        try {
            paramCustom.put("2", cedtDistrict.getText()); //District
            paramCustom.put("3", zone); //Area
            paramCustom.put("4", cedtStreetNo.getText()); //Street No
            paramCustom.put("5", cedtHouseNo.getText()); //House No
            paramCustom.put("6", "Nearest Landmark");
            //paramCustom.put("8",selectedDefaultOption);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            account.put("1", selectedGender);
            customField.put("account", account);
            customField.put("address",paramCustom);

            jsonObject.put("firstname", guestModel.getFirstname());
            jsonObject.put("lastname", guestModel.getLastname());
            jsonObject.put("email", guestModel.getEmail());
            jsonObject.put("telephone", guestModel.getTelephone());
            jsonObject.put("company", guestModel.getCompany());
            jsonObject.put("city", guestModel.getCity());
            jsonObject.put("address_1", guestModel.getAddress_1());
            jsonObject.put("address_2", guestModel.getAddress_2());
            jsonObject.put("country_id", guestModel.getCountry_id());
            jsonObject.put("zone_id", guestModel.getZone_id());
            jsonObject.put("postcode", guestModel.getPostcode());
            jsonObject.put("custom_field", customField);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.GUEST_SHIPPING_API, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                startActivity(new Intent(context, OrderReviewActivity.class).putExtra("cart_model", new Gson().toJson(cartModel))
                        .putExtra("ship_address", shippingAddress));
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
