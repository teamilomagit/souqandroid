package com.proweb.souq.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.ViewCartAdapter;
import com.proweb.souq.dialog.CancelOrderDialog;
import com.proweb.souq.model.OrderDetailModel;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.OrderStatusBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderTrackActivity extends BaseActivity {

    Context context;
    private Button btnCancelOrder;
    TextView txtOrderNo, txtOrderDate, txtShipAddress, txtChangeAddress, txtPhoneNumber, txtEdit, txtManufacturer, txtModel, txtPrice, txtPaymentMethod;
    TextView txtSubtotal, txtShippingFee, txtCod, txtTotal;
    OrderHistoryModel orderHistoryModel;
    OrderDetailModel orderDetailModel;
    RecyclerView mRecyclerView;
    ViewCartAdapter mAdapter;
    ArrayList<ProductModel> list;
    CustomProgressBar progressBar;
    OrderStatusBar orderStatusBar;
    TextView txtCouponValue, txtCouponLabel;
    RelativeLayout llCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_track);
        init();
    }

    private void init() {
        context = this;
        btnCancelOrder = findViewById(R.id.btn_cancel_order);
        txtOrderNo = findViewById(R.id.txt_order_no);
        txtOrderDate = findViewById(R.id.txt_order_date);
        txtShipAddress = findViewById(R.id.txt_ship_address);
        txtChangeAddress = findViewById(R.id.txt_change_address);
        txtPhoneNumber = findViewById(R.id.txt_phone_number);
        txtEdit = findViewById(R.id.txt_edit);
        txtManufacturer = findViewById(R.id.txt_manufacturer);
        txtModel = findViewById(R.id.txt_model);
        txtPrice = findViewById(R.id.txt_price);
        txtPaymentMethod = findViewById(R.id.txt_payment_method);
        txtSubtotal = findViewById(R.id.txt_subtotal);
        txtShippingFee = findViewById(R.id.txt_shipping_fee);
        txtCod = findViewById(R.id.txt_cod);
        txtTotal = findViewById(R.id.txt_total);
        mRecyclerView = findViewById(R.id.recycler_view);
        orderStatusBar = findViewById(R.id.order_status_no);
        txtCouponValue = findViewById(R.id.txt_coupon_value);
        txtCouponLabel = findViewById(R.id.txt_coupon_label);
        llCoupon = findViewById(R.id.ll_coupon);
        list = new ArrayList<>();
        progressBar = new CustomProgressBar(context);
        orderHistoryModel = new Gson().fromJson(getIntent().getStringExtra("order_history_model"), OrderHistoryModel.class);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_order_track));
        setListeners();
        getOrderDetail();
        setUpRecyclerview();
    }

    private void setUpRecyclerview() {
        mAdapter = new ViewCartAdapter(context, list, false, new ViewCartAdapter.ViewCartAdapterInterface() {
            @Override
            public void getDeleteSuccess() {
            }

            @Override
            public void onItemSelected(String item, int position) {
            }

            @Override
            public void setItemQuantity(TextView itemView, int position) {
            }

            @Override
            public void isAnyItemOutOfStock() {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
    }

    private void setData() {
        //0 - txtSubtotal, shipping, coupon, total
        //0 - txtSubtotal, shipping, total
        if (orderDetailModel != null) {
            txtOrderNo.setText(getString(R.string.txt_order) + " " + orderDetailModel.getOrder_id());
            txtOrderDate.setText(getString(R.string.txt_placed_on) + " " + AppUtils.getFormattedDate(orderDetailModel.getDate_added()));
            txtShipAddress.setText(orderDetailModel.getShipping_address_1() + " " + orderDetailModel.getShipping_address_2() + "-" + orderDetailModel.getShipping_postcode());
            txtPhoneNumber.setText(orderDetailModel.getTelephone());
            if (orderDetailModel.getOrder_status_id().equalsIgnoreCase("1")) {
                orderStatusBar.setProgress(25);
            } else if (orderDetailModel.getOrder_status_id().equalsIgnoreCase("2")) {
                orderStatusBar.setProgress(50);
            } else if (orderDetailModel.getOrder_status_id().equalsIgnoreCase("3")) {
                orderStatusBar.setProgress(75);
            } else if (orderDetailModel.getOrder_status_id().equalsIgnoreCase("5")) {
                orderStatusBar.setProgress(100);
            }
            txtPaymentMethod.setText(orderDetailModel.getPayment_method());
            if (orderDetailModel.getTotals().size() >= 4) {
                llCoupon.setVisibility(View.VISIBLE);
                txtSubtotal.setText("$" + " " + orderDetailModel.getTotals().get(0).getValue());
                txtShippingFee.setText("$" + " " + orderDetailModel.getTotals().get(1).getValue());
                txtCouponValue.setText("$" + " " + orderDetailModel.getTotals().get(2).getValue());
                txtCouponLabel.setText(orderDetailModel.getTotals().get(2).getTitle());
                txtTotal.setText("$" + " " + orderDetailModel.getTotals().get(3).getValue());
            }else {
                txtSubtotal.setText("$" + " " + orderDetailModel.getTotals().get(0).getValue());
                txtShippingFee.setText("$" + " " + orderDetailModel.getTotals().get(1).getValue());
                llCoupon.setVisibility(View.GONE);
                txtTotal.setText("$" + " " + orderDetailModel.getTotals().get(2).getValue());
            }
        }
    }

    private void setListeners() {

        btnCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelOrderDialog cancelOrderDialog = new CancelOrderDialog(context, orderDetailModel.getOrder_id(), new CancelOrderDialog.CancelOrderDialogInterface() {
                    @Override
                    public void clickedCancel(String comment) {
                        cancelOrderAPI(comment);
                    }
                });
                cancelOrderDialog.show();
            }
        });

    }

    /**
     * API CALLING
     */
    private void getOrderDetail() {
        progressBar.show();
        String url = AppConstant.GET_ORDERS_LIST_API + orderHistoryModel.getOrder_id();
        new APIManager().getAPI(url, OrderDetailModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressBar.dismiss();
                orderDetailModel = (OrderDetailModel) resultObj;
                list = orderDetailModel.getProducts();
                mAdapter.updateAdapter(list);
                setData();
            }

            @Override
            public void onError(String error) {
                progressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }


    private void cancelOrderAPI(String comment) {
        progressBar.show();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("order_status_id", orderDetailModel.getOrder_id());
            jsonObject.put("notify", 1);
            jsonObject.put("comment", comment);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = AppConstant.CANCEL_ORDER_API + "/" + orderDetailModel.getOrder_id();

        new APIManager().putAPI(url, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressBar.dismiss();
                AppConstant.SHOULD_RELOAD = true;
                finish();
            }

            @Override
            public void onError(String error) {
                progressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
