package com.proweb.souq.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.adapter.SortAdapter;
import com.proweb.souq.model.FilterModel;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;

public class SortItemsActivity extends BaseActivity {
    private Context context;
    private Button btnApply;
    private TextView txtSortName;
    private ArrayList<FilterModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_items);

        init();
    }


    private void init() {
        context = this;
        btnApply = findViewById(R.id.btn_apply);
        txtSortName = findViewById(R.id.txt_sort_name);
        list = new ArrayList<>();

        setToolBarTitleWithBack("");
        setListeners();
        setSortData();
        setUpRecyclerView();
    }

    private void setListeners()
    {
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstant.selectedSort == null) {
                    AppConstant.selectedSort = list.get(0);
//                    AppConstant.selectedSort = new FilterModel();
                }

                Intent data = new Intent();
                setResult(Activity.RESULT_OK,data);
                finish();
            }
        });
    }

    private void setSortData() {
        FilterModel filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name1));
        filterModel.setSort_type(getString(R.string.type1));
        filterModel.setSort_order(getString(R.string.order_asc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name2));
        filterModel.setSort_type(getString(R.string.type2));
        filterModel.setSort_order(getString(R.string.order_asc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name3));
        filterModel.setSort_type(getString(R.string.type3));
        filterModel.setSort_order(getString(R.string.order_desc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name4));
        filterModel.setSort_type(getString(R.string.type_price));
        filterModel.setSort_order(getString(R.string.order_asc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name5));
        filterModel.setSort_type(getString(R.string.type_price));
        filterModel.setSort_order(getString(R.string.order_desc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name6));
        filterModel.setSort_type(getString(R.string.type_rating));
        filterModel.setSort_order(getString(R.string.order_desc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name7));
        filterModel.setSort_type(getString(R.string.type_rating));
        filterModel.setSort_order(getString(R.string.order_asc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name8));
        filterModel.setSort_type(getString(R.string.type_model));
        filterModel.setSort_order(getString(R.string.order_asc));
        list.add(filterModel);

        filterModel = new FilterModel();
        filterModel.setName(getString(R.string.name9));
        filterModel.setSort_type(getString(R.string.type_model));
        filterModel.setSort_order(getString(R.string.order_desc));
        list.add(filterModel);

        if (AppConstant.selectedSort != null) {
            for (FilterModel filterModel1 : list) {
                if (filterModel1.getName().equalsIgnoreCase(AppConstant.selectedSort.getName())) {
                    filterModel1.setChecked(true);
                    txtSortName.setText(filterModel1.getName());
                    break;
                }
            }
        } else {
            list.get(0).setChecked(true);
        }

    }


    private void setUpRecyclerView() {
        SortAdapter mAdapter = new SortAdapter(context, list, new SortAdapter.SortAdapterInterface() {
            @Override
            public void onClicked(FilterModel filterModel) {

            }
        });
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }
}
