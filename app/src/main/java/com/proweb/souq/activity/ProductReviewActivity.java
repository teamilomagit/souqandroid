package com.proweb.souq.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.ReviewAdapter;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.model.ReviewDetailModel;

public class ProductReviewActivity extends BaseActivity {

    private Context context;
    private RecyclerView mRecyclerView;
    private ReviewAdapter mAdapter;

    private ProductModel productModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_review);

        init();
    }

    private void init() {
        context = this;
        productModel = new Gson().fromJson(getIntent().getStringExtra("product_model"), ProductModel.class);

        setToolBarTitleWithBack("");
        setRecyclerView();
    }

    private void setRecyclerView() {
        mRecyclerView = findViewById(R.id.recycler_view);
        mAdapter = new ReviewAdapter(context, productModel.getReviews().getReviews());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }
}
