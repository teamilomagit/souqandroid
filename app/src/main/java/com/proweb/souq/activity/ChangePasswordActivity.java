package com.proweb.souq.activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.proweb.souq.R;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends BaseActivity {

    Context context;
    TextView txtEdit, txtChangePassword;
    EditText edtNewPassword, edtCurrentPassword;
    CustomProgressBar customProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
    }

    private void init() {
        context = this;
        txtEdit = findViewById(R.id.txt_edit);
        edtNewPassword = findViewById(R.id.edt_new_password);
        edtCurrentPassword = findViewById(R.id.edt_current_password);
        txtChangePassword = findViewById(R.id.txt_change_password);
        customProgressBar = new CustomProgressBar(context);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_change_password));
        callHeader(context);
        setListeners();
    }

    private void setListeners() {
        txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtCurrentPassword.setEnabled(true);
                edtNewPassword.setEnabled(true);
            }
        });

        txtChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    changePasswordAPI();
                }
            }
        });
    }

    private boolean isValid() {
        if(edtCurrentPassword.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_current_password), Toast.LENGTH_SHORT).show();
            return false;
        }else if(edtNewPassword.getText().toString().isEmpty()){
            Toast.makeText(context, getString(R.string.err_new_password), Toast.LENGTH_SHORT).show();
            return false;
        }else if (!edtCurrentPassword.getText().toString().equalsIgnoreCase(edtNewPassword.getText().toString())){
            Toast.makeText(context, getString(R.string.err_mismatch), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    /**
     * API CALLING
     */

    private void changePasswordAPI(){
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("password",edtCurrentPassword.getText().toString());
            params.put("confirm",edtNewPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().putAPI(AppConstant.CHANGE_PASSWORD_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                logoutAPI();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG",error);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void logoutAPI() {

        JSONObject params = new JSONObject();

        new APIManager().postAPI(AppConstant.LOGOUT_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                SessionManager.getInstance().logout();
                startActivity(new Intent(context,HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK));
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG",error);
            }
        });
    }
}
