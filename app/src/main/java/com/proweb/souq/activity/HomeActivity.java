package com.proweb.souq.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proweb.souq.R;
import com.proweb.souq.adapter.HomeListAdapter;
import com.proweb.souq.adapter.MyPagerAdapter;
import com.proweb.souq.model.BannerModel;
import com.proweb.souq.model.BannerTypeModel;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.SessionManager;
import com.proweb.souq.utils.SetLocalLanguage;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HomeActivity extends BaseActivity
        implements BaseSliderView.OnSliderClickListener {

    Context context;
    ArrayList<CategoryModel> list;
    private ImageView imgClose;
    RecyclerView recyclerView;
    private SliderLayout slider;
    HomeListAdapter homeListAdapter;
    private boolean doubleBackToExitPressedOnce = false;
    ArrayList<CategoryModel> categoryList;
    ArrayList<BannerTypeModel> listBanner;
    ArrayList<BannerModel> listBannerImages;
    public boolean isRightToLeft;


    //Slider
    private static ViewPager mPager;
    final Handler handler = new Handler();
    private static int currentPage = 0;
    private static final Integer[] XMEN = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner3};
    private ArrayList<String> XMENArray = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_home);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home, null, false);
        containerView.addView(contentView, 0);
        super.initBase();
        setHomeToolBar();
        init();
    }


    private void init() {
        context = this;
        list = new ArrayList<>();
        listBanner = new ArrayList<>();
        listBannerImages = new ArrayList<>();
        categoryList = new ArrayList<>();
        slider = findViewById(R.id.slider);
        imgClose = findViewById(R.id.img_close);

        //Method calling
        setUpRecyclerViewHome();
//        getSliderImages();
        getBanners();
        getViewCartListAPI();
    }

    private void setCategoryData() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setName(getString(R.string.lbl_deals_of_the_day));
        categoryList.add(categoryModel);
        Type type = new TypeToken<ArrayList<CategoryModel>>() {
        }.getType();
        menuList = new Gson().fromJson(new AppPreference(context).getCategoryList(), type);
        categoryList.addAll(menuList);
    }

    private void setUpRecyclerViewHome() {
        //add additional data like deal of the day to category list
        setCategoryData();
        recyclerView = findViewById(R.id.recycler_view);
        homeListAdapter = new HomeListAdapter(context, categoryList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(homeListAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    private void getSliderImages() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyPagerAdapter(context, XMENArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);


        /*final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == XMEN.length) {
                    currentPage = -1;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 4000);*/

        bannerSlider();
    }

    private void bannerSlider() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (currentPage == XMEN.length) {
                    currentPage = -1;
                }
                mPager.setCurrentItem(currentPage++, true);
                bannerSlider();
            }
        }, 5000);
    }

    /**
     * API CALLING
     **/
    private void getBanners() {
        JSONObject jsonObject = new JSONObject();
        new APIManager().getJSONArrayAPI(AppConstant.GET_BANNERS_API, jsonObject, BannerTypeModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                listBanner = (ArrayList<BannerTypeModel>) resultObj;
                String banner = new AppUtils().getBannerTypeId(listBanner, "Home Page Slideshow");
                getBannersById(banner);
            }

            @Override
            public void onError(String error) {
                //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getBannersById(String id) {
        JSONObject jsonObject = new JSONObject();
        new APIManager().getJSONArrayAPI(AppConstant.GET_BANNERS_API + id, jsonObject, BannerModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                listBannerImages = (ArrayList<BannerModel>) resultObj;
                for (BannerModel model : listBannerImages) {
                    XMENArray.add(model.getImage().replaceAll(" ", "%20"));
                }

                getSliderImages();
            }

            @Override
            public void onError(String error) {
                //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    //get cart item count
    private void getViewCartListAPI() {

        new APIManager().getAPI(AppConstant.GET_CART_API, CartModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                CartModel cartModel = (CartModel) resultObj;
                new AppPreference(context).setCartCount(cartModel.getTotal_product_count());
                updateCartCount();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    /**
     * HANDLER METHODS
     */
    @Override
    protected void onResume() {
        super.onResume();
        updateCartCount();

        isRightToLeft = TextUtils.getLayoutDirectionFromLocale(Locale
                .getDefault()) == ViewCompat.LAYOUT_DIRECTION_RTL;

        if (isRightToLeft != AppConstant.isAppRTL) {
            if (isRightToLeft) {
                SetLocalLanguage.setLocaleLanguage(context, "ar");
            } else {
                SetLocalLanguage.setLocaleLanguage(context, "en-gb");
            }
            AppConstant.isAppRTL = isRightToLeft;
            startActivity(new Intent(context, HomeActivity.class));
            finish();
        }

        //check user login and change drawer layout
        checkForloggedInUser();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish(); // finish activity
        } else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            Toast.makeText(this, getResources().getString(R.string.backagain_message), Toast.LENGTH_SHORT).show();
            doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3 * 1000);

        }
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}





 /*for (BannerModel model : listBannerImages
                ) {
            TextSliderView textSliderView = new TextSliderView(HomeActivity.this);


            String imgPath = model.getImage().replaceAll(" ", "%20");

            // initialize a SliderLayout
            textSliderView
                    .description("")
                    .image(imgPath)
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(HomeActivity.this);
            slider.addSlider(textSliderView);
        }*/