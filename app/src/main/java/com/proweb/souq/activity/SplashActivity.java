package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.SetLocalLanguage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class SplashActivity extends AppCompatActivity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        if (new AppPreference(context).getSessionID() == null) {
            getSessionIDAPI();
        } else {
            getCategories();
        }

        String language = new AppPreference(getApplicationContext()).getAppLanguage();
        if (language!= null) {
            SetLocalLanguage.setLocaleLanguage(context, language);
        }else {
            SetLocalLanguage.setLocaleLanguage(context,"en-gb");
        }
    }

    private void pushToActivity() {
        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                    if (!new AppPreference(context).isFirstTimeLaunch()) {
                        startActivity(new Intent(context, HomeActivity.class));
                    }else {
                        startActivity(new Intent(context,OnBoardingActivity.class));
                        new AppPreference(context).setFirstTimeLaunch(false);
                    }

                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    /**
     * API CALLING
     */
    private void getCategories() {

        new APIManager().getJSONArrayAPI(AppConstant.GET_CATEGORIES_API, null, CategoryModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ArrayList<CategoryModel>menuList = (ArrayList<CategoryModel>) resultObj;
                new AppPreference(context).setCategoryList(new Gson().toJson(menuList));
                pushToActivity();
            }

            @Override
            public void onError(String error) {
                pushToActivity();
                Toast.makeText(context, getString(R.string.err_no_server), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSessionIDAPI() {
        new APIManager().getAPI(AppConstant.GET_SESSION, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                UserModel userModel = (UserModel) resultObj;
                String sessionID = userModel.getSession();
                Log.d("TAG",sessionID);
                new AppPreference(context).setSessionID(sessionID);
                getCategories();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, getString(R.string.err_no_server), Toast.LENGTH_SHORT).show();
                Log.d("TAG",error);
            }
        });
    }

}