package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.HomeListAdapter;
import com.proweb.souq.adapter.ItemListAdapter;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.FilterModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.PaginationScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;

public class ItemListActivity extends BaseActivity {
    Context context;
    private ImageView imgView;
    RecyclerView mRecyclerView;
    ItemListAdapter mAdapter;
    private LinearLayout llSort;
    private RelativeLayout rlView, rlFilter, rlSort;
    private CustomProgressBar loader;
    private static final String TAG = ItemListActivity.class.getSimpleName();
    CategoryModel categoryModel;
    ArrayList<ProductModel> list;
    TextView txtNoData;

    // PAGINATION VARS
    private static final int PAGE_START = 1;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;
    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;
    // indicates the current page which Pagination is fetching.
    private int currentPage = PAGE_START;
    ProgressBar progressBar;
    SwipeRefreshLayout mSwipeRefreshLayout;

    LinearLayoutManager mLayoutManager;
    private int currentLayout = 0;
    String optionIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_item_list);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_item_list, null, false);
        containerView.addView(contentView, 0);
        super.initBase();
        init();
    }

    private void init() {
        context = this;
        imgView = findViewById(R.id.img_view);
        mRecyclerView = findViewById(R.id.recyclerview_items);
        llSort = findViewById(R.id.ll_middle);
        rlView = findViewById(R.id.rl_view);
        rlFilter = findViewById(R.id.rl_filter);
        rlSort = findViewById(R.id.rl_sort);
        txtNoData = findViewById(R.id.txt_no_data);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        loader = new CustomProgressBar(context);
        list = new ArrayList<>();
        categoryModel = new Gson().fromJson(getIntent().getStringExtra("menu_model"), CategoryModel.class);

        //Method calling
        setToolBarTitle(categoryModel.getName());
        setUpRecyclerView();
        setListeners();
    }

    private void setListeners() {
        rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(currentLayout);
            }
        });

        rlFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, FilterActivity.class).putExtra("category_model", new Gson().toJson(categoryModel)), 1);
            }
        });

        rlSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, SortItemsActivity.class), 2);
            }
        });
    }

    private void setUpRecyclerView() {
        mAdapter = new ItemListAdapter(context, list);
        mRecyclerView.setAdapter(mAdapter);
        setRecyclerViewLayoutManager(currentLayout);

        // mocking network delay for API call
        mSwipeRefreshLayout.setRefreshing(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (categoryModel.getName().equalsIgnoreCase(getString(R.string.lbl_deals_of_the_day))) {
                    llSort.setVisibility(View.GONE);
                    getDailyDeals();
                } else {
                    llSort.setVisibility(View.VISIBLE);
                    getProductsByCategoryAPI(null);
                }
            }
        }, 500);

        // On swipe refresh
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
    }

    private void refreshItems() {
        // To clear already set adapter and new menuList on swipe refresh
        mSwipeRefreshLayout.setRefreshing(true);
        mAdapter.clear();
        currentPage = PAGE_START;
        isLastPage = false;
        if (categoryModel.getName().equalsIgnoreCase(getString(R.string.lbl_deals_of_the_day))) {
            llSort.setVisibility(View.GONE);
            getDailyDeals();
        } else {
            llSort.setVisibility(View.VISIBLE);
            getProductsByCategoryAPI(optionIds);
        }
    }


    public void setRecyclerViewLayoutManager(int value) {
        int scrollPosition = 0;
        GridLayoutManager gridLayoutManager;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (value) {
            case 0:
                mAdapter.updateViewType(false);
                imgView.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_sub_menu));
                mLayoutManager = new GridLayoutManager(context, 2);
                gridLayoutManager = (GridLayoutManager) mLayoutManager;
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        switch (mAdapter.getItemViewType(position)) {
                            case ItemListAdapter.ITEM:
                                return 1;
                            case ItemListAdapter.LOADING:
                                return 2;
                        }
                        return 0;
                    }
                });
                currentLayout = 1;
                break;
            case 1:
                mAdapter.updateViewType(true);
                imgView.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_grid));
                mLayoutManager = new LinearLayoutManager(context);
                currentLayout = 0;
                break;
            default:
                mAdapter.updateViewType(false);
                imgView.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_sub_menu));
                mLayoutManager = new GridLayoutManager(context, 2);
                gridLayoutManager = (GridLayoutManager) mLayoutManager;
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        switch (mAdapter.getItemViewType(position)) {
                            case ItemListAdapter.ITEM:
                                return 1;
                            case ItemListAdapter.LOADING:
                                return 2;
                        }
                        return 0;
                    }
                });
                currentLayout = 1;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);

        // On scroll listener
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (categoryModel.getName().equalsIgnoreCase(getString(R.string.lbl_deals_of_the_day))) {
                            llSort.setVisibility(View.GONE);
                            getDailyDeals();
                        } else {
                            llSort.setVisibility(View.VISIBLE);
                            getProductsByCategoryAPI(null);
                        }

                    }
                }, 500);
            }

            @Override
            public int getTotalPageCount() {
                return 99999;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    /**
     * API CALLING
     */
    private void getProductsByCategoryAPI(String optionsIds) {
        String url = AppConstant.GET_PRODUCTS_BY_CATEGORY_API_NEW + "=" + categoryModel.getCategory_id() + "&limit=" + AppConstant.PAGINATION_LIMIT
                + "&page=" + currentPage;

        if (optionsIds != null) {
            Uri.Builder builder = Uri.parse(url).buildUpon();
            builder.appendQueryParameter("filters", optionsIds);
            url = builder.build().toString();
//            url = url + "&filter=" + optionsIds;
        }

        FilterModel filterModel = AppConstant.selectedSort;
        if (filterModel != null) {
            if (filterModel.getSort_type() != null) {
                if (!filterModel.getSort_type().equalsIgnoreCase(getString(R.string.str_default))) {
                    url = url + "&sort=" + filterModel.getSort_type() + "&order=" + filterModel.getSort_order();
                }
            }

        }

        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                //loader.dismiss();
                if (currentPage == PAGE_START) {
                    mAdapter.clear();
                }

                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.removeLoadingFooter();
                isLoading = false;

                list = (ArrayList<ProductModel>) resultObj;

                // if menuList<10 then last page true
                if (list.size() < 10) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    mAdapter.addAll(list);
                }

                if (mAdapter.list.size() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }


                // Add loading footer if last page is false
                if (!isLastPage) {
                    mAdapter.addLoadingFooter();
                }
            }

            @Override
            public void onError(String error) {
                Log.e(TAG, error);
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getDailyDeals() {

        String url = AppConstant.GET_DEAL_OF_DAY + "/limit/" + AppConstant.PAGINATION_LIMIT;
        new APIManager().getJSONArrayAPI(AppConstant.GET_DEAL_OF_DAY, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                if (currentPage == PAGE_START) {
                    mAdapter.clear();
                }

                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.removeLoadingFooter();
                isLoading = false;

                list = (ArrayList<ProductModel>) resultObj;

                // if menuList<10 then last page true
                if (list.size() < 1000) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    mAdapter.addAll(list);
                }

                if (mAdapter.list.size() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }


                // Add loading footer if last page is false
                if (!isLastPage) {
                    mAdapter.addLoadingFooter();
                }
            }

            @Override
            public void onError(String error) {
                Log.e(TAG, error);
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    private void filterByCategoryAPI(String optionsIds) {
        mSwipeRefreshLayout.setRefreshing(true);
        String url = AppConstant.GET_PRODUCT_DETAIL_API + "/category/" + categoryModel.getCategory_id() + "/filters/" + optionsIds;
        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                if (currentPage == PAGE_START) {
                    mAdapter.clear();
                }

                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.removeLoadingFooter();
                isLoading = false;

                list = (ArrayList<ProductModel>) resultObj;

                // if menuList<10 then last page true
                if (list.size() < 10) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    mAdapter.addAll(list);
                }

                if (mAdapter.list.size() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }


                // Add loading footer if last page is false
                if (!isLastPage) {
                    mAdapter.addLoadingFooter();
                }
            }

            @Override
            public void onError(String error) {
                Log.e(TAG, error);
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * HANDLER METHOD
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    String optionIds = data.getStringExtra("option_ids");
                    this.optionIds = optionIds;
                    refreshItems();

                }
            } else if (requestCode == 2) {
                refreshItems();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppConstant.tempSelected = null;
        AppConstant.selectedSort = null;
    }
}
