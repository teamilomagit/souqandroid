package com.proweb.souq.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class EditAccountActivity extends BaseActivity {

    Context context;
    EditText edtEmail, edtMobile, edtFirstname, edtLastname;
    TextView txtUpdate;
    int selectedGender = 0;
    CustomProgressBar customProgressBar;
    UserModel userModelAccount;
    RadioGroup rgGender;
    RadioButton rbMale,rbFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        init();
    }

    private void init() {
        context = this;
        edtFirstname = findViewById(R.id.edt_first_name);
        edtLastname = findViewById(R.id.edt_last_name);
        edtEmail = findViewById(R.id.edt_email);
        edtMobile = findViewById(R.id.edt_mobile);
        /*llFullName = findViewById(R.id.ll_full_name);
        llEmail = findViewById(R.id.ll_email);
        llMobile = findViewById(R.id.ll_mobile);*/
        rgGender = findViewById(R.id.rg_gender);
        rbMale = findViewById(R.id.rb_male);
        rbFemale = findViewById(R.id.rb_female);
        txtUpdate = findViewById(R.id.txt_update);
        customProgressBar = new CustomProgressBar(context);
        edtMobile.setInputType(InputType.TYPE_CLASS_NUMBER);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_edit_account));
        callHeader(context);
        setClickListeners();
        getAccountDetailAPI();
    }

    private void setClickListeners() {

        //Address default actions
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = group.findViewById(checkedId);
                if (radioButton.getText().toString().equalsIgnoreCase(getString(R.string.male))){
                    selectedGender = 1;
                }else {
                    selectedGender = 2;
                }
            }
        });

        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    updateAccountAPI();
                }
            }
        });
    }

    /**
     * FUNCTIONALITY
     */
    private boolean isValid() {
        if (edtFirstname.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_firstname), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtFirstname.getText().toString().length() > 32){
            Toast.makeText(context, getString(R.string.err_firstname_length), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtLastname.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_lastname), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtLastname.getText().toString().length() > 32) {
            Toast.makeText(context, getString(R.string.err_lastname_length), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtEmail.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_email_address), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!AppUtils.isValidEmail(edtEmail.getText().toString())){
            Toast.makeText(context, getResources().getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtMobile.getText().toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.err_mobile_number), Toast.LENGTH_SHORT).show();
            return false;
        }/*else if (edtMobile.getText().toString().length() < 10){
            Toast.makeText(context, getString(R.string.err_valid_mobile), Toast.LENGTH_SHORT).show();
            return false;
        }*/else if (selectedGender == 0){
            Toast.makeText(context, getString(R.string.err_gender), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void setData() {
        edtFirstname.setHint("");
        edtLastname.setHint("");
        edtFirstname.setText(userModelAccount.getFirstname());
        edtLastname.setText(userModelAccount.getLastname());
        edtEmail.setText(userModelAccount.getEmail());
        edtMobile.setText(userModelAccount.getTelephone());
        if (userModelAccount.getAccount_custom_field() != null) {
            if (userModelAccount.getAccount_custom_field().getGender().equalsIgnoreCase("1")) {
                rbMale.setChecked(true);
                selectedGender = 1;
            } else if (userModelAccount.getAccount_custom_field().getGender().equalsIgnoreCase("2")) {
                rbFemale.setChecked(true);
                selectedGender = 2;
            }
        }
    }

    /**
     * API CALLING
     */

    private void getAccountDetailAPI() {
        customProgressBar.show();
        new APIManager().getAPI(AppConstant.GET_ACCOUNT_DETAIL_API, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                //Toast.makeText(context, ""+msg, Toast.LENGTH_SHORT).show();
                customProgressBar.dismiss();
                userModelAccount = (UserModel) resultObj;
                setData();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, getString(R.string.err_profile), Toast.LENGTH_SHORT).show();
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }

    private void updateAccountAPI() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("firstname", edtFirstname.getText().toString());
            params.put("lastname", edtLastname.getText().toString());
            params.put("email", edtEmail.getText().toString());
            params.put("telephone", edtMobile.getText().toString());
            //params.put("gender",selectedGender);
            params.put("custom_field", new JSONObject().put("account", new JSONObject().put("1", selectedGender)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().putAPI(AppConstant.UPDATE_ACCOUNT_DETAIL_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                UserModel userModel = SessionManager.getInstance().getUserModel(context);
                userModel.setFirstname(edtFirstname.getText().toString());
                userModel.setLastname(edtLastname.getText().toString());
                userModel.setEmail(edtEmail.getText().toString());
                userModel.setTelephone(edtMobile.getText().toString());
                SessionManager.getInstance().createLoginSession(userModel,context);
                callHeader(context);
                Toast.makeText(context,getString(R.string.msg_success_update_account), Toast.LENGTH_SHORT).show();
                //disableControls();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }
}



        /*String[] fullname = new String[0];
        final String firstname, lastname;*/

        /*if (edtFullname.getText().toString().trim().contains(" ")) {
            fullname = edtFullname.getText().toString().split(" ");
            firstname = fullname[0];
            lastname = fullname[2];
        } else {
            firstname = edtFullname.getText().toString();
            lastname = " ";
        }

        if (lastname.equalsIgnoreCase(" ")){
            Toast.makeText(context, getString(R.string.err_lastname), Toast.LENGTH_SHORT).show();
            return;
        }*/



    /*private void showGenderDialog() {
        spinnerDialog = new SpinnerDialog(context, getString(R.string.title_select_gender), new SpinnerDialog.SpinnerDialogInterface() {
            @Override
            public void onItemSelected(String item) {
                 spinner.setText(item);

                 if (item.equalsIgnoreCase(getString(R.string.male))){
                     selectedGender = 1;
                 }else {
                     selectedGender = 2;
                 }
            }
        });
        spinnerDialog.setCancelable(false);
        spinnerDialog.show();
    }*/

    /*private void enableControls() {
        edtFirstname.setEnabled(true);
        edtLastname.setEnabled(true);
        edtEmail.setEnabled(true);
        edtMobile.setEnabled(true);
        txtUpdate.setClickable(true);
        txtUpdate.setEnabled(true);
        txtEdit.setVisibility(View.INVISIBLE);
        //edtFullname.requestFocus();
        rlGender.setClickable(true);
        spinner.setClickable(true);
        spinner.setEnabled(true);
    }

    private void disableControls() {
        edtFirstname.setEnabled(false);
        edtLastname.setEnabled(false);
        edtEmail.setEnabled(false);
        edtMobile.setEnabled(false);
        txtUpdate.setClickable(false);
        txtUpdate.setEnabled(false);
        txtEdit.setVisibility(View.VISIBLE);
        rlGender.setClickable(false);
        spinner.setClickable(false);
        spinner.setEnabled(false);
    }*/
