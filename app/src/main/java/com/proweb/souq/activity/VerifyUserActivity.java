package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class VerifyUserActivity extends BaseActivity {

    private Context context;
    private Button btnContinue, btnLogin;
    private EditText edtEmail, edtPassword;
    private ImageView imgGoogle, imgFacebook;
    private RadioButton rbRegister, rbGuest;

    private boolean isGuest = true;

    //google sign in
    private GoogleSignInClient mGoogleSignInClient;
    private final int RC_SIGN_IN = 101;

    //facebook
    CallbackManager callbackManager;
    LoginButton loginButton;

    private CustomProgressBar loader;
    private static final String TAG = VerifyUserActivity.class.getSimpleName();
    private CartModel cartModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_user);


        init();
        setToolBarTitleWithBack("");
    }

    private void init() {

        context = this;
        btnContinue = findViewById(R.id.btn_continue);
        btnLogin = findViewById(R.id.btn_login);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        imgGoogle = findViewById(R.id.img_google);
        imgFacebook = findViewById(R.id.img_fb);
        rbRegister = findViewById(R.id.rb_register_acc);
        rbGuest = findViewById(R.id.rb_guest_checkout);
        loader = new CustomProgressBar(context);
        cartModel = new Gson().fromJson(getIntent().getStringExtra("cart_model"), CartModel.class);


        //google
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //facebook
        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);

        //Method calling
        clickListener();
        initiateFacebookSignIn();
    }


    private void clickListener() {

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGuest) {
                    startActivity(new Intent(context, GuestDetailsActivity.class).putExtra("cart_model",new Gson().toJson(cartModel)));
                } else {
                    startActivity(new Intent(context, SignUpActivity.class));
                }
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    loginAPI();
                }
            }
        });

        imgFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        imgGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateGoogleSignIn();
            }
        });

        rbRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGuest = false;
            }
        });

        rbGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGuest = true;
            }
        });
    }

    private boolean isValid() {

        if (edtEmail.getText().toString().isEmpty()) {
            Toast.makeText(context, getResources().getString(R.string.err_registered_email), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!AppUtils.isValidEmail(edtEmail.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtPassword.getText().toString().isEmpty()) {
            Toast.makeText(context, getResources().getString(R.string.err_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    /**
     * Google Sign In
     **/
    private void initiateGoogleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
//            Toast.makeText(context, "sign in token -> " + account.getIdToken(), Toast.LENGTH_SHORT).show();
            // Signed in successfully, show authenticated UI.
            mGoogleSignInClient.signOut();
            socialLoginAPI(account.getEmail(), account.getGivenName(), account.getFamilyName(), account.getIdToken(), "google");

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    /**
     * FACEBOOK
     **/
    private void initiateFacebookSignIn() {
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken()
                        .getToken();
                Log.i("accessToken", accessToken);
                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData(accessToken);
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(getBaseContext(), getString(R.string.facebook_sign_in_cancelled), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(context, "" + exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void RequestData(final String accessCode) {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                try {
                    if (json != null) {
                        //Splitting name as first and last name
                        String lastName = "", firstName = "";

                        firstName = json.getString("name");

                        if (firstName.split("\\w+").length > 1) {
                            lastName = firstName.substring(firstName.lastIndexOf(" ") + 1);
                            firstName = firstName.substring(0, firstName.lastIndexOf(' '));
                        }

                        String emailID = json.getString("email");

                        LoginManager.getInstance().logOut();

                        socialLoginAPI(emailID, firstName, lastName, accessCode, "facebook");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(500).height(500),gender");
        request.setParameters(parameters);
        request.executeAsync();
    }


    /**
     * API CALLING
     */
    private void loginAPI() {

        loader.show();

        JSONObject params = new JSONObject();
        try {
            params.put("email", edtEmail.getText().toString());
            params.put("password", edtPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.LOGIN_API, params, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Log.d("TAG", resultObj.toString());
                loader.dismiss();
                UserModel userModel = (UserModel) resultObj;
                SessionManager.getInstance().createLoginSession(userModel, context);
                startActivity(new Intent(context, ViewCartActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void socialLoginAPI(String email, String firstname, String lastname, String accessToken, String provider) {

        loader.show();

        JSONObject params = new JSONObject();
        try {
            params.put("email", email);
            params.put("access_token", accessToken);
            params.put("provider", provider);
            params.put("firstname", firstname);
            params.put("lastname", lastname);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.SOCIAL_LOGIN_API, params, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Log.d("TAG", resultObj.toString());
                loader.dismiss();
                UserModel userModel = (UserModel) resultObj;
                SessionManager.getInstance().createLoginSession(userModel, context);
                startActivity(new Intent(context, ViewCartActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        });
    }


    /**
     * HANDLER METHODS
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
