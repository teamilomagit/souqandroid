package com.proweb.souq.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.adapter.MenuAdapter;
import com.proweb.souq.adapter.SubMenuAdapter;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

public class MyAccountActivity extends BaseActivity {

    Context context;
    ArrayList<CategoryModel> list;
    RecyclerView mRecyclerView;
    SubMenuAdapter mAdapter;
    LinearLayout llLogout;
    ProgressBar progressBar;
    CustomAlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        init();
    }

    /**
     * INITIALIZATIONS
     */
    private void init() {
        context = this;
        mRecyclerView = findViewById(R.id.recycler_view);
        llLogout = findViewById(R.id.ll_logout);
        list = new ArrayList<>();
        progressBar = new ProgressBar(context);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_my_account));
        callHeader(context);
        setUpMenu();
        setUpRecyclerview();
        setListeners();
    }

    private void setListeners() {
          llLogout.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  confirmLogout();
              }
          });
    }

    private void setUpRecyclerview() {
        //mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new SubMenuAdapter(context,list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * FUNCTIONALITY
     */
    private void setUpMenu() {
        CategoryModel model = new CategoryModel();
        model.setName(getString(R.string.submenu_edit_account));
        list.add(model);

        model = new CategoryModel();
        model.setName(getString(R.string.submenu_password));
        list.add(model);

        model = new CategoryModel();
        model.setName(getString(R.string.submenu_wishlist));
        list.add(model);

        model = new CategoryModel();
        model.setName(getString(R.string.submenu_order_history));
        list.add(model);

        model = new CategoryModel();
        model.setName(getString(R.string.submenu_address_book));
        list.add(model);
    }

    private void confirmLogout() {
            dialog = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
                @Override
                public void onPositive() {
                    dialog.dismiss();
                    logoutAPI();
                }
            });
            dialog.setCancelable(false);
            dialog.show();
            dialog.setTitle(getString(R.string.msg_logout));
     }

    /**
     * API CALLING
     */

    private void logoutAPI() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject params = new JSONObject();

        new APIManager().postAPI(AppConstant.LOGOUT_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                SessionManager.getInstance().logout();
                progressBar.setVisibility(View.GONE);
                new AppPreference(context).setCartCount(0);
                startActivity(new Intent(context,HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK));
                //txtLogout.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    /**
     * HANDLER METHODS
     */
    @Override
    protected void onResume() {
        super.onResume();
        callHeader(context);
    }
}
