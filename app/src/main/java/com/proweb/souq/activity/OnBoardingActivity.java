package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ilomatech.crosstwopageindicator.CrossTwoPageIndicator;
import com.proweb.souq.R;
import com.proweb.souq.adapter.MyPagerAdapter;
import com.proweb.souq.adapter.OnBoardingAdapter;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.Dataset;

import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class OnBoardingActivity extends AppCompatActivity {

    Context context;
    ViewPager mPager;
    CrossTwoPageIndicator indicator;
    TextView btnSkip, btnSignup, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        init();
    }

    private void init() {
        context = this;
        mPager = findViewById(R.id.pager);
        btnSkip = findViewById(R.id.btn_skip);
        btnSignup = findViewById(R.id.btn_signup);
        btnLogin = findViewById(R.id.btn_login);
        indicator = findViewById(R.id.indicator);

        setIntroSlider();
        setListeners();
    }

    private void setListeners() {
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AppPreference(context).setFirstTimeLaunch(false);
                startActivity(new Intent(context,HomeActivity.class));
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,SignUpActivity.class));
                //finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,LoginActivity.class));
                //finish();
            }
        });
    }


    private void setIntroSlider() {
        mPager.setAdapter(new OnBoardingAdapter(Dataset.getOnboardingData(context), context));
        indicator.setIndicatorCount(mPager.getAdapter().getCount());
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                //Toast.makeText(context, ""+i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int i) {
                   indicator.setPosition(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }
}
