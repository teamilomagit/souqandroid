package com.proweb.souq.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.ShippingMethodAdapter;
import com.proweb.souq.adapter.ViewCartAdapter;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.FlatModel;
import com.proweb.souq.model.FreeModel;
import com.proweb.souq.model.MethodModel;
import com.proweb.souq.model.OrderDetailModel;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.model.PaymentMethodPostModel;
import com.proweb.souq.model.PaymentMethodsModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.model.QuoteModel;
import com.proweb.souq.model.ShippingMethodsModel;
import com.proweb.souq.model.ShippingModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class OrderReviewActivity extends BaseActivity {

    Context context;
    private EditText edtComment;
    TextView txtPlaceOrder, txtSubtotalValue, txtShippingRateValue, txtCodValue, txtTotalValue, txtShipAddress, txtShipContact, txtShippingType;
    CustomProgressBar customProgressBar;
    CheckBox chkPaymentCash;
    String shippingMethod = "", shipAddress;
    CartModel cartModel;
    private RecyclerView mRecyclerView;
    private ViewCartAdapter mAdapter;
    private ArrayList<ProductModel> list;
    private ArrayList<FlatModel> shippingMethodList;
    TextView txtCouponLabel, txtCouponValue;
    RelativeLayout llRow3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_review);
        setToolBarTitleWithBack(getString(R.string.title_review));

        //Method caling
        init();
        setListeners();
        setUpRecyclerView();
        getProductList();
        getShippingMethods();
    }

    private void init() {
        context = this;
        edtComment = findViewById(R.id.edt_comment);
        txtPlaceOrder = findViewById(R.id.txt_place_order);
        chkPaymentCash = findViewById(R.id.chk_payment_cash);
        txtSubtotalValue = findViewById(R.id.txt_subtotal_value);
        txtShippingRateValue = findViewById(R.id.txt_shipping_rate_value);
        //txtCodValue = findViewById(R.id.txt_cod_value);
        txtTotalValue = findViewById(R.id.txt_total_value);
        txtShipAddress = findViewById(R.id.txt_ship_address);
        txtShipContact = findViewById(R.id.txt_ship_contact);
        txtShippingType = findViewById(R.id.txt_shipping_type);
        txtCouponLabel = findViewById(R.id.txt_coupon_label);
        txtCouponValue = findViewById(R.id.txt_coupon_value);
        llRow3 = findViewById(R.id.ll_row3);
        customProgressBar = new CustomProgressBar(context);
        cartModel = new Gson().fromJson(getIntent().getStringExtra("cart_model"), CartModel.class);
        shipAddress = getIntent().getStringExtra("ship_address");

        shippingMethodList = new ArrayList<>();
        // Method calling
    }

    private void setListeners() {
        txtPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkPaymentCash.isChecked()) {
                    confirmorder();
                } else {
                    Toast.makeText(context, getString(R.string.err_payment_method_check), Toast.LENGTH_SHORT).show();
                }
            }
        });

        chkPaymentCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    getPaymentMethods();
                }
            }
        });
    }

    private void setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new ViewCartAdapter(context, new ArrayList<ProductModel>(), new ViewCartAdapter.ViewCartAdapterInterface() {
            @Override
            public void getDeleteSuccess() {
            }

            @Override
            public void onItemSelected(String item, int position) {
            }

            @Override
            public void setItemQuantity(TextView itemView, int position) {
                itemView.setText(getString(R.string.quote_qty) + " " + cartModel.getProducts()[position].getQuantity());
            }

            @Override
            public void isAnyItemOutOfStock() {

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
    }

    /**
     * FUNCTIONALITY
     */
    private void setData(String shippingType, String shippingPrice) {
        Float shipPrice = Float.parseFloat(shippingPrice.trim());
        String cartPriceStr = cartModel.getTotals()[0].getValue();
        Float cartPrice = Float.parseFloat(cartPriceStr);
        float couponPrice = 0;
        if (cartModel.getTotals().length >= 3){
            String s1 = cartModel.getTotals()[1].getValue().replace("-","");
            s1.trim();
            couponPrice = Float.parseFloat(s1);
            llRow3.setVisibility(View.VISIBLE);
            txtCouponLabel.setText(cartModel.getTotals()[1].getTitle());
            txtCouponValue.setText(cartModel.getTotals()[1].getText());
        }else {
            llRow3.setVisibility(View.GONE);
        }

        Float total = shipPrice + cartPrice - couponPrice;

        txtSubtotalValue.setText(cartModel.getTotals()[0].getText());
        txtTotalValue.setText("$" + total);
        txtShippingRateValue.setText("$" + shippingPrice);
        txtShippingType.setText(shippingType);
        txtShipAddress.setText(shipAddress);

        if (SessionManager.getInstance().getUserModel(context) != null) {
            txtShipContact.setText(SessionManager.getInstance().getUserModel(context).getTelephone());
        } else if (new AppPreference(context).getGuestModel() != null) {
            txtShipContact.setText(new AppPreference(context).getGuestModel().getTelephone());
        }

        getShippingMethodsPost();

    }

    private void getProductList() {
        list = new ArrayList<>(Arrays.asList(cartModel.getProducts()));
        mAdapter.updateAdapter(list);
    }

    private void extractShippingMethods(ShippingMethodsModel shippingMethodsModel) {
        FreeModel freeModel = shippingMethodsModel.getFree();
        FlatModel flatModel = shippingMethodsModel.getFlat();

        if (freeModel != null) {
            QuoteModel quoteModel = freeModel.getQuote();
            FlatModel flatModel1 = new FlatModel();
            flatModel1.setTitle(quoteModel.getFree().getTitle());
            flatModel1.setText(quoteModel.getFree().getText());
            flatModel1.setCost(quoteModel.getFree().getCost());
            flatModel1.setCode(quoteModel.getFree().getCode());
            shippingMethodList.add(flatModel1);
        }

        if (flatModel != null) {
            QuoteModel quoteModel = flatModel.getQuote();
            flatModel = new FlatModel();
            flatModel.setTitle(quoteModel.getFlat().getTitle());
            flatModel.setText(quoteModel.getFlat().getText());
            flatModel.setCost(quoteModel.getFlat().getCost());
            flatModel.setCode(quoteModel.getFlat().getCode());
            shippingMethodList.add(flatModel);
        }

        if (shippingMethodList.size() > 0) {
            shippingMethodList.get(0).setChecked(true);
            shippingMethod = shippingMethodList.get(0).getCode();
            setData(shippingMethodList.get(0).getTitle(), shippingMethodList.get(0).getCost());
        }

        setUpShippingMethodList();

    }

    private void setUpShippingMethodList() {
        ShippingMethodAdapter shippingMethodAdapter = new ShippingMethodAdapter(context, shippingMethodList, new ShippingMethodAdapter.ShippingMethodAdapterInterface() {
            @Override
            public void onClicked(FlatModel flatModel) {
                shippingMethod = flatModel.getCode();
                setData(flatModel.getTitle(), flatModel.getCost());
            }
        });
        RecyclerView recyclerView = findViewById(R.id.recycler_view_ship_method);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(shippingMethodAdapter);

    }

    /**
     * API CALLING
     */
    private void getShippingMethods() {
        customProgressBar.show();
        new APIManager().getAPI(AppConstant.GET_SHIPPING_METHODS, ShippingModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ShippingModel shippingModel = (ShippingModel) resultObj;
                ShippingMethodsModel shippingMethodsModel = shippingModel.getShipping_methods();

                extractShippingMethods(shippingMethodsModel);
                customProgressBar.dismiss();
            }

            @Override
            public void onError(String error) {
                Log.e("TAG", error);
                customProgressBar.dismiss();
            }
        });
    }

    private void getShippingMethodsPost() {
        JSONObject params = new JSONObject();
        try {
            params.put("shipping_method", shippingMethod);
            params.put("comment", edtComment.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.GET_SHIPPING_METHODS_POST, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }

    private void getPaymentMethods() {
        customProgressBar.show();
        new APIManager().getAPI(AppConstant.GET_PAYMENT_METHODS, PaymentMethodsModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                PaymentMethodsModel paymentMethodsModel = (PaymentMethodsModel) resultObj;
                PaymentMethodPostModel paymentMethodPostModel = paymentMethodsModel.getPayment_methods();
                MethodModel methodModel = paymentMethodPostModel.getCod();
                shippingMethod = methodModel.getCode();
                getPaymentMethodPost();
            }

            @Override
            public void onError(String error) {
                Log.e("TAG", error);
                customProgressBar.dismiss();
            }
        });
    }

    private void getPaymentMethodPost() {
        JSONObject params = new JSONObject();
        try {
            params.put("payment_method", shippingMethod);
            params.put("agree", "1");
            params.put("comment", edtComment.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postJSONArrayAPI(AppConstant.GET_PAYMENT_METHODS, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }

    private void confirmorder() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("payment_method", shippingMethod);
            params.put("agree", "1");
            params.put("comment", edtComment.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().postAPI(AppConstant.CONFIRM_ORDER, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                confirmOrderPut();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }

    private void confirmOrderPut() {

        new APIManager().putAPI(AppConstant.CONFIRM_ORDER, null, OrderDetailModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                new AppPreference(context).setCartCount(0);
                updateCartCount();
                setOrderConfirmDialog(((OrderDetailModel) resultObj).getOrder_id());
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, getString(R.string.msg_order_failed), Toast.LENGTH_SHORT).show();
                Log.e("TAG", error);
            }
        });
    }

    /**
     * DIALOG
     **/
    private void setOrderConfirmDialog(String orderId) {
        CustomAlertDialog builder = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
        builder.show();
        builder.setTitle(getString(R.string.msg_confirm_order_str1) + " " + orderId + " " + getString(R.string.msg_confirm_order_str2));
        builder.setCancelable(false);

        builder.txtNegative.setVisibility(View.GONE);
        builder.txtPositive.setText(getString(R.string.continue_shopping));
    }
}
