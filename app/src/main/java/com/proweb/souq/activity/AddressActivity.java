package com.proweb.souq.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.AddressAdapter;
import com.proweb.souq.adapter.SubMenuAdapter;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.model.AddressCollectionModel;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomProgressBar;

import java.util.ArrayList;

public class AddressActivity extends BaseActivity {

    Context context;
    ArrayList<AddressCollectionModel> cList;
    ArrayList<AddressModel> list;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    AddressAdapter mAdapter;
    TextView txtAddAddress, txtNoData;
    CustomProgressBar customProgressBar;
    AddressModel addressModel;
    CustomAlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        init();
    }

    private void init() {
        context = this;
        txtAddAddress = findViewById(R.id.txt_add_address);
        txtNoData = findViewById(R.id.txt_no_data);
        mRecyclerView = findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        cList = new ArrayList<>();
        list = new ArrayList<>();
        customProgressBar = new CustomProgressBar(context);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_address));
        setListeners();
        setUpRecyclerview();
        getAddressAPI();
    }

    private void setListeners() {
        txtAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, AddAddressActivity.class),1);
            }
        });
    }

    private void setUpRecyclerview() {
        mAdapter = new AddressAdapter(context, list, new AddressAdapter.AddressAdapterInterface() {
            @Override
            public void onDeleteItem(AddressModel model) {
                addressModel = model;
                confirmDelete();
            }

            @Override
            public void onEditItem(AddressModel addressModel) {
                startActivityForResult(new Intent(context,AddAddressActivity.class).putExtra("address_model",new Gson().toJson(addressModel)),0);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
        // On swipe refresh
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAddressAPI();
            }
        });
    }

    private void confirmDelete() {
        dialog = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                dialog.dismiss();
                deleteAddressAPI();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
        dialog.setTitle(context.getString(R.string.msg_delete));
    }

    /**
     * API CALLING
     */
    private void getAddressAPI() {
         //customProgressBar.show();
        mSwipeRefreshLayout.setRefreshing(true);
         new APIManager().getAPI(AppConstant.GET_ADDRESSES_API, AddressCollectionModel.class, context, new APIManager.APIManagerInterface() {
             @Override
             public void onSuccess(Object resultObj, String msg) {
                 mSwipeRefreshLayout.setRefreshing(false);
                 //Toast.makeText(context, ""+msg, Toast.LENGTH_SHORT).show();
                 AddressCollectionModel addressCollectionModel = (AddressCollectionModel) resultObj;
                 list = addressCollectionModel.getAddresses();
                 if (list.size()>0){
                     mRecyclerView.setVisibility(View.VISIBLE);
                     txtNoData.setVisibility(View.GONE);
                     mAdapter.updateAdapter(list);
                 }else {
                     mRecyclerView.setVisibility(View.GONE);
                     txtNoData.setVisibility(View.VISIBLE);
                 }

             }

             @Override
             public void onError(String error) {
                 mSwipeRefreshLayout.setRefreshing(false);
                 mRecyclerView.setVisibility(View.GONE);
                 txtNoData.setVisibility(View.VISIBLE);
                 if (!error.equalsIgnoreCase("[]")) {
                     //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                 }

                 Log.e("TAG",error);
             }
         });
    }

    private void deleteAddressAPI(){
        String url = AppConstant.DELETE_ADDRESSES_API+"/"+addressModel.getAddress_id();
        customProgressBar.show();
        new APIManager().deleteAPI(url, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                  getAddressAPI();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                   Log.e("TAG",error);
            }
        });
    }

    /**
     * HANDLER METHODS
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == 0){
                    getAddressAPI();
            }else if(requestCode == 1){
                    getAddressAPI();
            }
        }
    }
}
