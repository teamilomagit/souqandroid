package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.ViewCartAdapter;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.AreaAddressModel;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class ViewCartActivity extends BaseActivity {
    private Context context;
    private ImageView imgEmptyCart;
    private LinearLayout llEmptyCart;
    private RecyclerView mRecyclerView;
    private RelativeLayout rlMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView txtTotalPrise, txtQuantity, txtSubtotalValue, txtTotalValue, txtContinueShopping, txtSubTotal, txtCheckout, txtShippingRateValue,
            txtApply;

    private CustomProgressBar customProgressBar;
    private ViewCartAdapter mAdapter;
    private ArrayList<ProductModel> list;
    CartModel cartModel;
    EditText edtCouponCode;
    TextView txtCouponValue, txtCouponLabel;
    boolean isAllItemInStock = true;
    private CustomAlertDialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_view_cart, null, false);
        containerView.addView(contentView, 0);
        super.initBase();
        init();
    }

    private void init() {
        context = this;
        imgEmptyCart = findViewById(R.id.img_empty_cart);
        llEmptyCart = findViewById(R.id.ll_empty_cart);
        txtQuantity = findViewById(R.id.txt_quantity);
        txtTotalPrise = findViewById(R.id.txt_total_prise);
        txtSubtotalValue = findViewById(R.id.txt_subtotal_value);
        txtTotalValue = findViewById(R.id.txt_total_value);
        txtContinueShopping = findViewById(R.id.txt_continue_shopping);
        txtSubTotal = findViewById(R.id.txt_subtotal);
        txtCheckout = findViewById(R.id.txt_checkout);
        txtApply = findViewById(R.id.txt_apply);
        txtCouponValue = findViewById(R.id.txt_coupon_value);
        txtCouponLabel = findViewById(R.id.txt_coupon_label);
        edtCouponCode = findViewById(R.id.edt_coupon_code);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        rlMain = findViewById(R.id.rl_main);
        txtShippingRateValue = findViewById(R.id.txt_shipping_rate_value);
        customProgressBar = new CustomProgressBar(context);

        //Method calling
        setToolBarTitle(getString(R.string.title_viewcart));
        setOnClickListener();
        setUpRecyclerView();
        getViewCartListAPI();

        //change empty icon as per language
        String language = new AppPreference(getApplicationContext()).getAppLanguage();
        if (language != null) {
            if (!language.equalsIgnoreCase("en-gb")) {
                Picasso.with(context).load(R.drawable.empty_cart_arabic).into(imgEmptyCart);
            } else {
                Picasso.with(context).load(R.drawable.empty_cart).into(imgEmptyCart);
            }
        }
    }

    private void setOnClickListener() {
        txtContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });

        txtCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllItemInStock) {
                    if (SessionManager.getInstance().isLoggedIn(context)) {
                        startActivity(new Intent(context, PaymentAddressActivity.class).putExtra("cart_model", new Gson().toJson(cartModel)));
                        //finish();
                    } else {
//                    startActivity(new Intent(context,LoginActivity.class));
                        startActivity(new Intent(context, VerifyUserActivity.class).putExtra("cart_model", new Gson().toJson(cartModel)));
                    }
                } else {
                    setOutOfStockDialog();
                }
            }
        });

        txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtCouponCode.getText().toString().isEmpty()) {
                    Toast.makeText(context, getString(R.string.err_apply_code), Toast.LENGTH_SHORT).show();
                } else {
                    applyCodeAPI();
                }
            }
        });
    }

    private void setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new ViewCartAdapter(context, new ArrayList<ProductModel>(), true, new ViewCartAdapter.ViewCartAdapterInterface() {
            @Override
            public void getDeleteSuccess() {
                isAllItemInStock = true;
                getViewCartListAPI();
            }

            @Override
            public void onItemSelected(String quantity, int position) {
                updateItemQuantity(quantity, position);
            }

            @Override
            public void setItemQuantity(TextView itemView, int position) {

            }

            @Override
            public void isAnyItemOutOfStock() {
                isAllItemInStock = false;
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
    }


    /**
     * FUNCTIONALITY
     */
    private void setData(CartModel model) {
        if (model.getTotals().length >= 3) {
            txtTotalPrise.setText(model.getTotals()[2].getText()); //top
            txtSubtotalValue.setText(model.getTotals()[0].getText()); //bottom
            txtTotalValue.setText(model.getTotals()[2].getText()); //bottom
            txtCouponLabel.setText(model.getTotals()[1].getTitle()); //bottom
            txtCouponValue.setText(model.getTotals()[1].getText()); //bottom
            txtCouponValue.setVisibility(View.VISIBLE); //bottom
            txtCouponLabel.setVisibility(View.VISIBLE); //bottom
        } else {
            txtTotalPrise.setText(model.getTotals()[1].getText()); //top
            txtSubtotalValue.setText(model.getTotals()[0].getText()); //bottom
            txtTotalValue.setText(model.getTotals()[1].getText()); //bottom
        }

        if (model.getTotal_product_count() == 1) {
            txtQuantity.setText(model.getTotal_product_count() + " " + getString(R.string.txt_item));
            txtSubTotal.setText(getString(R.string.lbl_subtotal) + " " + model.getTotal_product_count() + " " + getString(R.string.txt_item));
        } else {
            txtQuantity.setText(model.getTotal_product_count() + " " + getString(R.string.txt_items));
            txtSubTotal.setText(getString(R.string.lbl_subtotal) + " " + model.getTotal_product_count() + " " + getString(R.string.txt_items));
        }

        new AppPreference(context).setCartCount(cartModel.getTotal_product_count());
        updateCartCount();
    }

    /**
     * DIALOG
     **/
    private void setOutOfStockDialog() {
        builder = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                builder.dismiss();
            }
        });
        builder.show();
        builder.setTitle(getString(R.string.err_out_of_stock));
        builder.setCancelable(false);

        builder.txtNegative.setVisibility(View.GONE);
        builder.txtPositive.setText(getString(R.string.got_it));
    }


    /******** API *******/
    private void getViewCartListAPI() {
        customProgressBar.show();
        new APIManager().getAPI(AppConstant.GET_CART_API, CartModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                cartModel = (CartModel) resultObj;
                list = new ArrayList<>(Arrays.asList(cartModel.getProducts()));
                mAdapter.updateAdapter(list);
                if (list.size() > 0) {
                    rlMain.setVisibility(View.VISIBLE);
                    llEmptyCart.setVisibility(View.GONE);
                } else {
                    llEmptyCart.setVisibility(View.VISIBLE);
                    rlMain.setVisibility(View.GONE);
                }
                setData(cartModel);
            }

            @Override
            public void onError(String error) {
                if (error.equalsIgnoreCase("[]")) {
                    llEmptyCart.setVisibility(View.VISIBLE);
                    rlMain.setVisibility(View.GONE);
                }
                new AppPreference(context).setCartCount(0);
                customProgressBar.dismiss();
            }
        });
    }

    private void updateItemQuantity(String quantity, int position) {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("key", list.get(position).getKey());
            params.put("quantity", quantity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().putJSONArrayAPI(AppConstant.GET_CART_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                getViewCartListAPI();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
            }
        });
    }

    private void applyCodeAPI() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("coupon", edtCouponCode.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().postJSONArrayAPI(AppConstant.APPLY_CODE_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                getViewCartListAPI();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }
}