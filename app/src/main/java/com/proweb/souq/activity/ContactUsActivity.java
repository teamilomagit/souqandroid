package com.proweb.souq.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.proweb.souq.R;
import com.proweb.souq.adapter.ContactUsAdapter;
import com.proweb.souq.fragment.MapFragment;
import com.proweb.souq.utils.Dataset;

public class ContactUsActivity extends BaseActivity{

    Context context;
    RecyclerView mRecyclerview;
    ContactUsAdapter mAdapter;
    BroadcastReceiver mBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        init();
        //setMap();
    }

    private void init() {
        context = this;
        mRecyclerview = findViewById(R.id.recycler_view);

        //Method call
        setToolBarTitleWithBack("");
        setRecyclerview();
    }

    private void setRecyclerview() {
        mAdapter = new ContactUsAdapter(context, Dataset.getContactList(context));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.setAdapter(mAdapter);
    }
}
