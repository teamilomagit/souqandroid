package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.AttributeAdapter;
import com.proweb.souq.adapter.HomeListAdapter;
import com.proweb.souq.adapter.ItemListAdapter;
import com.proweb.souq.adapter.MyWishListAdapter;
import com.proweb.souq.dialog.AddtoCartDialog;
import com.proweb.souq.dialog.AttributeOptionsDialog;
import com.proweb.souq.dialog.ShippingDeliveryDialog;
import com.proweb.souq.dialog.SpinnerDialog;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.model.ProductOptionModel;
import com.proweb.souq.model.ProductOptionValueModel;
import com.proweb.souq.model.ShippingDeliveryModel;
import com.proweb.souq.model.WishListModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener {
    Context context;
    private ImageView imgWishlist, imgDelivery;
    private RecyclerView mRecyclerViewAttr, mRecyclerViewSimilarProducts;
    private SliderLayout slider;
    private TextView spinner;
    private TextView txtProductName, txtAvailability, txtPrice, txtRating, txtReviews, txtAddToCart, txtPostQuestion, txtSimilarProductTitle;
    ProductModel productModel;
    WishListModel wishListModel;
    private CustomProgressBar loader;
    WebView webView;
    LinearLayout llRatingReview;
    RelativeLayout rlSpinner;
    String spinnerSelectedItem = "1";
    RelativeLayout rlSlider;
    ShippingDeliveryDialog dialog;
    ShippingDeliveryModel referenceShippingModel;
    private ArrayList<ProductOptionModel> tempList;
    private AttributeAdapter mAttributeAdapter;
    private ItemListAdapter mItemListAdapter;
    // public boolean isFromWishlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_item_detail, null, false);
        containerView.addView(contentView, 0);
        super.initBase();
        init();
    }

    private void init() {
        context = this;
        imgDelivery = findViewById(R.id.img_delivery);
        imgWishlist = findViewById(R.id.img_wish_list);
        slider = findViewById(R.id.slider);
        webView = findViewById(R.id.web_view);
        txtProductName = findViewById(R.id.txt_product_name);
        txtAvailability = findViewById(R.id.txt_availibility);
        txtPrice = findViewById(R.id.txt_price);
        txtRating = findViewById(R.id.txt_rating);
        txtReviews = findViewById(R.id.txt_reviews);
        txtPostQuestion = findViewById(R.id.txt_post_question);
        txtSimilarProductTitle = findViewById(R.id.txt_similar_product_title);
        llRatingReview = findViewById(R.id.ll_rating_review);
        txtAddToCart = findViewById(R.id.txt_add_to_cart);
        rlSpinner = findViewById(R.id.rl_spinner);
        spinner = findViewById(R.id.spinner);
        rlSlider = findViewById(R.id.rl_slider);
        loader = new CustomProgressBar(context);
        if (getIntent().getBooleanExtra("flag", false)) {
            wishListModel = new Gson().fromJson(getIntent().getStringExtra("wishlist_model"), WishListModel.class);
        } else {
            productModel = new Gson().fromJson(getIntent().getStringExtra("product_model"), ProductModel.class);
        }
        tempList = new ArrayList<>();

        //Method calling
        setToolBarTitle("");
        if (productModel != null) {
            getSliderImages();
        }
        getProductDetailApi();
        setListeners();
        setUpSimilarProducts();
    }

    private void setListeners() {
        imgDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showShippingDialog();
            }
        });

        imgWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productModel.isIn_wishlist()) {
                    removeFromWishlist();
                } else {
                    addToWishListAPI();
                }
            }
        });


        txtAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartAPI();
            }
        });

        rlSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog();
            }
        });

        slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Slider click", Toast.LENGTH_SHORT).show();
                /*startActivity(new Intent(context, FullImageviewActivity.class).putExtra("image_list", productModel.getImages())
                        .putExtra("position", 0));*/
            }
        });

        txtPostQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, PostQuestionActivity.class));
            }
        });

        txtReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ProductReviewActivity.class)
                        .putExtra("product_model", new Gson().toJson(productModel)));
            }
        });
    }

    private void setAttributeRecyclerView() {

        List<ProductOptionModel> list = Arrays.asList(productModel.getOptions());

        tempList.addAll(list);

        mAttributeAdapter = new AttributeAdapter(context, tempList, new AttributeAdapter.AddressAdapterInterface() {
            @Override
            public void getSelectedPositionVal(ArrayList<ProductOptionModel> list) {
                mAttributeAdapter.updateAdapter(list);
            }
        });

        mRecyclerViewAttr = findViewById(R.id.recycler_view_options);
        mRecyclerViewAttr.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerViewAttr.setAdapter(mAttributeAdapter);
    }

    private void setUpSimilarProducts() {
        mItemListAdapter = new ItemListAdapter(context, new ArrayList<ProductModel>(), true);

        mRecyclerViewSimilarProducts = findViewById(R.id.recycler_view_item);
        mRecyclerViewSimilarProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewSimilarProducts.setAdapter(mItemListAdapter);
        mRecyclerViewSimilarProducts.setNestedScrollingEnabled(false);

        getSimilarProducts();
    }

    /**
     * FUNCTIONALITY
     */
    private void showShippingDialog() {
        dialog = new ShippingDeliveryDialog(context, new ShippingDeliveryDialog.ShippingDeliveryDialogInterface() {
            @Override
            public void onItemSelected(ShippingDeliveryModel shippingDeliveryModel) {
                dialog.dismiss();
                referenceShippingModel = shippingDeliveryModel;
                //Toast.makeText(context, referenceShippingModel.getName(), Toast.LENGTH_SHORT).show();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showSpinnerDialog() {
        SpinnerDialog spinnerDialog = new SpinnerDialog(context, getString(R.string.lbl_quantity), new SpinnerDialog.SpinnerDialogInterface() {
            @Override
            public void onItemSelected(String item) {
                spinnerSelectedItem = item;
                spinner.setText(item);
            }
        });
        spinnerDialog.setCancelable(false);
        spinnerDialog.show();
    }

    private void getSliderImages() {

        if (productModel.getImage() != null) {
            if (productModel.getImages().length > 0) {
                for (String i : productModel.getImages()) {

                    TextSliderView textSliderView = new TextSliderView(ItemDetailActivity.this);

                    // initialize a SliderLayout
                    textSliderView
                            .description("")
                            .image(i)
                            .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                            .setOnSliderClickListener(ItemDetailActivity.this);
                    slider.addSlider(textSliderView);
                }
            } else {
                TextSliderView textSliderView = new TextSliderView(ItemDetailActivity.this);

                // initialize a SliderLayout
                textSliderView
                        .description("")
                        .image(productModel.getImage())
                        .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                        .setOnSliderClickListener(ItemDetailActivity.this);
                slider.addSlider(textSliderView);
            }
        } else if (productModel.getThumb() != null) {
            TextSliderView textSliderView = new TextSliderView(ItemDetailActivity.this);

            // initialize a SliderLayout
            textSliderView
                    .description("")
                    .image(productModel.getThumb())
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(ItemDetailActivity.this);
            slider.addSlider(textSliderView);
        }

    }

    private void setData(ProductModel productModel) {
        this.productModel = productModel;
        ///quant = productModel.getQuantity()+1;
        spinner.setText("1");
        webView.loadData(productModel.getDescription(), "text/html; charset=utf-8", "UTF-8");
        if (productModel.getManufacturer() != null) {
            txtProductName.setText(productModel.getManufacturer() + " " + productModel.getName());
        } else {
            txtProductName.setText(productModel.getName());
        }
        txtAvailability.setText(" " + productModel.getStock_status());
        txtPrice.setText(productModel.getPrice_formated());


        if (!productModel.getRating().equalsIgnoreCase("0") ||
                !productModel.getReviews().getReview_total().equalsIgnoreCase("0")) {
            llRatingReview.setVisibility(View.VISIBLE);
        }

        if (productModel.getRating().equalsIgnoreCase("0") && productModel.getReviews().getReview_total().equalsIgnoreCase("0")) {
            llRatingReview.setVisibility(View.GONE);
        } else {
            llRatingReview.setVisibility(View.VISIBLE);
        }

        if (!productModel.getRating().equalsIgnoreCase("0")) {
            txtRating.setText(productModel.getRating());
        } else {
            txtRating.setVisibility(View.GONE);
        }

        if (productModel.getReviews().getReview_total().equalsIgnoreCase("0")) {
            txtReviews.setVisibility(View.GONE);
        } else if (productModel.getReviews().getReview_total().equalsIgnoreCase("1")) {
            txtReviews.setText(productModel.getReviews().getReview_total() + " " + getString(R.string.review));
        } else {
            txtReviews.setText(productModel.getReviews().getReview_total() + " " + getString(R.string.reviews));
        }

        if (productModel.isIn_wishlist()) {
            imgWishlist.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        getSliderImages();
        // set up product options
        setAttributeRecyclerView();

    }

    private void showAddToCartDialog(ProductModel productModel) {
        AddtoCartDialog dialog = new AddtoCartDialog(context, productModel.getName(), productModel.getPrice_formated(), productModel.getImage());
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }


    /**** API*****/

    private void getProductDetailApi() {
        String url;
        loader.show();
        if (productModel != null) {
            url = AppConstant.GET_PRODUCT_DETAIL_API + "/" + productModel.getProduct_id();
        } else {
            url = AppConstant.GET_PRODUCT_DETAIL_API + "/" + wishListModel.getProduct_id();
        }

        new APIManager().getAPI(url, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                loader.dismiss();
                setData((ProductModel) resultObj);
            }

            @Override
            public void onError(String error) {
                //Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        });
    }

    private void addToCartAPI() {
        loader.show();
        JSONObject params = new JSONObject();
        final AppPreference appPreference = new AppPreference(context);

        try {

            ArrayList<ProductOptionValueModel> tempList = mAttributeAdapter.getSelectedOptions();

            JSONObject optionObject = new JSONObject();

            if (tempList.size() > 0) {
                for (ProductOptionValueModel productOptionValueModel : tempList) {
                    optionObject.put(productOptionValueModel.getSelected_parent_id(), productOptionValueModel.getOption_value_id());
                }
                params.put("option", optionObject);
            }

            params.put("product_id", productModel.getProduct_id());
            params.put("quantity", spinnerSelectedItem);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.ADD_TO_CART_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                loader.dismiss();
                getViewCartListAPI();
                showAddToCartDialog(productModel);
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
            }
        });
    }

    private void addToWishListAPI() {
        loader.show();
        String url = AppConstant.GET_WISHLIST_API + "/" + productModel.getId();

        new APIManager().postAPI(url, null, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Toast.makeText(context, getString(R.string.msg_added_wishlist), Toast.LENGTH_SHORT).show();
                imgWishlist.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                productModel.setIn_wishlist(true);
                loader.dismiss();
            }

            @Override
            public void onError(String error) {
                Log.e("TAG", error);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        });
    }

    private void removeFromWishlist() {
        loader.show();

        String url = AppConstant.GET_WISHLIST_API + "/" + productModel.getProduct_id();
        new APIManager().deleteAPI(url, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                loader.dismiss();
//                Toast.makeText(context,getString(R.string.alert_success_wishlist_remove), Toast.LENGTH_SHORT).show();
                imgWishlist.setColorFilter(ContextCompat.getColor(context, R.color.colorGray), android.graphics.PorterDuff.Mode.MULTIPLY);
                productModel.setIn_wishlist(false);
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //get cart item count
    private void getViewCartListAPI() {

        new APIManager().getAPI(AppConstant.GET_CART_API, CartModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                CartModel cartModel = (CartModel) resultObj;
                new AppPreference(context).setCartCount(cartModel.getTotal_product_count());
                updateCartCount();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void getSimilarProducts() {
        String url;
        if (productModel != null) {
            url = "http://dev.ravikatre.in/index.php?route=feed/rest_api/related&id=" + productModel.getProduct_id();
        } else {
            url = "http://dev.ravikatre.in/index.php?route=feed/rest_api/related&id=" + wishListModel.getProduct_id();
        }
        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ArrayList<ProductModel> tempList = (ArrayList<ProductModel>) resultObj;

                if (tempList.size() > 0) {
                    txtSimilarProductTitle.setVisibility(View.VISIBLE);
                    mRecyclerViewSimilarProducts.setVisibility(View.VISIBLE);
                    mItemListAdapter.updateAdapter(tempList);
                }

            }

            @Override
            public void onError(String error) {
                txtSimilarProductTitle.setVisibility(View.GONE);
                mRecyclerViewSimilarProducts.setVisibility(View.GONE);
            }
        });
    }


    /**
     * HANDLER
     **/

    @Override
    protected void onResume() {
        super.onResume();
        updateCartCount();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (productModel.getImage() != null) {
            if (productModel.getImages().length > 0) {
                startActivity(new Intent(context, FullImageviewActivity.class).putExtra("model_list", productModel.getImages())
                        .putExtra("position", 0));
            } else {
                String[] image = new String[1];
                image[0] = productModel.getImage();
                startActivity(new Intent(context, FullImageviewActivity.class).putExtra("model_list", image)
                        .putExtra("position", 0));
            }
        } else if (productModel.getThumb() != null) {
            String[] image = new String[1];
            image[0] = productModel.getThumb();
            startActivity(new Intent(context, FullImageviewActivity.class).putExtra("model_list", image)
                    .putExtra("position", 0));
        }

    }
}




    /*private void setSpinner() {
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.layout_spinner_item, quantity);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }*/