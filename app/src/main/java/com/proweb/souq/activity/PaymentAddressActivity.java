package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.AddressAdapter;
import com.proweb.souq.adapter.AreaAddressAdapter;
import com.proweb.souq.adapter.ItemListAdapter;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.AreaAddressModel;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.PaginationScrollListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PaymentAddressActivity extends BaseActivity {

    Context context;
    RecyclerView mRecyclerView;
    TextView txtNoData, txtNext, txtSaveAddress;
    AreaAddressAdapter mAdapter;
    ArrayList<AddressModel> list;
    CustomProgressBar customProgressBar;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AddressModel model;
    CartModel cartModel;
    String shipAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_address);
        init();
    }

    private void init() {
        context = this;
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mRecyclerView = findViewById(R.id.recyclerview_items);
        txtNoData = findViewById(R.id.txt_no_data);
        txtNext = findViewById(R.id.txt_next);
        txtSaveAddress = findViewById(R.id.txt_save_address);
        list = new ArrayList<>();
        customProgressBar = new CustomProgressBar(context);
        cartModel = new Gson().fromJson(getIntent().getStringExtra("cart_model"), CartModel.class);

        //Method calling
        setToolBarTitleWithBack(getString(R.string.title_payment_addresses));
        setListeners();
        setUpRecyclerview();
        getPaymentAddresses();
    }

    private void setListeners() {

        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model != null) {
                    getPaymentExistingAddress();
                } else {
                    Toast.makeText(context, getString(R.string.err_select_payment_address), Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AddAddressActivity.class));
                //finish();
            }
        });

    }

    private void setUpRecyclerview() {

        mAdapter = new AreaAddressAdapter(context, list, new AreaAddressAdapter.AreaAddressAdapterInterface() {
            @Override
            public void onItemSelected(AddressModel addressModel) {
                model = addressModel;
                shipAddress = model.getAddress_1() + " " + model.getAddress_2();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // On swipe refresh
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

    }

    private void refreshItems() {

        // To clear already set adapter and new menuList on swipe refresh
        mSwipeRefreshLayout.setRefreshing(true);
        mAdapter.clear();
        getPaymentAddresses();

    }

    /**
     * API CALLING
     */
    private void getPaymentAddresses() {

        //customProgressBar.show();
        mSwipeRefreshLayout.setRefreshing(true);
        new APIManager().getJSONArrayAPIADDRESS(AppConstant.GET_PAYMENT_ADDRESSES, null, AreaAddressModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                //customProgressBar.dismiss();
                mSwipeRefreshLayout.setRefreshing(false);
                AreaAddressModel areaAddressModel = (AreaAddressModel) resultObj;
                list = areaAddressModel.getAddresses();
                if (list.size() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                    txtNext.setVisibility(View.VISIBLE);
                    mAdapter.updateAdapter(areaAddressModel.getAddress_id(), list);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                    txtNext.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(String error) {
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
                txtNext.setVisibility(View.GONE);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                Log.e("TAG", error);
            }
        });

    }

    private void getPaymentExistingAddress() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("address_id", model.getAddress_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().postJSONArrayAPI(AppConstant.GET_PAYMENT_EXISTING_ADDRESS, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                getShippingExistingAddress();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                Log.e("TAG", error);
            }
        });
    }

    private void getShippingExistingAddress() {
        JSONObject params = new JSONObject();
        try {
            params.put("address_id", model.getAddress_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new APIManager().postJSONArrayAPI(AppConstant.GET_SHIPPING_ADDRESS, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                startActivity(new Intent(context, OrderReviewActivity.class).putExtra("cart_model", new Gson().toJson(cartModel))
                        .putExtra("ship_address", shipAddress));

            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Log.e("TAG", error);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstant.SHOULD_RELOAD) {
            AppConstant.SHOULD_RELOAD = false;
            refreshItems();
        }
    }
}
