package com.proweb.souq.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proweb.souq.R;
import com.proweb.souq.adapter.FilterGroupAdapter;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.FilterGroupsModel;
import com.proweb.souq.model.FilterModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

public class FilterActivity extends AppCompatActivity {

    Context context;
    RecyclerView mRecyclerView;
    FilterGroupAdapter mAdapter;
    ArrayList<FilterGroupsModel> list, tempList;
    ArrayList<CategoryModel> catList;
    CategoryModel categoryModel;
    TextView txtCategoryName, txtReset;
    String selectedOptions = "";
    String optionsIds = "";
    RelativeLayout rlApply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        //super.initBase();
        init();
    }

    private void init() {
        context = this;
        mRecyclerView = findViewById(R.id.recycler_view);
        txtReset = findViewById(R.id.txt_reset);
        txtCategoryName = findViewById(R.id.txt_category_name);
        rlApply = findViewById(R.id.rl_apply);
        list = new ArrayList<>();
        tempList = new ArrayList<>();
        categoryModel = new Gson().fromJson(getIntent().getStringExtra("category_model"), CategoryModel.class);
        if (categoryModel != null) {
            txtCategoryName.setText(categoryModel.getName());
        }

        ImageView imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppConstant.tempSelected != null) {
                    AppConstant.tempSelected.clear();
                    AppConstant.tempSelected.addAll(tempList);
                }
                finish();
            }
        });


        setListeners();
        setUpRecyclerview();
        getFilterGroupList();
    }

    private void setListeners() {
        txtReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (FilterGroupsModel model : list) {
                    model.setSelectedOptions(null);
                    model.setSelectedValue(null);
                }
                Toast.makeText(context, getString(R.string.txt_filter_reset), Toast.LENGTH_SHORT).show();
                mAdapter.notifyDataSetChanged();
            }
        });

        rlApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //filterByCategoryAPI();
                if (AppConstant.tempSelected != null) {
                    AppConstant.tempSelected.clear();
                } else {
                    AppConstant.tempSelected = new ArrayList<>();
                }

                AppConstant.tempSelected.addAll(list);

                optionsIds = "";
                for (int i = 0; i < AppConstant.tempSelected.size(); i++) {
                    if (list.get(i).getSelectedOptions() != null) {
                        if (i == AppConstant.tempSelected.size() - 1) {
                            optionsIds = optionsIds + list.get(i).getSelectedOptions();
                        } else {
                            optionsIds = optionsIds + list.get(i).getSelectedOptions() + ",";
                        }
                    }
                }

                if (optionsIds.length() > 0) {
                    if ((optionsIds.substring(optionsIds.length() - 1).equalsIgnoreCase(","))) {
                        optionsIds = optionsIds.substring(0, optionsIds.length() - 1);
                    }
                }

                Intent data = new Intent();
                data.putExtra("option_ids", optionsIds);
                setResult(Activity.RESULT_OK, data);
                finish();
            }
        });
    }

    private void getFilterGroupList() {
        if (AppConstant.tempSelected != null && AppConstant.tempSelected.size() != 0) {
            copyArrayList();
            list = (ArrayList<FilterGroupsModel>) AppConstant.tempSelected.clone();
            //if coming fresh then just show the appconstant filter as it is
//            updateLocalFilters(null, 0);
        } else {
            Type type = new TypeToken<ArrayList<CategoryModel>>() {
            }.getType();
            catList = new Gson().fromJson(new AppPreference(context).getCategoryList(), type);

            if (categoryModel != null) {
                for (CategoryModel model : catList) {
                    if (model.getCategory_id().equalsIgnoreCase(categoryModel.getCategory_id().toString())) {
                        for (FilterGroupsModel filterGroupsModel : model.getFilters().getFilter_groups()) {
                            list.add(filterGroupsModel);
                        }
                    }
                }
            }

        }//else close
        mAdapter.updateAdapter(list);

    }

    private void setUpRecyclerview() {
        mAdapter = new FilterGroupAdapter(context, list, new FilterGroupAdapter.FilterGroupAdapterInterface() {
            @Override
            public void onGroupSelected(int position) {
                optionsIds = list.get(position).getSelectedOptions();
                ((Activity) context).startActivityForResult(new Intent(context, FilterOptionsActivity.class).
                        putExtra("filter_model", new Gson().toJson(list.get(position))).putExtra("position", position).putExtra("option_ids", list.get(position).getSelectedOptions()), 1);
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void copyArrayList() {

        for (FilterGroupsModel filterGroupsModel : AppConstant.tempSelected) {
            FilterGroupsModel temp = new FilterGroupsModel();
            temp.setSelectedOptions(filterGroupsModel.getSelectedOptions());
            temp.setFilter(filterGroupsModel.getFilter());
            temp.setFilter_group_id(filterGroupsModel.getFilter_group_id());
            temp.setName(filterGroupsModel.getName());
            temp.setSelectedValue(filterGroupsModel.getSelectedValue());
            tempList.add(temp);
        }

    }

    private void updateLocalFilters(ArrayList<FilterModel> tempArr, int position) {
        ArrayList<FilterGroupsModel> localList = new ArrayList<>();
        selectedOptions = "";
        optionsIds = "";

        ArrayList<FilterGroupsModel> mainList = AppConstant.tempSelected.size() != 0 ? AppConstant.tempSelected : list;

        //Create temp local array
        for (FilterGroupsModel fModel :
                mainList) {
            FilterGroupsModel model = new FilterGroupsModel();
            model.setSelectedOptions(selectedOptions);
            model.setSelectedOptions(optionsIds);

            localList.add(model);
        }
        if (tempArr != null) {
            for (int i = 0; i < tempArr.size(); i++) {

                if (i < tempArr.size() - 1) {
                    selectedOptions = selectedOptions + tempArr.get(i).getName() + ", ";
                    optionsIds = optionsIds + tempArr.get(i).getFilter_id() + ", ";
                } else {
                    selectedOptions = selectedOptions + tempArr.get(i).getName();
                    optionsIds = optionsIds + tempArr.get(i).getFilter_id();
                }

            }

            if (!selectedOptions.equalsIgnoreCase("")) {
                localList.get(position).setSelectedOptions(optionsIds);
                localList.get(position).setSelectedValue(selectedOptions);
                //mAdapter.updateAdapter(localList);
            } else {
                localList.get(position).setSelectedOptions(null);
                localList.get(position).setSelectedValue(null);
                // mAdapter.updateAdapter(localList);
            }
        }

        mAdapter.updateAdapter(localList);
    }

    /**
     * HANDLER METHODS
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                if (data != null) {
                    selectedOptions = "";
                    optionsIds = "";

                    int position = data.getIntExtra("position", 0);
                    ArrayList<FilterModel> tempArr = data.getParcelableArrayListExtra("filter_options");

//                    updateLocalFilters(tempArr, position);

                    //get comma separated string
                    for (int i = 0; i < tempArr.size(); i++) {
                        if (i < tempArr.size() - 1) {
                            selectedOptions = selectedOptions + tempArr.get(i).getName() + ", ";
                            optionsIds = optionsIds + tempArr.get(i).getFilter_id() + ", ";
                        } else {
                            selectedOptions = selectedOptions + tempArr.get(i).getName();
                            optionsIds = optionsIds + tempArr.get(i).getFilter_id();
                        }
                    }

                    if (!selectedOptions.equalsIgnoreCase("")) {
                        list.get(position).setSelectedOptions(optionsIds);
                        list.get(position).setSelectedValue(selectedOptions);
                        mAdapter.updateAdapter(list);
                    } else {
                        list.get(position).setSelectedOptions(null);
                        list.get(position).setSelectedValue(null);
                        mAdapter.updateAdapter(list);
                    }

                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (AppConstant.tempSelected != null) {
            AppConstant.tempSelected.clear();
            AppConstant.tempSelected.addAll(tempList);
        }
        super.onBackPressed();
    }
}
