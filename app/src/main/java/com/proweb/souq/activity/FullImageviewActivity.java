package com.proweb.souq.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.adapter.ImagePagerAdapter;
import com.proweb.souq.model.GalleryModel;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class FullImageviewActivity extends AppCompatActivity {

    private Context context;
    private TextView txtCount;
    private ImagePagerAdapter mAdapter;
    private ViewPager viewPager;
    private int position;
    //private String[] list;
    ArrayList<String> list;
    private CustomProgressBar customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_imageview);
        init();
    }

    private void init() {
        context = this;
        txtCount = findViewById(R.id.txt_count);
        customProgressDialog = new CustomProgressBar(context);
        list = new ArrayList<>();

        setToolBar();
        setIntentData();
        changeStatusBarColor();
        updateCounter();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorLightBlack));
        }
    }

    private void setToolBar() {
        ImageView imgBack = findViewById(R.id.img_back);
        ImageView imgDelete = findViewById(R.id.img_delete);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //confirmDelete();
            }
        });

    }

    private void setIntentData() {
        position = getIntent().getIntExtra("position", 0);
        list = new ArrayList<>(Arrays.asList(getIntent().getStringArrayExtra("model_list")));
        setViewPager();
    }

    private void setViewPager() {
        viewPager = findViewById(R.id.view_pager);
        mAdapter = new ImagePagerAdapter(context, list);
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                position = i;
                updateCounter();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setCurrentItem(position);
    }

    private void updateCounter() {
        txtCount.setText((position + 1) + "/" + list.size());
    }


    /**
     * DIALOG
     **/
    private void confirmDelete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete this image?");
        builder.setPositiveButton(context.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //deleteImageAPI();
            }
        });

        builder.setNegativeButton(context.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }


    /**
     * API CALLING
     **/

    /*private void deleteImageAPI() {

        customProgressDialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("image_id", list.get(position).getId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_DELETE_GALLERY_PICS, jsonObject, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, Object extraInfo, String msg) {
                customProgressDialog.dismiss();
                AppConstant.SHOULD_RELOAD = true;
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

                list.remove(position);

                if (list.size() > 0) {
                    mAdapter.deleteImage(list);
                    updateCounter();
                } else {
                    finish();
                }
            }

            @Override
            public void onError(String error) {
                customProgressDialog.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }*/
}
