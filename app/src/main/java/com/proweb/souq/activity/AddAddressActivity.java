package com.proweb.souq.activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.dialog.AreaDialog;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.AreaModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomEditText;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class AddAddressActivity extends BaseActivity {

    CustomEditText cedtFirstname, cedtLastname, cedtCity, cedtAddressLine1, cedtAddressLine2, cedtPostcode;
    CustomEditText cedtDistrict, cedtStreetNo, cedtHouseNo;
    Context context;
    TextView txtSaveAddress;
    TextView spinner, spinnerZone;
    String country_id = "";
    String zone_id = "";
    String zone = "";
    AddressModel addressModel;
    RelativeLayout rlSpinner, rlSpinnerZone;
    AreaDialog areaDialog;
    RadioGroup rgDefault, rgAddressType;
    RadioButton rbYes, rbNo, rbHome, rbWork;
    String selectedDefaultOption, selectedAddressType;
    int defaultAddress;
    CustomProgressBar customProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        init();
    }

    private void init() {
        context = this;
        cedtFirstname = findViewById(R.id.cedt_firstname);
        cedtLastname = findViewById(R.id.cedt_lastname);
        cedtCity = findViewById(R.id.cedt_city);
        cedtAddressLine1 = findViewById(R.id.cedt_address_line1);
        cedtAddressLine2 = findViewById(R.id.cedt_address_line2);
        cedtPostcode = findViewById(R.id.cedt_postcode);
        cedtDistrict = findViewById(R.id.cedt_district);
        cedtStreetNo = findViewById(R.id.cedt_street_no);
        cedtHouseNo = findViewById(R.id.cedt_house_no);
        spinner = findViewById(R.id.spinner);
        spinnerZone = findViewById(R.id.spinner_zone);
        rlSpinner = findViewById(R.id.rl_spinner);
        rlSpinnerZone = findViewById(R.id.rl_spinner_zone);
        txtSaveAddress = findViewById(R.id.txt_save_address);
        rgDefault = findViewById(R.id.rg_default);
        rbNo = findViewById(R.id.rb_no);
        rbYes = findViewById(R.id.rb_yes);
        rgAddressType = findViewById(R.id.rg_address_type);
        rbHome = findViewById(R.id.rb_home);
        rbWork = findViewById(R.id.rb_work);
        addressModel = new Gson().fromJson(getIntent().getStringExtra("address_model"),AddressModel.class);
        cedtPostcode.setInputType(InputType.TYPE_CLASS_NUMBER);
        cedtCity.setInputType(InputType.TYPE_CLASS_TEXT);
        cedtDistrict.setInputType(InputType.TYPE_CLASS_TEXT);

        customProgressBar = new CustomProgressBar(context);

        //Method calling
        setToolBarTitleWithBack("");
        setListeners();
        autoFill();
        if (addressModel!=null){
            setData();
        }
    }

    private void autoFill() {
        cedtFirstname.setText(SessionManager.getInstance().getUserModel(context).getFirstname());
        cedtLastname.setText(SessionManager.getInstance().getUserModel(context).getLastname());
    }

    private void setListeners() {
        txtSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    if (addressModel!=null){
                        editAddressAPI();
                    }else {
                        addAddressAPI();
                    }
                }
            }
        });

        rlSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryDialog();
            }
        });

        rlSpinnerZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showZoneDialog();
            }
        });

        //Address default actions
        rgDefault.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = group.findViewById(checkedId);
                selectedDefaultOption = radioButton.getText().toString();
                if (selectedDefaultOption.equalsIgnoreCase(getString(R.string.lbl_yes))){
                    defaultAddress = 1;
                }else {
                    defaultAddress = 0;
                }
                //Toast.makeText(context, radioButton.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        //Address type
        rgAddressType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = group.findViewById(checkedId);
                selectedAddressType = radioButton.getText().toString();
                if (selectedAddressType.equalsIgnoreCase(getString(R.string.lbl_home))){
                    //defaultAddress = 1;
                }else {
                    //defaultAddress = 0;
                }
                Toast.makeText(context, radioButton.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * FUNCTIONALITY
     */
    private void showCountryDialog() {
        areaDialog = new AreaDialog(context,0, getString(R.string.lbl_select_country), new AreaDialog.AreaDialogInterface() {
            @Override
            public void onItemSelected(AreaModel areaModel) {
                country_id = areaModel.getId();
                spinner.setText(areaModel.getName());
                //showZoneDialog();
            }
        });
        areaDialog.setCancelable(false);
        areaDialog.show();
    }

    private void showZoneDialog() {

        if (country_id.equalsIgnoreCase(""))
        {
            Toast.makeText(context, getString(R.string.err_select_country), Toast.LENGTH_SHORT).show();
            return;
        }

       areaDialog = new AreaDialog(context,Integer.parseInt(country_id), getString(R.string.lbl_select_zone), new AreaDialog.AreaDialogInterface() {
            @Override
            public void onItemSelected(AreaModel areaModel) {
                zone_id = areaModel.getId();
                zone = areaModel.getName();
                spinnerZone.setText(areaModel.getName());
            }
        });
        areaDialog.setCancelable(false);
        areaDialog.show();
    }

    private void setData() {
        country_id = addressModel.getCountry_id();
        zone_id = addressModel.getZone_id();
        zone = addressModel.getZone();
        cedtFirstname.setText(addressModel.getFirstname());
        cedtLastname.setText(addressModel.getLastname());
        cedtCity.setText(addressModel.getCity());
        cedtAddressLine1.setText(addressModel.getAddress_1());
        cedtAddressLine2.setText(addressModel.getAddress_2());
        cedtPostcode.setText(addressModel.getPostcode());
        cedtDistrict.setText(addressModel.getCustom_field().getDistrict());
        cedtStreetNo.setText(addressModel.getCustom_field().getStreet());
        cedtHouseNo.setText(addressModel.getCustom_field().getHouse());
        spinner.setText(addressModel.getCountry());
        spinnerZone.setText(addressModel.getZone());
        if (addressModel.isPrimary()){
              rbYes.setChecked(true);
              defaultAddress = 1;
        }else {
              rbNo.setChecked(true);
              defaultAddress = 0;
        }
    }

    public boolean isValid(){
        if (cedtFirstname.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_firstname), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtFirstname.getText().toString().length() > 32){
            Toast.makeText(context, getString(R.string.err_firstname_length), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtLastname.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_lastname), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtLastname.getText().toString().length() > 32) {
            Toast.makeText(context, getString(R.string.err_lastname_length), Toast.LENGTH_SHORT).show();
            return false;
        }else if (country_id.equalsIgnoreCase("")){
            Toast.makeText(context, getString(R.string.err_country), Toast.LENGTH_SHORT).show();
            return false;
        }else if (zone_id.equalsIgnoreCase("")){
            Toast.makeText(context, getString(R.string.err_zone), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtCity.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_city), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtDistrict.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_district), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtStreetNo.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_street_no), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtHouseNo.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_house_no), Toast.LENGTH_SHORT).show();
            return false;
        }else if (cedtAddressLine1.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_area), Toast.LENGTH_SHORT).show();
            return false;
        }/*else if (cedtAddressLine2.getText().isEmpty()){
            Toast.makeText(context, getString(R.string.err_area), Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }

    /**
     * API CALLING
     */
    private void addAddressAPI() {
        customProgressBar.show();
        String url;
        if (addressModel!=null) {
            url = AppConstant.ADD_ADDRESS_API + "/" + addressModel.getAddress_id();
        }else {
            url = AppConstant.ADD_ADDRESS_API;
        }
        JSONObject paramCustom = new JSONObject();
        try {
            paramCustom.put("2",cedtDistrict.getText().toString()); //District
            paramCustom.put("3",zone); //Area
            paramCustom.put("4",cedtStreetNo.getText().toString()); //Street No
            paramCustom.put("5",cedtHouseNo.getText().toString()); //House No
            paramCustom.put("6","Nearest Landmark");
            //paramCustom.put("8",selectedDefaultOption);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject params = new JSONObject();
        try {
            params.put("firstname", cedtFirstname.getText().toString());
            params.put("lastname", cedtLastname.getText().toString());
            params.put("city", cedtCity.getText().toString());
            params.put("address_1", cedtAddressLine1.getText().toString());
            params.put("address_2", cedtAddressLine2.getText().toString());
            params.put("country_id", country_id);
            params.put("postcode", cedtPostcode.getText().toString());
            params.put("zone_id",zone_id );
            params.put("company", "Demo Address");
            params.put("custom_field", new JSONObject().put("address",paramCustom));
            params.put("default", defaultAddress);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(url, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ///Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                AppConstant.SHOULD_RELOAD = true;
                customProgressBar.dismiss();
                setResult(RESULT_OK, new Intent());
                finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                customProgressBar.dismiss();
                Log.e("TAG",error);
            }
        });

    }

    private void editAddressAPI() {
        customProgressBar.show();
        String url = AppConstant.ADD_ADDRESS_API + "/" + addressModel.getAddress_id();
        JSONObject paramCustom = new JSONObject();
        try {
            paramCustom.put("2",cedtDistrict.getText().toString()); //District
            paramCustom.put("3",zone); //Area
            paramCustom.put("4",cedtStreetNo.getText().toString()); //Street No
            paramCustom.put("5",cedtHouseNo.getText().toString()); //House No
            paramCustom.put("6","Nearest Landmark");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject params = new JSONObject();
        try {
            params.put("firstname", cedtFirstname.getText().toString());
            params.put("lastname", cedtLastname.getText().toString());
            params.put("city", cedtCity.getText().toString());
            params.put("address_1", cedtAddressLine1.getText().toString());
            params.put("address_2", cedtAddressLine2.getText().toString());
            params.put("country_id", country_id);
            params.put("postcode", cedtPostcode.getText().toString());
            params.put("zone_id",zone_id );
            params.put("company", "Demo Address");
            params.put("custom_field", new JSONObject().put("address",paramCustom));
            params.put("default", defaultAddress);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().putAPI(url, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK, new Intent());
                finish();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                customProgressBar.dismiss();
                Log.e("TAG",error);
            }
        });

    }

}
