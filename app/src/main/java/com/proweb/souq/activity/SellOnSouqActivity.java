package com.proweb.souq.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.proweb.souq.R;

public class SellOnSouqActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_on_souq);
        init();
    }

    private void init() {
        setToolBarTitleWithBack("");
    }
}
