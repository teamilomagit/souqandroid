package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.utils.SessionManager;

public class HelpActivity extends BaseActivity {

    private Context context;
    private TextView txtMyAccount, txtMyOrders;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        init();
    }

    private void init() {
        context = this;
        txtMyAccount = findViewById(R.id.txt_my_account);
        txtMyOrders = findViewById(R.id.txt_my_orders);
        sessionManager = new SessionManager();
        //super.initBase();
        setToolBarTitleWithBack("");
        setListeners();
    }

    private void setListeners() {

        txtMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getUserModel(context) != null) {
                    startActivity(new Intent(context, MyAccountActivity.class));
                } else {
                    startActivity(new Intent(context, LoginActivity.class));
                }
            }
        });

        txtMyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getUserModel(context) != null) {
                    startActivity(new Intent(context, OrderHistoryActivity.class));
                } else {
                    startActivity(new Intent(context, LoginActivity.class));
                }
            }
        });

    }
}
