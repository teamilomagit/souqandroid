package com.proweb.souq.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.FilterGroupOptionAdapter;
import com.proweb.souq.model.FilterGroupsModel;
import com.proweb.souq.model.FilterModel;

import java.util.ArrayList;
import java.util.Arrays;

public class FilterOptionsActivity extends BaseActivity {

    Context context;
    ArrayList<FilterModel> list;
    FilterGroupsModel filterGroupsModel;
    FilterGroupOptionAdapter mAdapter;
    RecyclerView mRecyclerview;
    String optionIds;
    TextView txtApply;
    EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_options);
        //super.initBase();
        init();
    }

    private void init() {
        context = this;
        mRecyclerview = findViewById(R.id.recycler_view);
        txtApply = findViewById(R.id.txt_apply);
        edtSearch = findViewById(R.id.edt_search);
        list = new ArrayList<>();
        filterGroupsModel = new Gson().fromJson(getIntent().getStringExtra("filter_model"),FilterGroupsModel.class);
        optionIds = getIntent().getStringExtra("option_ids");

        //Method calling
        setToolBarTitleWithBack("");
        setListeners();
        setUpRecyclerview();
        getOptionsList();
    }

    private void setListeners() {
        txtApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = getIntent().getIntExtra("position",0);
                Intent data = new Intent();
                data.putParcelableArrayListExtra("filter_options",mAdapter.getSelectedOptions());
                data.putExtra("position", position);
                setResult(Activity.RESULT_OK,data);
                finish();
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<FilterModel> tempArray = new ArrayList<>();
                for (FilterModel filterModel:list) {
                    if (filterModel.getName().contains(s)) {
                        tempArray.add(filterModel);
                    }else {
                        tempArray.remove(filterModel);
                    }
                }
                mAdapter.updateAdapter(tempArray);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUpRecyclerview() {
        mAdapter = new FilterGroupOptionAdapter(context,list,optionIds);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.setAdapter(mAdapter);
    }

    private void getOptionsList() {
        list = new ArrayList<>(Arrays.asList(filterGroupsModel.getFilter()));
        mAdapter.updateAdapter(list);
    }
}
