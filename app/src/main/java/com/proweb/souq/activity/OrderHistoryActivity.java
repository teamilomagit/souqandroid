package com.proweb.souq.activity;

import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.adapter.OrderHistoryAdapter;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.PaginationScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;

public class OrderHistoryActivity extends BaseActivity {
    private Context context;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private OrderHistoryAdapter mAdapter;
    TextView txtNoData;
    ArrayList<OrderHistoryModel> list;

    // PAGINATION VARS
    private static final int PAGE_START = 1;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;
    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;
    // indicates the current page which Pagination is fetching.
    private int currentPage = PAGE_START;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        init();
    }

    private void init() {
        context = this;
        mRecyclerView = findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        txtNoData = findViewById(R.id.txt_no_data);
        list = new ArrayList<>();

        //Method calling
        setUpRecyclerView();
        setToolBarTitleWithBack(getString(R.string.title_order_history));
    }


    private void setUpRecyclerView() {
        mAdapter = new OrderHistoryAdapter(context, new ArrayList<OrderHistoryModel>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        // On scroll listener
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getOrdersListAPI();
                    }
                }, 500);
            }

            @Override
            public int getTotalPageCount() {
                return 99999;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        // mocking network delay for API call
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getOrdersListAPI();
            }
        }, 500);

        // On swipe refresh
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
    }

    private void refreshItems() {
        // To clear already set adapter and new menuList on swipe refresh
        mAdapter.clear();
        currentPage = PAGE_START;
        isLastPage = false;
        getOrdersListAPI();
    }

    /**
     * API CALLING
     */
    public void getOrdersListAPI() {
        mSwipeRefreshLayout.setRefreshing(true);
        String url = AppConstant.GET_ORDERS_LIST_API + "limit/" + AppConstant.PAGINATION_LIMIT + "/page/" + currentPage;
        new APIManager().getJSONArrayAPI(url, null, OrderHistoryModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                if (currentPage == PAGE_START) {
                    mAdapter.clear();
                }

                mSwipeRefreshLayout.setRefreshing(false);
                mAdapter.removeLoadingFooter();
                isLoading = false;

                list = (ArrayList<OrderHistoryModel>) resultObj;

                // if menuList<10 then last page true
                if (list.size() < 10) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    mAdapter.addAll(list);
                }

                if (mAdapter.list.size() > 0) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }


                // Add loading footer if last page is false
                if (!isLastPage) {
                    mAdapter.addLoadingFooter();
                }
            }

            @Override
            public void onError(String error) {
                Log.e("TAG", error);
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstant.SHOULD_RELOAD) {
            AppConstant.SHOULD_RELOAD = false;
            refreshItems();
        }
    }
}
