package com.proweb.souq.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.CustomProgressBar;
import com.proweb.souq.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity {

    Context context;
    private TextView txtSignIn, txtSignUp;
    private EditText edtName, edtEmail, edtPassword, edtMobile, edtConfirmPass, edtLastName;
    private ProgressBar progressBar;
    private CheckBox checkBox;
    RadioGroup radioGroup;
    RadioButton radioButton1, radioButton2;
    String genderVal = "1";
    ImageView imgClose;
    CustomProgressBar customProgressBar;
    TextView txtPrivacyPolicy;
    RelativeLayout rlTxtPrivacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    private void init() {
        context = this;
        txtSignIn = findViewById(R.id.txt_sign_in);
        txtSignUp = findViewById(R.id.txt_sign_up);
        progressBar = findViewById(R.id.prgress_bar);
        edtConfirmPass = findViewById(R.id.edt_confirm_password);
        edtEmail = findViewById(R.id.edt_email);
        edtName = findViewById(R.id.edt_name);
        edtMobile = findViewById(R.id.edt_mobile);
        edtPassword = findViewById(R.id.edt_password);
        edtLastName = findViewById(R.id.edt_last_name);
        checkBox = findViewById(R.id.chk_privacy);
        radioButton1 = findViewById(R.id.radio1);
        radioButton2 = findViewById(R.id.radio2);
        radioGroup = findViewById(R.id.radiogrp);
        imgClose = findViewById(R.id.img_close);
        txtPrivacyPolicy = findViewById(R.id.txt_privacy_policy);
        rlTxtPrivacyPolicy = findViewById(R.id.rl_txt_privacy_policy);
        customProgressBar = new CustomProgressBar(context);
        edtMobile.setInputType(InputType.TYPE_CLASS_NUMBER);

        clickListener();

    }

    private void clickListener() {
        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    signUpAPI();
                }

            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rlTxtPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_privacy_policy)));
            }
        });

        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderVal = "1";
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderVal = "2";
            }
        });
    }

    private void signUpAPI() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {

            JSONObject customField = new JSONObject();
            JSONObject account = new JSONObject();
            account.put("1", genderVal);
            customField.put("account", account);

            params.put("firstname", edtName.getText().toString());
            params.put("lastname", edtLastName.getText().toString());
            params.put("email", edtEmail.getText().toString());
            params.put("password", edtPassword.getText().toString());
            params.put("confirm", edtConfirmPass.getText().toString());
            params.put("telephone", edtMobile.getText().toString());
            params.put("customer_group_id", null);
            params.put("agree", 1);
            params.put("custom_field", customField);
            params.put("account", null);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.REGISTRATION_API, params, UserModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                Toast.makeText(context, getString(R.string.msg_register_success), Toast.LENGTH_SHORT).show();
                UserModel userModel = (UserModel) resultObj;
                SessionManager.getInstance().createLoginSession(userModel, context);
                startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                //progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                //progressBar.setVisibility(View.GONE);
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isValidate() {

        if (edtName.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_firstname), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtName.getText().toString().length() > 32) {
            Toast.makeText(context, getString(R.string.err_firstname_length), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtLastName.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_lastname), Toast.LENGTH_SHORT).show();
            return false;

        } else if (edtLastName.getText().toString().length() > 32) {
            Toast.makeText(context, getString(R.string.err_lastname_length), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtEmail.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_email_address), Toast.LENGTH_SHORT).show();
            return false;

        } else if (!AppUtils.isValidEmail(edtEmail.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.err_valid_email), Toast.LENGTH_SHORT).show();
            return false;

        } else if (edtMobile.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_mobile_number), Toast.LENGTH_SHORT).show();
            return false;

        } /*else if (edtMobile.getText().toString().length() < 10) {
            Toast.makeText(context, getString(R.string.err_valid_mobile), Toast.LENGTH_SHORT).show();
            return false;

        }*/ else if (edtPassword.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_password), Toast.LENGTH_SHORT).show();
            return false;

        } else if (edtPassword.getText().toString().length() < 5) {
            Toast.makeText(context, getString(R.string.err_pass), Toast.LENGTH_SHORT).show();
            return false;

        } else if (edtConfirmPass.getText().toString().length() == 0) {
            Toast.makeText(context, getString(R.string.err_confirm_password), Toast.LENGTH_SHORT).show();
            return false;

        }/*else if (edtConfirmPass.getText().toString().length() < 5){
            Toast.makeText(context, getString(R.string.err_confirm_pass), Toast.LENGTH_SHORT).show();
            return false;
        }*/ else if (!edtPassword.getText().toString().equalsIgnoreCase(edtConfirmPass.getText().toString())) {
            Toast.makeText(context, getString(R.string.err_password_mismatch), Toast.LENGTH_SHORT).show();
            return false;
        } else if (checkBox.isChecked() == false) {
            Toast.makeText(context, getString(R.string.err_privacy), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
