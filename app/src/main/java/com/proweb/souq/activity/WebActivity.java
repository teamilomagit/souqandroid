package com.proweb.souq.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.proweb.souq.R;
import com.proweb.souq.model.WebModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import org.json.JSONObject;

import java.util.ArrayList;

public class WebActivity extends BaseActivity {

    WebView mWebView;
    String url;
    ProgressBar progressbar;
    boolean loadingFinished = true;
    boolean redirect = false;
    int info_id = 0;
    String screen_title;
    WebModel webModel;
    Context context;
    ArrayList<WebModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_web, null, false);
        containerView.addView(contentView, 0);
        init();
    }

    private void init() {
        context = this;
        mWebView = findViewById(R.id.web_view);
        progressbar = findViewById(R.id.progressbar);
        list = new ArrayList<>();

        //Method calling
        setToolBarTitleWithBack("");
        getWebPagesAPI();
    }

    /**
     * FUNCTIONALITY
     */
    private void setInfoId() {

        for (WebModel model :
             list) {
            if (getIntent().getStringExtra("title").equalsIgnoreCase(model.getTitle())){
                info_id = Integer.parseInt(model.getId());
                screen_title = model.getTitle();
            }
        }

        if (info_id > 0) {
            getInformationById();
        }
    }

    private void setWebView() {
        url = webModel.getDescription();

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                if (!loadingFinished) {
                    redirect = true;
                }
                loadingFinished = false;
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                loadingFinished = false;
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                progressbar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (!redirect) {
                    loadingFinished = true;
                }

                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    progressbar.setVisibility(View.GONE);
                } else {
                    redirect = false;
                }

            }
        });
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.clearCache(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        String replaced = url.replace("&lt;","<");
        String replaced2 = replaced.replace("&gt;", ">");
        replaced2 = replaced2.replace("&amp;", "&");
        String newBody = "<html><body>" +  "<h3>" + screen_title + "</h3>" +  replaced2 + "</body></html>";
        mWebView.loadDataWithBaseURL(null, newBody, "text/html", "utf-8", null);

    }

    /**
     * API CALLING
     */
    private void getWebPagesAPI() {
        new APIManager().getJSONArrayAPI(AppConstant.GET_WEB_INFO, null, WebModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                list = (ArrayList<WebModel>) resultObj;
                setInfoId();
            }

            @Override
            public void onError(String error) {
                 Log.e("TAG",error);
            }
        });
    }

    public void getInformationById(){
        String url = AppConstant.GET_WEB_INFO + "/" + info_id;
        new APIManager().getAPI(url, WebModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                webModel = (WebModel) resultObj;
                setWebView();
            }

            @Override
            public void onError(String error) {
                Log.e("TAG",error);
            }
        });
    }
}