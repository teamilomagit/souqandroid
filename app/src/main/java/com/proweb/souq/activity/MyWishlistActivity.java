package com.proweb.souq.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.adapter.MyWishListAdapter;
import com.proweb.souq.adapter.OrderHistoryAdapter;
import com.proweb.souq.model.CartModel;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.model.WishListModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyWishlistActivity extends BaseActivity {
    private Context context;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private MyWishListAdapter mAdapter;
    ArrayList<WishListModel> list;
    CustomProgressBar progressBar;
    TextView txtNoData;
    CustomProgressBar loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wishlist);
        setToolBarTitleWithBack(getString(R.string.title_my_whishlist));
        init();
    }

    private void init() {
        context = this;
        mRecyclerView = findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        txtNoData = findViewById(R.id.txt_no_data);
        list = new ArrayList<>();
        progressBar = new CustomProgressBar(context);
        loader = new CustomProgressBar(context);

        //Method calling
        setUpRecyclerView();
        getWishList();
    }

    private void setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mAdapter = new MyWishListAdapter(context, new ArrayList<WishListModel>(), new MyWishListAdapter.MyWishListAdapterInterface() {
            @Override
            public void onRemove(WishListModel wishListModel) {
                removeFromWishlist(wishListModel);
            }

            @Override
            public void addToCart(WishListModel wishListModel) {
                //addToCartAPI(wishListModel);
                context.startActivity(new Intent(context, ItemDetailActivity.class).putExtra("wishlist_model", new Gson().toJson(wishListModel))
                        .putExtra("flag",true));
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getWishList();
            }
        });
    }

    /**
     * API CALLING
     */
    private void getWishList() {
        mSwipeRefreshLayout.setRefreshing(true);
        new APIManager().getJSONArrayAPI(AppConstant.GET_WISHLIST_API, null, WishListModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                mSwipeRefreshLayout.setRefreshing(false);
                list = (ArrayList<WishListModel>) resultObj;
                if (list.size() > 0) {
                    mAdapter.updateAdapter(list);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(String error) {
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void removeFromWishlist(final WishListModel wishListModel) {
        progressBar.show();

        String url = AppConstant.GET_WISHLIST_API + "/" + wishListModel.getProduct_id();
        new APIManager().deleteAPI(url, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressBar.dismiss();
                Toast.makeText(context, getString(R.string.alert_success_wishlist_remove), Toast.LENGTH_SHORT).show();
                list.remove(wishListModel);
                if (list.size() > 0) {
                    mAdapter.updateAdapter(list);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(String error) {
                progressBar.dismiss();
                mRecyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
                Log.e("TAG", error);
            }
        });
    }

    private void addToCartAPI(WishListModel wishListModel) {
        loader.show();
        JSONObject params = new JSONObject();
        final AppPreference appPreference = new AppPreference(context);
        try {
            params.put("product_id", wishListModel.getProduct_id());
            params.put("quantity", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().postAPI(AppConstant.ADD_TO_CART_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                loader.dismiss();
                Toast.makeText(context, getString(R.string.msg_add_cart), Toast.LENGTH_SHORT).show();
                getViewCartListAPI();
                //showAddToCartDialog(productModel);
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
            }
        });
    }

    //get cart item count
    private void getViewCartListAPI() {

        new APIManager().getAPI(AppConstant.GET_CART_API, CartModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                CartModel cartModel = (CartModel) resultObj;
                new AppPreference(context).setCartCount(cartModel.getTotal_product_count());
                updateCartCount();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
