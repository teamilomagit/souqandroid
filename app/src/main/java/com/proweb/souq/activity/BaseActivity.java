package com.proweb.souq.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proweb.souq.R;
import com.proweb.souq.adapter.MenuAdapter;
import com.proweb.souq.adapter.SearchAdapter;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.dialog.SellOnSouqDialog;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.model.UserModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.SessionManager;
import com.proweb.souq.utils.SetLocalLanguage;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

public class BaseActivity extends AppCompatActivity {
    Context context;
    private TextView txtTitle, txtDrawerTitle;
    private android.support.v4.app.FragmentManager fragmentManager;
    protected ImageView imgMenu, imgCart, imgSearch, imgBack;
    protected FrameLayout containerView;
    DrawerLayout mDrawerLayout;
    boolean mSlideState = false;
    static NavigationView navView;
    protected ArrayList<CategoryModel> menuList;
    private ImageView imgClose;
    private TextView txtTerms, txtReturnPolicy, txtPrivacyPolicy, txtWarrantyPolicy, txtFaq, txtSignIn, txtSignUp, txtHome,
            txtCart, txtLogout, txtHome2, txtCart2, txtWishList, txtContactUs, txtDelivery, txtSellSouq;
    TextView txtMenuAccount;
    RecyclerView mRecyclerView, mRecyclerViewSearch;
    RelativeLayout rlSearch, rlFb, rlInsta, rlYoutube, rlSnapchat, rlLanguage;
    MenuAdapter mAdapter;
    private ProgressBar progressBar;
    LinearLayout row2, row3, row4;
    UserModel userModel;
    EditText edtSearch;
    SearchAdapter searchAdapter;
    ImageView imgLogo;
    TextView txtCartCounter;
    CustomAlertDialog dialog;
    TextView txtLanguageValue, txtHelp;
    SellOnSouqDialog sellOnSouqDialog;
    TextView txtRateUs, txtNoData;
    ImageView imgDrawerLogo;

    private static final String TAG = BaseActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        containerView = findViewById(R.id.content_frame);

        String language = new AppPreference(getApplicationContext()).getAppLanguage();
        if (language != null) {
            if (!language.equalsIgnoreCase("en-gb")) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            } else {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }
    }

    protected void initBase() {
        context = this;
        txtTitle = findViewById(R.id.txt_title);
        txtDelivery = findViewById(R.id.txt_delivery);
        txtDrawerTitle = findViewById(R.id.txt_drawer_title);
        imgCart = findViewById(R.id.img_cart);
        imgMenu = findViewById(R.id.img_menu);
        imgBack = findViewById(R.id.img_back);
        imgSearch = findViewById(R.id.img_search);
        imgLogo = findViewById(R.id.img_logo);
        imgDrawerLogo = findViewById(R.id.img_drawer_logo);
        txtLanguageValue = findViewById(R.id.txt_language_value);
        txtHelp = findViewById(R.id.txt_help);
        txtRateUs = findViewById(R.id.txt_rate_us);
        fragmentManager = getSupportFragmentManager();

        menuList = new ArrayList<>();
        imgClose = findViewById(R.id.img_close);
        txtFaq = findViewById(R.id.txt_faq);
        txtTerms = findViewById(R.id.txt_terms);
        txtWarrantyPolicy = findViewById(R.id.txt_warrenty_policy);
        txtPrivacyPolicy = findViewById(R.id.txt_privacy_policy);
        txtReturnPolicy = findViewById(R.id.txt_return_policy);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        txtSignIn = findViewById(R.id.txt_signin);
        txtSignUp = findViewById(R.id.txt_signup);
        txtHome = findViewById(R.id.txt_home);
        txtCart = findViewById(R.id.txt_cart_drawer);
        txtCart2 = findViewById(R.id.txt_cart_drawer_2);
        txtHome2 = findViewById(R.id.txt_home_2);
        txtWishList = findViewById(R.id.txt_menu_wishlist);
        txtLogout = findViewById(R.id.txt_logout);
        txtSellSouq = findViewById(R.id.txt_sell_souq);
        progressBar = findViewById(R.id.progressbar);
        txtMenuAccount = findViewById(R.id.txt_menu_account);
        txtContactUs = findViewById(R.id.txt_contact_us);
        txtCartCounter = findViewById(R.id.txt_cart_counter);

        row2 = findViewById(R.id.row2);
        row3 = findViewById(R.id.row3);
        row4 = findViewById(R.id.row4);
        rlSearch = findViewById(R.id.rl_search);
        rlFb = findViewById(R.id.rl_fb);
        rlInsta = findViewById(R.id.rl_insta);
        rlYoutube = findViewById(R.id.rl_youtube);
        rlSnapchat = findViewById(R.id.rl_snapchat);
        rlLanguage = findViewById(R.id.rl_language);
        edtSearch = findViewById(R.id.edt_search);

       /* if (SessionManager.getInstance().isLoggedIn(context)) {
            row2.setVisibility(View.GONE);
            row3.setVisibility(View.VISIBLE);
        } else {
            row2.setVisibility(View.VISIBLE);
            row3.setVisibility(View.GONE);
            txtLogout.setVisibility(View.GONE);
        }
*/

        checkForloggedInUser();

        if (context instanceof HomeActivity) {
            imgBack.setVisibility(View.GONE);
            imgSearch.setVisibility(View.GONE);
            rlSearch.setVisibility(View.VISIBLE);
        } else {
            if (rlSearch != null) {
                rlSearch.setVisibility(View.GONE);
            }
        }

        if (context instanceof ViewCartActivity) {
            imgCart.setVisibility(View.GONE);
            txtCartCounter.setVisibility(View.GONE);
        }

        if (new AppPreference(context).getAppLanguage() != null) {
            if (new AppPreference(context).getAppLanguage().equalsIgnoreCase("ar")) {
                txtLanguageValue.setText(getString(R.string.english));
            } else {
                txtLanguageValue.setText(getString(R.string.arabic));
            }
        } else {
            txtLanguageValue.setText(getString(R.string.arabic));
        }

        //Method calling
        setUpRecyclerView();
        setupSearchRecyclerView();
        setClickListener();
        setDrawer();
        //setLanguage();
    }

    protected void checkForloggedInUser() {
        if (SessionManager.getInstance().isLoggedIn(context)) {
            row4.setVisibility(View.VISIBLE);
            callHeader(context);
            row2.setVisibility(View.GONE);
            row3.setVisibility(View.VISIBLE);
            txtLogout.setVisibility(View.VISIBLE);
        } else {
            row2.setVisibility(View.VISIBLE);
            row3.setVisibility(View.GONE);
            txtLogout.setVisibility(View.GONE);
        }
    }

    private void setClickListener() {

        txtRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=&hl=en"));
                startActivity(browserIntent);
            }
        });

        txtHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, HelpActivity.class));
            }
        });

        rlLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, txtLanguageValue.getText().toString(), Toast.LENGTH_SHORT).show();
                if (txtLanguageValue.getText().toString().equalsIgnoreCase(getString(R.string.arabic))) {
                    SetLocalLanguage.setLocaleLanguage(context, "ar");
                    new AppPreference(context).setAppLanguage("ar");
                    txtLanguageValue.setText(getString(R.string.english));
                } else {
                    SetLocalLanguage.setLocaleLanguage(context, "en-gb");
                    new AppPreference(context).setAppLanguage("en-gb");
                    txtLanguageValue.setText(getString(R.string.arabic));
                }
                restartApp();
                //startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                //SetLocalLanguage.setLocaleLanguage(context,);
            }
        });

        txtDrawerTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof HomeActivity) {
                    clickEventSlide();
                } else {
                    startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            }
        });

        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEventSlide();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlSearch.getVisibility() == View.VISIBLE) {
                    rlSearch.setVisibility(View.GONE);
                    edtSearch.setText("");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                } else {
                    ((Activity) context).finish();
                }
            }
        });

        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ViewCartActivity.class));
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEventSlide();
            }
        });

        txtWarrantyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_warranty_policy)));
                clickEventSlide();
            }
        });

        txtTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_terms_conditions)));
                clickEventSlide();
            }
        });

        txtDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_delivery_info1)));
                clickEventSlide();
            }
        });

        txtPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_privacy_policy)));
                clickEventSlide();
            }
        });

        txtFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_faq)));
                clickEventSlide();
            }
        });

        txtReturnPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WebActivity.class).putExtra("title", getString(R.string.bottom_return_policy)));
                clickEventSlide();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SignUpActivity.class));
                clickEventSlide();
            }
        });
        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, LoginActivity.class));
                clickEventSlide();
            }
        });
        txtHome2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof HomeActivity) {
                    clickEventSlide();
                } else {
                    startActivity(new Intent(context, HomeActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            }
        });
        txtCart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(context instanceof ViewCartActivity)) {
                    startActivity(new Intent(context, ViewCartActivity.class));
                }

                clickEventSlide();
            }
        });

        txtHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof HomeActivity) {
                    clickEventSlide();
                } else {
                    startActivity(new Intent(context, HomeActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            }
        });
        txtCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(context instanceof ViewCartActivity)) {
                    startActivity(new Intent(context, ViewCartActivity.class));
                }
                clickEventSlide();
            }
        });

        txtMenuAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, MyAccountActivity.class));
                clickEventSlide();
            }
        });
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmLogout();
            }
        });

        txtWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(context instanceof MyWishlistActivity)) {
                    startActivity(new Intent(context, MyWishlistActivity.class));
                }
                clickEventSlide();
            }
        });

        txtContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ContactUsActivity.class));
            }
        });

        txtSellSouq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showSouqDialog();
                startActivity(new Intent(context, SellOnSouqActivity.class));
            }
        });

        rlFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/souq366"));
                startActivity(browserIntent);
            }
        });

        rlInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/souq366"));
                startActivity(browserIntent);
            }
        });

        rlYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCqkHGugdzP2H4Dm23Eb_1tw"));
                startActivity(browserIntent);
            }
        });

        rlSnapchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.snapchat.com/add/souq366.com"));
                startActivity(browserIntent);
            }
        });

        /*imgDrawerLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEventSlide();
                startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });*/
    }

    /**
     * RECYCLER AND SEARCH
     */
    private void setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_base);

        Type type = new TypeToken<ArrayList<CategoryModel>>() {
        }.getType();
        menuList = new Gson().fromJson(new AppPreference(context).getCategoryList(), type);

        mAdapter = new MenuAdapter(context, menuList, new MenuAdapter.MenuAdapterInterface() {
            @Override
            public void getItemClicked() {
                clickEventSlide();
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setSearchListener() {

        edtSearch = findViewById(R.id.edt_search);
        txtNoData = findViewById(R.id.txt_no_data);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getSearchData(s.toString());

                if (s.toString().length() == 0) {
                    mRecyclerViewSearch.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.GONE);
                } else {
                    mRecyclerViewSearch.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rlSearch.getVisibility() == View.VISIBLE) {
                    rlSearch.setVisibility(View.GONE);
                    mRecyclerViewSearch.setVisibility(View.GONE);
                } else {
                    rlSearch.setVisibility(View.VISIBLE);
                    mRecyclerViewSearch.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setupSearchRecyclerView() {
        mRecyclerViewSearch = findViewById(R.id.recycler_view_search);
        if (mRecyclerViewSearch != null) {
            searchAdapter = new SearchAdapter(context, new ArrayList<ProductModel>());
            mRecyclerViewSearch.setLayoutManager(new LinearLayoutManager(context));
            mRecyclerViewSearch.setAdapter(searchAdapter);

            setSearchListener();
        }
    }

    /**
     * COUNT SECTION
     */
    protected void updateCartCount() {
        if (txtCartCounter != null && !(context instanceof ViewCartActivity)) {
            int count = new AppPreference(context).getCartCount();
            if (count == 0) {
                txtCartCounter.setVisibility(View.GONE);
            } else {
                txtCartCounter.setVisibility(View.VISIBLE);
            }
            txtCartCounter.setText(count + "");
        }
    }

    /**
     * TOOLBAR AND HEADERS
     */
    protected void setHomeToolBar() {
        txtTitle.setVisibility(View.GONE);
        imgLogo.setVisibility(View.VISIBLE);
        updateCartCount();
    }

    protected void setToolBarTitle(String title) {
        ImageView imgLogo = findViewById(R.id.img_logo);
        txtTitle.setVisibility(View.GONE);
        imgLogo.setVisibility(View.VISIBLE);
        imgMenu.setVisibility(View.GONE);

        if (context instanceof ViewCartActivity) {
            imgSearch.setVisibility(View.INVISIBLE);
        }

        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        updateCartCount();
    }

    protected void setToolBarTitleWithoutCartSearch(String title) {
        txtTitle.setText(title);
        imgCart.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);
        txtCartCounter.setVisibility(View.GONE);
        imgMenu.setVisibility(View.GONE);
    }

    protected void setToolBarTitleWithBack(String title) {
        ImageView imgBack = findViewById(R.id.img_back);
        ImageView imgLogo = findViewById(R.id.img_logo);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
    }

    protected void callHeader(Context context) {
        this.context = context;
        setBasicHeader();
    }

    protected void setBasicHeader() {
        TextView txtHello = findViewById(R.id.txt_hello);
        TextView txtEmailHeader = findViewById(R.id.txt_email_header);
        txtHello.setText(getString(R.string.str_hello) + " " + SessionManager.getInstance().getUserModel(context).getFirstname() + " "
                + SessionManager.getInstance().getUserModel(context).getLastname());
        txtEmailHeader.setText(SessionManager.getInstance().getUserModel(context).getEmail());
    }

    /**
     * FUNCTIONALITY
     */
    private void confirmLogout() {

        dialog = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                dialog.dismiss();
                logoutAPI();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
        dialog.setTitle(getString(R.string.msg_logout));
    }

    protected void setDrawer() {
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mSlideState = true;//is Opened
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                mSlideState = false;//is Closed
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        NavigationView navigationView = findViewById(R.id.nav_view);
        navView = navigationView;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        params.width = metrics.widthPixels;
        navigationView.setLayoutParams(params);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        });
        navigationView.setItemIconTintList(null);

    }

    public void clickEventSlide() {
        if (mSlideState) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    /**
     * API CALLING
     */
    private void logoutAPI() {

        progressBar.setVisibility(View.VISIBLE);
        JSONObject params = new JSONObject();

        new APIManager().postAPI(AppConstant.LOGOUT_API, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                SessionManager.getInstance().logout();
                progressBar.setVisibility(View.GONE);
                row2.setVisibility(View.VISIBLE);
                row3.setVisibility(View.GONE);
                row4.setVisibility(View.GONE);
                txtLogout.setVisibility(View.GONE);
                new AppPreference(context).setCartCount(0);

                if (context instanceof HomeActivity) {
                    updateCartCount();
                    clickEventSlide();
                } else {
                    startActivity(new Intent(context, HomeActivity.class).addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_NEW_TASK));
                }

            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getSearchData(final String value) {
        String url = AppConstant.GET_SEARCHED_PRODUCTS + value + "/limit/7/page/1";
        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ArrayList<ProductModel> list = (ArrayList<ProductModel>) resultObj;
                if(list.size() > 0) {
                    txtNoData.setVisibility(View.GONE);
                }else {
                    if (!value.isEmpty()) {
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }
                searchAdapter.updateAdapter(list);
            }

            @Override
            public void onError(String error) {
                Log.e("TAG", error);
            }
        });
    }

    private void restartApp() {
        startActivity(new Intent(context, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                              | Intent.FLAG_ACTIVITY_CLEAR_TOP
                              | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}








/*private void setUpMenu() {
        CategoryModel model = new CategoryModel();
        model.setMenuName("MOBILES");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("ACCESSORIES");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("TABLETS");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("CAMERAS");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("COMPUTER");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("AUDIO & VIDEO");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("WATCHES");
        menuList.add(model);

        model = new CategoryModel();
        model.setMenuName("GADGETS");
        menuList.add(model);
    }*/