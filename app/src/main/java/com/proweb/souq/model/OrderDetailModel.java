package com.proweb.souq.model;

import java.util.ArrayList;

public class OrderDetailModel {

    private String payment_zone_id;

    private String shipping_city;

    private String shipping_address_1;

    private String order_status_id;

    private String shipping_address_2;

    private String language_id;

    private String shipping_address_format;

    private String payment_lastname;

    private String shipping_method;

    private String payment_postcode;

    private String shipping_iso_code_2;

    private String shipping_iso_code_3;

    private String payment_method;

    private String shipping_lastname;

    private String shipping_company;

    private String invoice_prefix;

    private String payment_zone;

    private String ip;

    private String telephone;

    private String payment_country_id;

    private String lastname;

    private String date_added;

    private String payment_zone_code;

    private String payment_address;

    private String order_id;

    private String shipping_country;

    private String firstname;

    private String payment_address_format;

    private String currency_code;

    private String store_url;

    private ArrayList<ProductModel> products;

    private String payment_iso_code_2;

    private String total;

    private String shipping_country_id;

    private String payment_country;

    private String payment_iso_code_3;

    private String store_name;

    private CurrencyModel currency;

    private String shipping_address;

    private String shipping_firstname;

    private String payment_city;

    private String shipping_postcode;

    private String email;

    private String payment_firstname;

    private String payment_company;

    private String timestamp;

    private String store_id;

    private String shipping_zone_code;

    private String invoice_no;

    private String currency_value;

    private ArrayList<HistoryModel> histories;

    private ArrayList<TotalsModel> totals;

    private String[] vouchers;

    private String payment_address_2;

    private String payment_address_1;

    private String date_modified;

    private String comment;

    private String shipping_zone;

    private String customer_id;

    private String shipping_zone_id;

    private String currency_id;

    public String getPayment_zone_id ()
    {
        return payment_zone_id;
    }

    public void setPayment_zone_id (String payment_zone_id)
    {
        this.payment_zone_id = payment_zone_id;
    }

    public String getShipping_city ()
    {
        return shipping_city;
    }

    public void setShipping_city (String shipping_city)
    {
        this.shipping_city = shipping_city;
    }

    public String getShipping_address_1 ()
    {
        return shipping_address_1;
    }

    public void setShipping_address_1 (String shipping_address_1)
    {
        this.shipping_address_1 = shipping_address_1;
    }

    public String getOrder_status_id ()
    {
        return order_status_id;
    }

    public void setOrder_status_id (String order_status_id)
    {
        this.order_status_id = order_status_id;
    }

    public String getShipping_address_2 ()
    {
        return shipping_address_2;
    }

    public void setShipping_address_2 (String shipping_address_2)
    {
        this.shipping_address_2 = shipping_address_2;
    }

    public String getLanguage_id ()
    {
        return language_id;
    }

    public void setLanguage_id (String language_id)
    {
        this.language_id = language_id;
    }

    public String getShipping_address_format ()
    {
        return shipping_address_format;
    }

    public void setShipping_address_format (String shipping_address_format)
    {
        this.shipping_address_format = shipping_address_format;
    }

    public String getPayment_lastname ()
    {
        return payment_lastname;
    }

    public void setPayment_lastname (String payment_lastname)
    {
        this.payment_lastname = payment_lastname;
    }

    public String getShipping_method ()
    {
        return shipping_method;
    }

    public void setShipping_method (String shipping_method)
    {
        this.shipping_method = shipping_method;
    }

    public String getPayment_postcode ()
    {
        return payment_postcode;
    }

    public void setPayment_postcode (String payment_postcode)
    {
        this.payment_postcode = payment_postcode;
    }

    public String getShipping_iso_code_2 ()
    {
        return shipping_iso_code_2;
    }

    public void setShipping_iso_code_2 (String shipping_iso_code_2)
    {
        this.shipping_iso_code_2 = shipping_iso_code_2;
    }

    public String getShipping_iso_code_3 ()
    {
        return shipping_iso_code_3;
    }

    public void setShipping_iso_code_3 (String shipping_iso_code_3)
    {
        this.shipping_iso_code_3 = shipping_iso_code_3;
    }

    public String getPayment_method ()
    {
        return payment_method;
    }

    public void setPayment_method (String payment_method)
    {
        this.payment_method = payment_method;
    }

    public String getShipping_lastname ()
    {
        return shipping_lastname;
    }

    public void setShipping_lastname (String shipping_lastname)
    {
        this.shipping_lastname = shipping_lastname;
    }

    public String getShipping_company ()
    {
        return shipping_company;
    }

    public void setShipping_company (String shipping_company)
    {
        this.shipping_company = shipping_company;
    }

    public String getInvoice_prefix ()
    {
        return invoice_prefix;
    }

    public void setInvoice_prefix (String invoice_prefix)
    {
        this.invoice_prefix = invoice_prefix;
    }

    public String getPayment_zone ()
    {
        return payment_zone;
    }

    public void setPayment_zone (String payment_zone)
    {
        this.payment_zone = payment_zone;
    }

    public String getIp ()
    {
        return ip;
    }

    public void setIp (String ip)
    {
        this.ip = ip;
    }

    public String getTelephone ()
    {
        return telephone;
    }

    public void setTelephone (String telephone)
    {
        this.telephone = telephone;
    }

    public String getPayment_country_id ()
    {
        return payment_country_id;
    }

    public void setPayment_country_id (String payment_country_id)
    {
        this.payment_country_id = payment_country_id;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getPayment_zone_code ()
    {
        return payment_zone_code;
    }

    public void setPayment_zone_code (String payment_zone_code)
    {
        this.payment_zone_code = payment_zone_code;
    }

    public String getPayment_address ()
    {
        return payment_address;
    }

    public void setPayment_address (String payment_address)
    {
        this.payment_address = payment_address;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

    public String getShipping_country ()
    {
        return shipping_country;
    }

    public void setShipping_country (String shipping_country)
    {
        this.shipping_country = shipping_country;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getPayment_address_format ()
    {
        return payment_address_format;
    }

    public void setPayment_address_format (String payment_address_format)
    {
        this.payment_address_format = payment_address_format;
    }

    public String getCurrency_code ()
    {
        return currency_code;
    }

    public void setCurrency_code (String currency_code)
    {
        this.currency_code = currency_code;
    }

    public String getStore_url ()
    {
        return store_url;
    }

    public void setStore_url (String store_url)
    {
        this.store_url = store_url;
    }

    public ArrayList<ProductModel> getProducts ()
    {
        return products;
    }

    public void setProducts (ArrayList<ProductModel> products)
    {
        this.products = products;
    }

    public String getPayment_iso_code_2 ()
    {
        return payment_iso_code_2;
    }

    public void setPayment_iso_code_2 (String payment_iso_code_2)
    {
        this.payment_iso_code_2 = payment_iso_code_2;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getShipping_country_id ()
    {
        return shipping_country_id;
    }

    public void setShipping_country_id (String shipping_country_id)
    {
        this.shipping_country_id = shipping_country_id;
    }

    public String getPayment_country ()
    {
        return payment_country;
    }

    public void setPayment_country (String payment_country)
    {
        this.payment_country = payment_country;
    }

    public String getPayment_iso_code_3 ()
    {
        return payment_iso_code_3;
    }

    public void setPayment_iso_code_3 (String payment_iso_code_3)
    {
        this.payment_iso_code_3 = payment_iso_code_3;
    }

    public String getStore_name ()
    {
        return store_name;
    }

    public void setStore_name (String store_name)
    {
        this.store_name = store_name;
    }

    public CurrencyModel getCurrency ()
    {
        return currency;
    }

    public void setCurrency (CurrencyModel currency)
    {
        this.currency = currency;
    }

    public String getShipping_address ()
    {
        return shipping_address;
    }

    public void setShipping_address (String shipping_address)
    {
        this.shipping_address = shipping_address;
    }

    public String getShipping_firstname ()
    {
        return shipping_firstname;
    }

    public void setShipping_firstname (String shipping_firstname)
    {
        this.shipping_firstname = shipping_firstname;
    }

    public String getPayment_city ()
    {
        return payment_city;
    }

    public void setPayment_city (String payment_city)
    {
        this.payment_city = payment_city;
    }

    public String getShipping_postcode ()
    {
        return shipping_postcode;
    }

    public void setShipping_postcode (String shipping_postcode)
    {
        this.shipping_postcode = shipping_postcode;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getPayment_firstname ()
    {
        return payment_firstname;
    }

    public void setPayment_firstname (String payment_firstname)
    {
        this.payment_firstname = payment_firstname;
    }

    public String getPayment_company ()
    {
        return payment_company;
    }

    public void setPayment_company (String payment_company)
    {
        this.payment_company = payment_company;
    }

    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getStore_id ()
    {
        return store_id;
    }

    public void setStore_id (String store_id)
    {
        this.store_id = store_id;
    }

    public String getShipping_zone_code ()
    {
        return shipping_zone_code;
    }

    public void setShipping_zone_code (String shipping_zone_code)
    {
        this.shipping_zone_code = shipping_zone_code;
    }

    public String getInvoice_no ()
    {
        return invoice_no;
    }

    public void setInvoice_no (String invoice_no)
    {
        this.invoice_no = invoice_no;
    }

    public String getCurrency_value ()
    {
        return currency_value;
    }

    public void setCurrency_value (String currency_value)
    {
        this.currency_value = currency_value;
    }

    public ArrayList<HistoryModel> getHistories ()
    {
        return histories;
    }

    public void setHistories (ArrayList<HistoryModel> histories)
    {
        this.histories = histories;
    }

    public ArrayList<TotalsModel> getTotals ()
    {
        return totals;
    }

    public void setTotals (ArrayList<TotalsModel> totals)
    {
        this.totals = totals;
    }

    public String[] getVouchers ()
    {
        return vouchers;
    }

    public void setVouchers (String[] vouchers)
    {
        this.vouchers = vouchers;
    }

    public String getPayment_address_2 ()
    {
        return payment_address_2;
    }

    public void setPayment_address_2 (String payment_address_2)
    {
        this.payment_address_2 = payment_address_2;
    }

    public String getPayment_address_1 ()
    {
        return payment_address_1;
    }

    public void setPayment_address_1 (String payment_address_1)
    {
        this.payment_address_1 = payment_address_1;
    }

    public String getDate_modified ()
    {
        return date_modified;
    }

    public void setDate_modified (String date_modified)
    {
        this.date_modified = date_modified;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public String getShipping_zone ()
    {
        return shipping_zone;
    }

    public void setShipping_zone (String shipping_zone)
    {
        this.shipping_zone = shipping_zone;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getShipping_zone_id ()
    {
        return shipping_zone_id;
    }

    public void setShipping_zone_id (String shipping_zone_id)
    {
        this.shipping_zone_id = shipping_zone_id;
    }

    public String getCurrency_id ()
    {
        return currency_id;
    }

    public void setCurrency_id (String currency_id)
    {
        this.currency_id = currency_id;
    }

}
