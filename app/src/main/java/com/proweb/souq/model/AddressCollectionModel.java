package com.proweb.souq.model;

import java.util.ArrayList;

public class AddressCollectionModel {

    private ArrayList<AddressModel> addresses;

    private CustomFieldsModel custom_field;

    public ArrayList<AddressModel> getAddresses ()
    {
        return addresses;
    }

    public void setAddresses (ArrayList<AddressModel> addresses)
    {
        this.addresses = addresses;
    }

    public CustomFieldsModel getCustom_field() {
        return custom_field;
    }

    public void setCustom_field(CustomFieldsModel custom_field) {
        this.custom_field = custom_field;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addresses = "+addresses+"]";
    }
}
