package com.proweb.souq.model;


public class ProductModel
{
    private String seo_url;

    private String length_class_id;

    private String rating;

    private String points;

    private String[] discounts;

    private String shipping;

    private ReviewModel reviews;

    private String price;

    private String jan;

    private String product_id;

    private String[] original_images;

    private String original_image;

    private String model;

    private String stock_status_id;

    private String id;

    private String tag;

    private String sku;

    private String price_excluding_tax;

    private String height;

    private String image;

    private String[] images;

    private String manufacturer_id;

    private String stock_status;

    private String special_excluding_tax_formated;

    private String meta_title;

    private String subtract;

    private String upc;

    private String weight;

    private String mpn;

    private String special_end_date;

    private String meta_description;

    private String date_added;

    private String meta_keyword;

    private String[] recurrings;

    private String name;

    private String weight_class_id;

    private String status;

    private String price_excluding_tax_formated;

    private String price_formated;

    private String special_formated;

    private String isbn;

    private String description;

    private String length_class;

    private String manufacturer;

    private String special_start_date;

    private String ean;

    private String viewed;


    private String sort_order;

    private String reward;

    private int quantity;

    private String tax_class_id;

    private String length;

    private String special_excluding_tax;

    private String special;

    private String date_modified;

    private String weight_class;

    private String width;

    private String location;

    private String date_available;

    private CategoryModel[] category;

    private String minimum;

    private String thumb;

    private String key;
    private boolean stock;

    private String total;
    private String price_raw;
    private String total_raw;

    private boolean in_wishlist;

    private ProductOptionModel[] options;

    public boolean isIn_wishlist() {
        return in_wishlist;
    }

    public void setIn_wishlist(boolean in_wishlist) {
        this.in_wishlist = in_wishlist;
    }

    public ProductOptionModel[] getOptions() {
        return options;
    }

    public void setOptions(ProductOptionModel[] options) {
        this.options = options;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isStock() {
        return stock;
    }

    public void setStock(boolean stock) {
        this.stock = stock;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPrice_raw() {
        return price_raw;
    }

    public void setPrice_raw(String price_raw) {
        this.price_raw = price_raw;
    }

    public String getTotal_raw() {
        return total_raw;
    }

    public void setTotal_raw(String total_raw) {
        this.total_raw = total_raw;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getSeo_url ()
    {
        return seo_url;
    }

    public void setSeo_url (String seo_url)
    {
        this.seo_url = seo_url;
    }

    public String getLength_class_id ()
    {
        return length_class_id;
    }

    public void setLength_class_id (String length_class_id)
    {
        this.length_class_id = length_class_id;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getPoints ()
    {
        return points;
    }

    public void setPoints (String points)
    {
        this.points = points;
    }

    public String[] getDiscounts ()
    {
        return discounts;
    }

    public void setDiscounts (String[] discounts)
    {
        this.discounts = discounts;
    }

    public String getShipping ()
    {
        return shipping;
    }

    public void setShipping (String shipping)
    {
        this.shipping = shipping;
    }

    public ReviewModel getReviews ()
    {
        return reviews;
    }

    public void setReviews (ReviewModel reviews)
    {
        this.reviews = reviews;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getJan ()
    {
        return jan;
    }

    public void setJan (String jan)
    {
        this.jan = jan;
    }

    public String getProduct_id ()
    {
        return product_id;
    }

    public void setProduct_id (String product_id)
    {
        this.product_id = product_id;
    }

    public String[] getOriginal_images ()
    {
        return original_images;
    }

    public void setOriginal_images (String[] original_images)
    {
        this.original_images = original_images;
    }

    public String getOriginal_image ()
    {
        return original_image;
    }

    public void setOriginal_image (String original_image)
    {
        this.original_image = original_image;
    }


    public String getModel ()
    {
        return model;
    }

    public void setModel (String model)
    {
        this.model = model;
    }

    public String getStock_status_id ()
    {
        return stock_status_id;
    }

    public void setStock_status_id (String stock_status_id)
    {
        this.stock_status_id = stock_status_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTag ()
    {
        return tag;
    }

    public void setTag (String tag)
    {
        this.tag = tag;
    }

    public String getSku ()
    {
        return sku;
    }

    public void setSku (String sku)
    {
        this.sku = sku;
    }

    public String getPrice_excluding_tax ()
    {
        return price_excluding_tax;
    }

    public void setPrice_excluding_tax (String price_excluding_tax)
    {
        this.price_excluding_tax = price_excluding_tax;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String[] getImages ()
    {
        return images;
    }

    public void setImages (String[] images)
    {
        this.images = images;
    }

    public String getManufacturer_id ()
    {
        return manufacturer_id;
    }

    public void setManufacturer_id (String manufacturer_id)
    {
        this.manufacturer_id = manufacturer_id;
    }

    public String getStock_status ()
    {
        return stock_status;
    }

    public void setStock_status (String stock_status)
    {
        this.stock_status = stock_status;
    }

    public String getSpecial_excluding_tax_formated ()
    {
        return special_excluding_tax_formated;
    }

    public void setSpecial_excluding_tax_formated (String special_excluding_tax_formated)
    {
        this.special_excluding_tax_formated = special_excluding_tax_formated;
    }

    public String getMeta_title ()
    {
        return meta_title;
    }

    public void setMeta_title (String meta_title)
    {
        this.meta_title = meta_title;
    }

    public String getSubtract ()
    {
        return subtract;
    }

    public void setSubtract (String subtract)
    {
        this.subtract = subtract;
    }

    public String getUpc ()
    {
        return upc;
    }

    public void setUpc (String upc)
    {
        this.upc = upc;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getMpn ()
    {
        return mpn;
    }

    public void setMpn (String mpn)
    {
        this.mpn = mpn;
    }

    public String getSpecial_end_date ()
    {
        return special_end_date;
    }

    public void setSpecial_end_date (String special_end_date)
    {
        this.special_end_date = special_end_date;
    }

    public String getMeta_description ()
    {
        return meta_description;
    }

    public void setMeta_description (String meta_description)
    {
        this.meta_description = meta_description;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getMeta_keyword ()
    {
        return meta_keyword;
    }

    public void setMeta_keyword (String meta_keyword)
    {
        this.meta_keyword = meta_keyword;
    }

    public String[] getRecurrings ()
    {
        return recurrings;
    }

    public void setRecurrings (String[] recurrings)
    {
        this.recurrings = recurrings;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getWeight_class_id ()
    {
        return weight_class_id;
    }

    public void setWeight_class_id (String weight_class_id)
    {
        this.weight_class_id = weight_class_id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getPrice_excluding_tax_formated ()
    {
        return price_excluding_tax_formated;
    }

    public void setPrice_excluding_tax_formated (String price_excluding_tax_formated)
    {
        this.price_excluding_tax_formated = price_excluding_tax_formated;
    }

    public String getPrice_formated ()
    {
        return price_formated;
    }

    public void setPrice_formated (String price_formated)
    {
        this.price_formated = price_formated;
    }

    public String getSpecial_formated ()
    {
        return special_formated;
    }

    public void setSpecial_formated (String special_formated)
    {
        this.special_formated = special_formated;
    }

    public String getIsbn ()
    {
        return isbn;
    }

    public void setIsbn (String isbn)
    {
        this.isbn = isbn;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getLength_class ()
    {
        return length_class;
    }

    public void setLength_class (String length_class)
    {
        this.length_class = length_class;
    }

    public String getManufacturer ()
    {
        return manufacturer;
    }

    public void setManufacturer (String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getSpecial_start_date ()
    {
        return special_start_date;
    }

    public void setSpecial_start_date (String special_start_date)
    {
        this.special_start_date = special_start_date;
    }

    public String getEan ()
    {
        return ean;
    }

    public void setEan (String ean)
    {
        this.ean = ean;
    }

    public String getViewed ()
    {
        return viewed;
    }

    public void setViewed (String viewed)
    {
        this.viewed = viewed;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }

    public String getReward ()
    {
        return reward;
    }

    public void setReward (String reward)
    {
        this.reward = reward;
    }

    public int getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (int quantity)
    {
        this.quantity = quantity;
    }

    public String getTax_class_id ()
    {
        return tax_class_id;
    }

    public void setTax_class_id (String tax_class_id)
    {
        this.tax_class_id = tax_class_id;
    }

    public String getLength ()
    {
        return length;
    }

    public void setLength (String length)
    {
        this.length = length;
    }

    public String getSpecial_excluding_tax ()
    {
        return special_excluding_tax;
    }

    public void setSpecial_excluding_tax (String special_excluding_tax)
    {
        this.special_excluding_tax = special_excluding_tax;
    }

    public String getSpecial ()
    {
        return special;
    }

    public void setSpecial (String special)
    {
        this.special = special;
    }

    public String getDate_modified ()
    {
        return date_modified;
    }

    public void setDate_modified (String date_modified)
    {
        this.date_modified = date_modified;
    }

    public String getWeight_class ()
    {
        return weight_class;
    }

    public void setWeight_class (String weight_class)
    {
        this.weight_class = weight_class;
    }

    public String getWidth ()
    {
        return width;
    }

    public void setWidth (String width)
    {
        this.width = width;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getDate_available ()
    {
        return date_available;
    }

    public void setDate_available (String date_available)
    {
        this.date_available = date_available;
    }

    public CategoryModel[] getCategory ()
    {
        return category;
    }

    public void setCategory (CategoryModel[] category)
    {
        this.category = category;
    }

    public String getMinimum ()
    {
        return minimum;
    }

    public void setMinimum (String minimum)
    {
        this.minimum = minimum;
    }

}



