package com.proweb.souq.model;

import com.google.gson.annotations.SerializedName;

public class AccountCustomFieldModel {

    @SerializedName("1")
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
