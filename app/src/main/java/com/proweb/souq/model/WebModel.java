package com.proweb.souq.model;

public class WebModel {

    private String id;

    private String information_id;

    private String store_id;

    private String meta_description;

    private String meta_title;

    private String bottom;

    private String meta_keyword;

    private String description;

    private String language_id;

    private String title;

    private String sort_order;

    private String policy;

    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInformation_id ()
    {
        return information_id;
    }

    public void setInformation_id (String information_id)
    {
        this.information_id = information_id;
    }

    public String getStore_id ()
    {
        return store_id;
    }

    public void setStore_id (String store_id)
    {
        this.store_id = store_id;
    }

    public String getMeta_description ()
    {
        return meta_description;
    }

    public void setMeta_description (String meta_description)
    {
        this.meta_description = meta_description;
    }

    public String getMeta_title ()
    {
        return meta_title;
    }

    public void setMeta_title (String meta_title)
    {
        this.meta_title = meta_title;
    }

    public String getBottom ()
    {
        return bottom;
    }

    public void setBottom (String bottom)
    {
        this.bottom = bottom;
    }

    public String getMeta_keyword ()
    {
        return meta_keyword;
    }

    public void setMeta_keyword (String meta_keyword)
    {
        this.meta_keyword = meta_keyword;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getLanguage_id ()
    {
        return language_id;
    }

    public void setLanguage_id (String language_id)
    {
        this.language_id = language_id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }

    public String getPolicy ()
    {
        return policy;
    }

    public void setPolicy (String policy)
    {
        this.policy = policy;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [information_id = "+information_id+", store_id = "+store_id+", meta_description = "+meta_description+", meta_title = "+meta_title+", bottom = "+bottom+", meta_keyword = "+meta_keyword+", description = "+description+", language_id = "+language_id+", title = "+title+", sort_order = "+sort_order+", policy = "+policy+", status = "+status+"]";
    }
}
