package com.proweb.souq.model;

public class CurrencyModel {

    private String symbol_right;

        private String symbol_left;

        private String decimal_place;

        private String value;

        private String currency_id;

        public String getSymbol_right ()
        {
            return symbol_right;
        }

        public void setSymbol_right (String symbol_right)
        {
            this.symbol_right = symbol_right;
        }

        public String getSymbol_left ()
        {
            return symbol_left;
        }

        public void setSymbol_left (String symbol_left)
        {
            this.symbol_left = symbol_left;
        }

        public String getDecimal_place ()
        {
            return decimal_place;
        }

        public void setDecimal_place (String decimal_place)
        {
            this.decimal_place = decimal_place;
        }

        public String getValue ()
        {
            return value;
        }

        public void setValue (String value)
        {
            this.value = value;
        }

        public String getCurrency_id ()
        {
            return currency_id;
        }

        public void setCurrency_id (String currency_id)
        {
            this.currency_id = currency_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [symbol_right = "+symbol_right+", symbol_left = "+symbol_left+", decimal_place = "+decimal_place+", value = "+value+", currency_id = "+currency_id+"]";
        }


}
