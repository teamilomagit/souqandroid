package com.proweb.souq.model;

import java.io.Serializable;

public class ReviewModel implements Serializable {

    private ReviewDetailModel[] reviews;

    private String review_total;

    public ReviewDetailModel[] getReviews() {
        return reviews;
    }

    public void setReviews(ReviewDetailModel[] reviews) {
        this.reviews = reviews;
    }

    public String getReview_total ()
    {
        return review_total;
    }

    public void setReview_total (String review_total)
    {
        this.review_total = review_total;
    }


}
