package com.proweb.souq.model;

public class ShippingMethodsModel {

    private FreeModel free;

    private FlatModel flat;

    public FlatModel getFlat() {
        return flat;
    }

    public void setFlat(FlatModel flat) {
        this.flat = flat;
    }

    public FreeModel getFree ()
    {
        return free;
    }

    public void setFree (FreeModel free)
    {
        this.free = free;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [free = "+free+"]";
    }
}
