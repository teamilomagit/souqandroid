package com.proweb.souq.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterModel implements Parcelable{

    private String filter_id;

    private String name;

    private String sort_type;

    private String sort_order;

    private boolean isChecked;

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getSort_type() {
        return sort_type;
    }

    public void setSort_type(String sort_type) {
        this.sort_type = sort_type;
    }

    public String getFilter_id ()
    {
        return filter_id;
    }

    public void setFilter_id (String filter_id)
    {
        this.filter_id = filter_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filter_id = "+filter_id+", name = "+name+"]";
    }

    protected FilterModel(Parcel in) {
        filter_id = in.readString();
        name = in.readString();
        isChecked = in.readByte() != 0;
    }

    public FilterModel()
    {

    }

    public static final Creator<FilterModel> CREATOR = new Creator<FilterModel>() {
        @Override
        public FilterModel createFromParcel(Parcel in) {
            return new FilterModel(in);
        }

        @Override
        public FilterModel[] newArray(int size) {
            return new FilterModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(filter_id);
        dest.writeString(name);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }
}
