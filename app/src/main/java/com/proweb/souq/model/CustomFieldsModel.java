package com.proweb.souq.model;

import com.google.gson.annotations.SerializedName;

public class CustomFieldsModel {
    private String custom_field_id;

    private String name;

    private String location;

    private CustomFieldValueModel[] custom_field_value;

    private String type;

    private String value;

    private String sort_order;

    private String validation;

    private String required;

    //Address detail
    @SerializedName("2")
    private String district;

    @SerializedName("3")
    private String zone;

    @SerializedName("4")
    private String street;

    @SerializedName("5")
    private String house;

    @SerializedName("6")
    private String landmark;

    public String getCustom_field_id ()
    {
        return custom_field_id;
    }

    public void setCustom_field_id (String custom_field_id)
    {
        this.custom_field_id = custom_field_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public CustomFieldValueModel[] getCustom_field_value ()
    {
        return custom_field_value;
    }

    public void setCustom_field_value (CustomFieldValueModel[] custom_field_value)
    {
        this.custom_field_value = custom_field_value;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }

    public String getValidation ()
    {
        return validation;
    }

    public void setValidation (String validation)
    {
        this.validation = validation;
    }

    public String getRequired ()
    {
        return required;
    }

    public void setRequired (String required)
    {
        this.required = required;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [custom_field_id = "+custom_field_id+", name = "+name+", location = "+location+", custom_field_value = "+custom_field_value+", type = "+type+", value = "+value+", sort_order = "+sort_order+", validation = "+validation+", required = "+required+"]";
    }
}
