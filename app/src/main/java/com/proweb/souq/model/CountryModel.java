package com.proweb.souq.model;

import java.util.ArrayList;

public class CountryModel {

    private String iso_code_2;

    private String address_format;

    private String iso_code_3;

    private String name;

    private String postcode_required;

    private String country_id;

    private String status;

    private ArrayList<ZoneModel> zone;

    public String getIso_code_2 ()
    {
        return iso_code_2;
    }

    public void setIso_code_2 (String iso_code_2)
    {
        this.iso_code_2 = iso_code_2;
    }

    public String getAddress_format ()
    {
        return address_format;
    }

    public void setAddress_format (String address_format)
    {
        this.address_format = address_format;
    }

    public String getIso_code_3 ()
    {
        return iso_code_3;
    }

    public void setIso_code_3 (String iso_code_3)
    {
        this.iso_code_3 = iso_code_3;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPostcode_required ()
    {
        return postcode_required;
    }

    public void setPostcode_required (String postcode_required)
    {
        this.postcode_required = postcode_required;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<ZoneModel> getZone ()
    {
        return zone;
    }

    public void setZone (ArrayList<ZoneModel> zone)
    {
        this.zone = zone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [iso_code_2 = "+iso_code_2+", address_format = "+address_format+", iso_code_3 = "+iso_code_3+", name = "+name+", postcode_required = "+postcode_required+", country_id = "+country_id+", status = "+status+"]";
    }

}
