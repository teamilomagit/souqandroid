package com.proweb.souq.model;

public class MethodModel {
    private String code;

    private String terms;

    private String title;

    private String sort_order;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getTerms ()
    {
        return terms;
    }

    public void setTerms (String terms)
    {
        this.terms = terms;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", terms = "+terms+", title = "+title+", sort_order = "+sort_order+"]";
    }
}
