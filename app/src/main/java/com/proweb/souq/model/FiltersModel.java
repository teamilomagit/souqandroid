package com.proweb.souq.model;

public class FiltersModel {
    private FilterGroupsModel[] filter_groups;

    public FilterGroupsModel[] getFilter_groups ()
    {
        return filter_groups;
    }

    public void setFilter_groups (FilterGroupsModel[] filter_groups)
    {
        this.filter_groups = filter_groups;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filter_groups = "+filter_groups+"]";
    }
}
