package com.proweb.souq.model;

public class PaymentMethodsModel {
    private PaymentMethodPostModel payment_methods;

    public PaymentMethodPostModel getPayment_methods ()
    {
        return payment_methods;
    }

    public void setPayment_methods (PaymentMethodPostModel payment_methods)
    {
        this.payment_methods = payment_methods;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [payment_methods = "+payment_methods+"]";
    }
}
