package com.proweb.souq.model;

public class ShippingDeliveryModel {
    private String zone_id;

    private String name;

    private String shipping_flat_cost;

    private String country_id;

    public String getZone_id ()
    {
        return zone_id;
    }

    public void setZone_id (String zone_id)
    {
        this.zone_id = zone_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getShipping_flat_cost ()
    {
        return shipping_flat_cost;
    }

    public void setShipping_flat_cost (String shipping_flat_cost)
    {
        this.shipping_flat_cost = shipping_flat_cost;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [zone_id = "+zone_id+", name = "+name+", shipping_flat_cost = "+shipping_flat_cost+", country_id = "+country_id+"]";
    }
}
