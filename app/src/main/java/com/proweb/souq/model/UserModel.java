package com.proweb.souq.model;

public class UserModel {

    private String session;

    private String seller;

    private String newsletter;

    private String firstname;

    private String code;

    //private String wishlist;

    private String language_id;

    private String cart;

    private String user_balance;

    private AccountCustomFieldModel account_custom_field;

    private String company;

    private String fax;

    private String email;

    private String store_id;

    private String reward_total;

    //private CustomFieldsModel[] custom_fields;

    private String ip;

    private String address_id;

    private String telephone;

    private String lastname;

    private String token;

    private String date_added;

    private String safe;

    private String customer_id;

    private String customer_group_id;

    private String status;

    public String getSeller ()
    {
        return seller;
    }

    public void setSeller (String seller)
    {
        this.seller = seller;
    }

    public String getNewsletter ()
    {
        return newsletter;
    }

    public void setNewsletter (String newsletter)
    {
        this.newsletter = newsletter;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    /*public String getWishlist ()
    {
        return wishlist;
    }

    public void setWishlist (String wishlist)
    {
        this.wishlist = wishlist;
    }*/

    public String getLanguage_id ()
    {
        return language_id;
    }

    public void setLanguage_id (String language_id)
    {
        this.language_id = language_id;
    }

    public String getCart ()
    {
        return cart;
    }

    public void setCart (String cart)
    {
        this.cart = cart;
    }

    public String getUser_balance ()
    {
        return user_balance;
    }

    public void setUser_balance (String user_balance)
    {
        this.user_balance = user_balance;
    }

    public AccountCustomFieldModel getAccount_custom_field ()
    {
        return account_custom_field;
    }

    public void setAccount_custom_field (AccountCustomFieldModel account_custom_field)
    {
        this.account_custom_field = account_custom_field;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getFax ()
    {
        return fax;
    }

    public void setFax (String fax)
    {
        this.fax = fax;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStore_id ()
    {
        return store_id;
    }

    public void setStore_id (String store_id)
    {
        this.store_id = store_id;
    }

    public String getReward_total ()
    {
        return reward_total;
    }

    public void setReward_total (String reward_total)
    {
        this.reward_total = reward_total;
    }

    /*public CustomFieldsModel[] getCustom_fields ()
    {
        return custom_fields;
    }

    public void setCustom_fields (CustomFieldsModel[] custom_fields)
    {
        this.custom_fields = custom_fields;
    }*/

    public String getIp ()
    {
        return ip;
    }

    public void setIp (String ip)
    {
        this.ip = ip;
    }

    public String getAddress_id ()
    {
        return address_id;
    }

    public void setAddress_id (String address_id)
    {
        this.address_id = address_id;
    }

    public String getTelephone ()
    {
        return telephone;
    }

    public void setTelephone (String telephone)
    {
        this.telephone = telephone;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getToken ()
    {
        return token;
    }

    public void setToken (String token)
    {
        this.token = token;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getSafe ()
    {
        return safe;
    }

    public void setSafe (String safe)
    {
        this.safe = safe;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getCustomer_group_id ()
    {
        return customer_group_id;
    }

    public void setCustomer_group_id (String customer_group_id)
    {
        this.customer_group_id = customer_group_id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [seller = "+seller+", newsletter = "+newsletter+", firstname = "+firstname+", code = "+code+", language_id = "+language_id+", cart = "+cart+", user_balance = "+user_balance+", company = "+company+", fax = "+fax+", email = "+email+", store_id = "+store_id+", reward_total = "+reward_total+", ip = "+ip+", address_id = "+address_id+", telephone = "+telephone+", lastname = "+lastname+", token = "+token+", date_added = "+date_added+", safe = "+safe+", customer_id = "+customer_id+", customer_group_id = "+customer_group_id+", status = "+status+"]";
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
