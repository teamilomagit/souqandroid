package com.proweb.souq.model;

public class CustomFieldValueModel {
    private String name;

    private String custom_field_value_id;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCustom_field_value_id ()
    {
        return custom_field_value_id;
    }

    public void setCustom_field_value_id (String custom_field_value_id)
    {
        this.custom_field_value_id = custom_field_value_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", custom_field_value_id = "+custom_field_value_id+"]";
    }
}
