package com.proweb.souq.model;

import com.google.gson.annotations.SerializedName;

public class AddressModel {

    private String address_id;

    private String zone_id;

    private CustomFieldsModel custom_field;

    private String firstname;

    @SerializedName("default")
    private boolean primary;
    //private boolean defaulti;

    private String city;

    private String address_1;

    private String address_2;

    private String postcode;

    private String company;

    private String country_id;

    private String lastname;

    private String address_format;

    private String country;

    private String zone_code;

    private String iso_code_2;

    private String iso_code_3;

    private String zone;

    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getZone_id ()
    {
        return zone_id;
    }

    public void setZone_id (String zone_id)
    {
        this.zone_id = zone_id;
    }

    public CustomFieldsModel getCustom_field() {
        return custom_field;
    }

    public void setCustom_field(CustomFieldsModel custom_field) {
        this.custom_field = custom_field;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getAddress_1 ()
    {
        return address_1;
    }

    public void setAddress_1 (String address_1)
    {
        this.address_1 = address_1;
    }

    public String getAddress_2 ()
    {
        return address_2;
    }

    public void setAddress_2 (String address_2)
    {
        this.address_2 = address_2;
    }

    public String getPostcode ()
    {
        return postcode;
    }

    public void setPostcode (String postcode)
    {
        this.postcode = postcode;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getAddress_format() {
        return address_format;
    }

    public void setAddress_format(String address_format) {
        this.address_format = address_format;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZone_code() {
        return zone_code;
    }

    public void setZone_code(String zone_code) {
        this.zone_code = zone_code;
    }

    public String getIso_code_2() {
        return iso_code_2;
    }

    public void setIso_code_2(String iso_code_2) {
        this.iso_code_2 = iso_code_2;
    }

    public String getIso_code_3() {
        return iso_code_3;
    }

    public void setIso_code_3(String iso_code_3) {
        this.iso_code_3 = iso_code_3;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [zone_id = "+zone_id+", firstname = "+firstname+", city = "+city+", address_1 = "+address_1+", address_2 = "+address_2+", postcode = "+postcode+", company = "+company+", country_id = "+country_id+", lastname = "+lastname+"]";
    }
}
