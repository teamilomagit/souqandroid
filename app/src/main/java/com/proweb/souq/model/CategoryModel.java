package com.proweb.souq.model;

import java.util.ArrayList;

public class CategoryModel
{
    private String image;

    private String seo_url;

    private String category_id;

    private String parent_id;

    private String name;

    private String original_image;

    private FiltersModel filters;

    private String[] categories;

    private ArrayList<ProductModel> productList;

    public ArrayList<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductModel> productList) {
        this.productList = productList;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getSeo_url ()
    {
        return seo_url;
    }

    public void setSeo_url (String seo_url)
    {
        this.seo_url = seo_url;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public String getParent_id ()
    {
        return parent_id;
    }

    public void setParent_id (String parent_id)
    {
        this.parent_id = parent_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getOriginal_image ()
    {
        return original_image;
    }

    public void setOriginal_image (String original_image)
    {
        this.original_image = original_image;
    }

    public FiltersModel getFilters ()
    {
        return filters;
    }

    public void setFilters (FiltersModel filters)
    {
        this.filters = filters;
    }

    public String[] getCategories ()
    {
        return categories;
    }

    public void setCategories (String[] categories)
    {
        this.categories = categories;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image = "+image+", seo_url = "+seo_url+", category_id = "+category_id+", parent_id = "+parent_id+", name = "+name+", original_image = "+original_image+", filters = "+filters+", categories = "+categories+"]";
    }
}

