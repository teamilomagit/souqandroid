package com.proweb.souq.model;

public class ProductOptionModel {

    private ProductOptionValueModel[] option_value;

    private String product_option_id;

    private String name;

    private String option_id;

    private String type;

    private String value;

    private String required;

    private ProductOptionValueModel selectedOptionVal;

    public ProductOptionValueModel getSelectedOptionVal() {
        return selectedOptionVal;
    }

    public void setSelectedOptionVal(ProductOptionValueModel selectedOptionVal) {
        this.selectedOptionVal = selectedOptionVal;
    }

    public ProductOptionValueModel[] getOption_value() {
        return option_value;
    }

    public void setOption_value(ProductOptionValueModel[] option_value) {
        this.option_value = option_value;
    }

    public String getProduct_option_id() {
        return product_option_id;
    }

    public void setProduct_option_id(String product_option_id) {
        this.product_option_id = product_option_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }
}
