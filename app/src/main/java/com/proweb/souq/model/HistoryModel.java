package com.proweb.souq.model;

public class HistoryModel {
    private String date_added;

    private String comment;

    private String status;

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date_added = "+date_added+", comment = "+comment+", status = "+status+"]";
    }
}
