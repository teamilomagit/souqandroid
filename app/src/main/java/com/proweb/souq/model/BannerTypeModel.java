package com.proweb.souq.model;

public class BannerTypeModel
{
    private String banner_id;

    private String name;

    private String checksum;

    private String status;

    public String getBanner_id ()
    {
        return banner_id;
    }

    public void setBanner_id (String banner_id)
    {
        this.banner_id = banner_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getChecksum ()
    {
        return checksum;
    }

    public void setChecksum (String checksum)
    {
        this.checksum = checksum;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [banner_id = "+banner_id+", name = "+name+", checksum = "+checksum+", status = "+status+"]";
    }
}
