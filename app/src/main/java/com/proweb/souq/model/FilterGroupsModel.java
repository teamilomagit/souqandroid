package com.proweb.souq.model;

public class FilterGroupsModel {
    private FilterModel[] filter;

    private String name;

    private String filter_group_id;

    private String selectedValue;

    private String selectedOptions;


    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public String getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(String selectedOptions) {
        this.selectedOptions = selectedOptions;
    }




    public FilterModel[] getFilter ()
    {
        return filter;
    }

    public void setFilter (FilterModel[] filter)
    {
        this.filter = filter;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getFilter_group_id ()
    {
        return filter_group_id;
    }

    public void setFilter_group_id (String filter_group_id)
    {
        this.filter_group_id = filter_group_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filter = "+filter+", name = "+name+", filter_group_id = "+filter_group_id+"]";
    }
}
