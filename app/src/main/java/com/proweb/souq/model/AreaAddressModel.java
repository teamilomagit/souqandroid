package com.proweb.souq.model;

import java.util.ArrayList;

public class AreaAddressModel {

    private ArrayList<AddressModel> addresses;

    private String address_id;

    private boolean isChecked;

    public ArrayList<AddressModel> getAddresses ()
    {
        return addresses;
    }

    public void setAddresses (ArrayList<AddressModel> addresses)
    {
        this.addresses = addresses;
    }

    public String getAddress_id ()
    {
        return address_id;
    }

    public void setAddress_id (String address_id)
    {
        this.address_id = address_id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addresses = "+addresses+", address_id = "+address_id+"]";
    }

}
