package com.proweb.souq.model;

public class PaymentMethodPostModel {
    private MethodModel cod;

    public MethodModel getCod ()
    {
        return cod;
    }

    public void setCod (MethodModel cod)
    {
        this.cod = cod;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cod = "+cod+"]";
    }
}
