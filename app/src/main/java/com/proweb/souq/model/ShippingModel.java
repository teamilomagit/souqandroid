package com.proweb.souq.model;

public class ShippingModel {
    private String code;

    private ShippingMethodsModel shipping_methods;

    private String comment;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public ShippingMethodsModel getShipping_methods ()
    {
        return shipping_methods;
    }

    public void setShipping_methods (ShippingMethodsModel shipping_methods)
    {
        this.shipping_methods = shipping_methods;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", shipping_methods = "+shipping_methods+", comment = "+comment+"]";
    }
}
