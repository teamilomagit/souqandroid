package com.proweb.souq.model;

public class CartModel
{
    private String reward;

    private String has_shipping;

    private String coupon;

    private String voucher_status;

    private String voucher;

    private String weight;

    private TotalsModel[] totals;

    private String[] vouchers;

    private ProductModel[] products;

    private String coupon_status;

    private int total_product_count;

    private String total_raw;

    private String total;

    private String has_recurring_products;

    private CurrencyModel currency;

    private String reward_status;

    private String has_download;

    public String getReward ()
    {
        return reward;
    }

    public void setReward (String reward)
    {
        this.reward = reward;
    }

    public String getHas_shipping ()
    {
        return has_shipping;
    }

    public void setHas_shipping (String has_shipping)
    {
        this.has_shipping = has_shipping;
    }

    public String getCoupon ()
    {
        return coupon;
    }

    public void setCoupon (String coupon)
    {
        this.coupon = coupon;
    }

    public String getVoucher_status ()
    {
        return voucher_status;
    }

    public void setVoucher_status (String voucher_status)
    {
        this.voucher_status = voucher_status;
    }

    public String getVoucher ()
    {
        return voucher;
    }

    public void setVoucher (String voucher)
    {
        this.voucher = voucher;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public TotalsModel[] getTotals ()
    {
        return totals;
    }

    public void setTotals (TotalsModel[] totals)
    {
        this.totals = totals;
    }

    public String[] getVouchers ()
    {
        return vouchers;
    }

    public void setVouchers (String[] vouchers)
    {
        this.vouchers = vouchers;
    }

    public ProductModel[] getProducts ()
    {
        return products;
    }

    public void setProducts (ProductModel[] products)
    {
        this.products = products;
    }

    public String getCoupon_status ()
    {
        return coupon_status;
    }

    public void setCoupon_status (String coupon_status)
    {
        this.coupon_status = coupon_status;
    }

    public int getTotal_product_count ()
    {
        return total_product_count;
    }

    public void setTotal_product_count (int total_product_count)
    {
        this.total_product_count = total_product_count;
    }

    public String getTotal_raw ()
    {
        return total_raw;
    }

    public void setTotal_raw (String total_raw)
    {
        this.total_raw = total_raw;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getHas_recurring_products ()
    {
        return has_recurring_products;
    }

    public void setHas_recurring_products (String has_recurring_products)
    {
        this.has_recurring_products = has_recurring_products;
    }

    public CurrencyModel getCurrency ()
    {
        return currency;
    }

    public void setCurrency (CurrencyModel currency)
    {
        this.currency = currency;
    }

    public String getReward_status ()
    {
        return reward_status;
    }

    public void setReward_status (String reward_status)
    {
        this.reward_status = reward_status;
    }

    public String getHas_download ()
    {
        return has_download;
    }

    public void setHas_download (String has_download)
    {
        this.has_download = has_download;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [reward = "+reward+", has_shipping = "+has_shipping+", coupon = "+coupon+", voucher_status = "+voucher_status+", voucher = "+voucher+", weight = "+weight+", totals = "+totals+", vouchers = "+vouchers+", products = "+products+", coupon_status = "+coupon_status+", total_product_count = "+total_product_count+", total_raw = "+total_raw+", total = "+total+", has_recurring_products = "+has_recurring_products+", currency = "+currency+", reward_status = "+reward_status+", has_download = "+has_download+"]";
    }
}
