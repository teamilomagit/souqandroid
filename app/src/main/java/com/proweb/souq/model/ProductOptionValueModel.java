package com.proweb.souq.model;

public class ProductOptionValueModel {

    private String price_excluding_tax_formated;

    private String image;

    private String price_formated;

    private String quantity;

    private String option_value_id;

    private String price;

    private String product_option_value_id;

    private String name;

    private String price_prefix;

    private String price_excluding_tax;

    private String selected_parent_id;


    public String getSelected_parent_id() {
        return selected_parent_id;
    }

    public void setSelected_parent_id(String selected_parent_id) {
        this.selected_parent_id = selected_parent_id;
    }

    public String getPrice_excluding_tax_formated() {
        return price_excluding_tax_formated;
    }

    public void setPrice_excluding_tax_formated(String price_excluding_tax_formated) {
        this.price_excluding_tax_formated = price_excluding_tax_formated;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice_formated() {
        return price_formated;
    }

    public void setPrice_formated(String price_formated) {
        this.price_formated = price_formated;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(String option_value_id) {
        this.option_value_id = option_value_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_option_value_id() {
        return product_option_value_id;
    }

    public void setProduct_option_value_id(String product_option_value_id) {
        this.product_option_value_id = product_option_value_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice_prefix() {
        return price_prefix;
    }

    public void setPrice_prefix(String price_prefix) {
        this.price_prefix = price_prefix;
    }

    public String getPrice_excluding_tax() {
        return price_excluding_tax;
    }

    public void setPrice_excluding_tax(String price_excluding_tax) {
        this.price_excluding_tax = price_excluding_tax;
    }
}
