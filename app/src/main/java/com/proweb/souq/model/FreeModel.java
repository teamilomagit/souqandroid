package com.proweb.souq.model;

public class FreeModel {
    private String code;

    private String cost;

    private String tax_class_id;

    private String text;

    private String title;


    private QuoteModel quote;

    //private String title;

    private String error;

    private String sort_order;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getCost ()
    {
        return cost;
    }

    public void setCost (String cost)
    {
        this.cost = cost;
    }

    public String getTax_class_id ()
    {
        return tax_class_id;
    }

    public void setTax_class_id (String tax_class_id)
    {
        this.tax_class_id = tax_class_id;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }


    //New
    public QuoteModel getQuote ()
    {
        return quote;
    }

    public void setQuote (QuoteModel quote)
    {
        this.quote = quote;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }


    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", cost = "+cost+", tax_class_id = "+tax_class_id+", text = "+text+", title = "+title+"]";
    }
}
