package com.proweb.souq.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by You on 28/02/18.
 */

public class ConnectionManager {
    public static boolean Connection(Context context) {
        if (context != null) {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            return !(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable());
        }

        return false;
    }
}
