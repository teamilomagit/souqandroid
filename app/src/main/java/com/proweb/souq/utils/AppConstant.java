package com.proweb.souq.utils;

import com.proweb.souq.model.FilterGroupsModel;
import com.proweb.souq.model.FilterModel;

import java.util.ArrayList;

public class AppConstant {


    //public static final String BASE_URL = "https://souq366.com/api/rest/";
    public static final String BASE_URL = "http://dev.ravikatre.in/api/rest/";
    public static final String USER_BASE_URL = "https://souq366.com/index.php?route=rest/";


    public static final String GET_SESSION = BASE_URL + "session";
    public static final String LOGIN_API = BASE_URL + "login";
    public static final String SOCIAL_LOGIN_API = BASE_URL + "sociallogin";
    public static final String ADD_TO_CART_API = BASE_URL + "cart";
    public static final String LOGOUT_API = BASE_URL + "logout";
    public static final String REGISTRATION_API = BASE_URL + "register2";
    public static final String GET_BANNERS_API = BASE_URL + "banners";
    public static final String FORGOT_PASS_API = BASE_URL + "forgotten";
    public static final String GET_CATEGORIES_API = BASE_URL + "categories";
    public static final String GET_WISHLIST_API = BASE_URL + "wishlist";
    //public static final String REMOVE_FROM_WISHLIST_API = BASE_URL + "wishlist";
    public static final String GET_CART_API = BASE_URL + "cart";
    public static final String GET_ADDRESSES_API = BASE_URL + "account/address";
    public static final String DELETE_ADDRESSES_API = BASE_URL + "account/address";
    public static final String GUEST_SHIPPING_API = BASE_URL + "guestshipping";
    public static final String CREATE_GUEST_API = BASE_URL + "guest";


    //PRODUCTS
    public static final String GET_PRODUCT_DETAIL_API = BASE_URL + "products";
    public static final String GET_PRODUCTS_BY_CATEGORY_API = BASE_URL + "products/category/";
    public static final String GET_PRODUCTS_RELATED = BASE_URL + "products/category/";
    public static final String GET_PRODUCTS_BY_CATEGORY_API_NEW = "http://dev.ravikatre.in/index.php?route=feed/rest_api/products&category";

    public static final String GET_DEAL_OF_DAY = BASE_URL + "latest";
    public static final String GET_ACCOUNT_DETAIL_API = BASE_URL + "account/account";
    public static final String UPDATE_ACCOUNT_DETAIL_API = BASE_URL + "account/account";
    public static final String CHANGE_PASSWORD_API = BASE_URL + "account/password";
    public static final String ADD_ADDRESS_API = BASE_URL + "account/address";
    public static final String GET_SEARCHED_PRODUCTS = BASE_URL + "products/search/";
    public static final String GET_ZONE_SHIPPING = BASE_URL + "zone_shipping";

    //COUNTRIES
    public static final String GET_COUNTRIES = BASE_URL + "countries";
    public static final String GET_ZONES = BASE_URL + "countries";

    //ORDERS
    public static final String GET_ORDERS_LIST_API = BASE_URL + "customerorders/";
    //public static final String GET_ORDERS_LIST_API = BASE_URL + "customerorders/";
    public static final String REVIEW_ORDER_API = BASE_URL + "confirm";
    public static final String CANCEL_ORDER_API = BASE_URL + "orderhistory";


    //PAYMENT ADDRESSES
    public static final String GET_PAYMENT_ADDRESSES = BASE_URL + "paymentaddress";
    public static final String GET_PAYMENT_EXISTING_ADDRESS = BASE_URL + "paymentaddress/existing";
    public static final String GET_SHIPPING_ADDRESS = BASE_URL + "shippingaddress/existing";
    public static final String GET_SHIPPING_METHODS = BASE_URL + "shippingmethods";
    public static final String GET_SHIPPING_METHODS_POST = BASE_URL + "shippingmethods";
    public static final String GET_PAYMENT_METHODS = BASE_URL + "paymentmethods";
    public static final String CONFIRM_ORDER = BASE_URL + "confirm";

    public static final String GET_WEB_INFO = BASE_URL + "information";
    public static final String POST_QUESTION_API = BASE_URL + "contact2";
    public static final String APPLY_CODE_API = BASE_URL + "coupon";


    //CONSTANTS
    public static final int PAGINATION_LIMIT = 10;
    public static boolean SHOULD_RELOAD = false;
    public static String LANGUAGE = "English";
    public static boolean isAppRTL = false;
    public static String REVERSE_INPUT_STRING = null;
    public static int CART_COUNTER = 0;
    public static ArrayList<FilterGroupsModel> tempSelected = new ArrayList<>();
    public static FilterModel selectedSort = null;
}
