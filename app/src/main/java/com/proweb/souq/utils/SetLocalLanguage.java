package com.proweb.souq.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

/**
 * Created by You on 12/06/18.
 */

public class SetLocalLanguage {

    synchronized static public void setLocaleLanguage(Context context, String strLanguage) {
        Locale locale = new Locale(strLanguage.toLowerCase());
        Configuration conf = context.getResources().getConfiguration();
        conf.locale = locale;
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLayoutDirection(conf.locale);
        }
        context.getResources().updateConfiguration(conf, context.getResources().getDisplayMetrics());
    }
}




/*Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(strLanguage.toLowerCase());
        res.updateConfiguration(conf, dm);
        Preferences.setAppLanguage(strLanguage);*/




