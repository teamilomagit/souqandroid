package com.proweb.souq.utils;

import android.content.Context;
import android.text.TextUtils;

import com.proweb.souq.model.BannerModel;
import com.proweb.souq.model.BannerTypeModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AppUtils {

    public String getBannerTypeId(ArrayList<BannerTypeModel> list, String type) {
        String bannerId = null;
        for (BannerTypeModel model : list
        ) {

            if (model.getName().equalsIgnoreCase(type)) {
                bannerId = model.getBanner_id();
                break;
            }
        }
        return bannerId;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String getFormattedDate(String rawDate) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dt = "";

        try {
            date = sdf.parse(rawDate);
            dt = new SimpleDateFormat("MMM dd, yyyy").format(date) + "";
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dt;
    }
}
