package com.proweb.souq.utils;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.proweb.souq.model.UserModel;

/**
 * Created by Kalpana on 1/24/2019.
 */

public class SessionManager {

    private static SessionManager sessionObj;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USER = "user";
    Context context;

    public static SessionManager getInstance(){
        if (sessionObj == null){
            sessionObj = new SessionManager();
        }
        return sessionObj;
    }

    private void init(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(UserModel userModel, Context context) {
        init(context);
        Gson gson = new Gson();
        String jsonString = gson.toJson(userModel);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER, jsonString);
        editor.commit();
    }

    public UserModel getUserModel(Context context) {
        init(context);
        UserModel userModel = null;
        Gson gson = new Gson();
        String jsonString = pref.getString(KEY_USER, "");
        userModel = gson.fromJson(jsonString, UserModel.class);
        return userModel;
    }


    public boolean isLoggedIn(Context context){
        init(context);
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void logout(){
        sessionObj = null;
        editor.putBoolean(IS_LOGIN, false);
        editor.putString(KEY_USER, "");
        editor.commit();
    }
}
