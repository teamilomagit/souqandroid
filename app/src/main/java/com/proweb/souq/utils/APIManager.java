package com.proweb.souq.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by You on 13/08/18.
 */

public class APIManager {

    APIManagerInterface listener;
    private RequestQueue requestQueue;
    String jsonString = null;

    public interface APIManagerInterface {
        public void onSuccess(Object resultObj, String msg);

        public void onError(String error);
    }


    public void postAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "ON RESPONSE " + response);
                            try {
                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    if (jsonString.equalsIgnoreCase("null")) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }
                                    if (classType == null) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }

                                    JSONObject object = new JSONObject(jsonString);
                                    Object model = gson.fromJson(object.toString(), classType);

                                    if (listener != null) {
                                        listener.onSuccess(model, successMsg);
                                    }
                                } else {
                                    JSONArray errArr = response.getJSONArray("error");

                                    if (listener != null) {
                                        listener.onError(errArr.getString(0));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        error.printStackTrace();
                        String jsonError = new String(error.networkResponse.data);
                        Log.e("Json Error", "Error: " + error
                                + "\nStatus Code " + error.networkResponse.statusCode
                                + "\nCause " + error.getCause()
                                + "\nnetworkResponse " + jsonError
                                + "\nmessage" + error.getMessage());

                        if (jsonError != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonError);
                                if (jsonObject.getString("success").equalsIgnoreCase("0")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("error");
                                    listener.onError(jsonArray.getString(0));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.onError(error.toString());
                        }

                        /*if(error != null) {
                            Log.e("Json Error", "Error: " + error
                                    + "\nStatus Code " + error.networkResponse.statusCode
                                    + "\nCause " + error.getCause()
                                    + "\nnetworkResponse " + error.networkResponse.data.toString()
                                    + "\nmessage" + error.getMessage());
                            listener.onError(error.toString());
                        }*/
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");
                    params.put("Content-Type", "application/json");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has session value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            //Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            if (listener != null) {
            }
        }
    }


    public void postJSONArrayAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "RESPONSE  = " + response);
                            try {

                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    JSONArray jsonArray = new JSONArray(jsonString);
                                    ArrayList arrList = new ArrayList();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
                                        arrList.add(model);
                                    }

                                    if (listener != null) {
                                        listener.onSuccess(arrList, successMsg);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        error.printStackTrace();

                        String jsonError = new String(error.networkResponse.data);
                        Log.e("Json Error", "Error: " + error
                                + "\nStatus Code " + error.networkResponse.statusCode
                                + "\nCause " + error.getCause()
                                + "\nnetworkResponse " + jsonError
                                + "\nmessage" + error.getMessage());

                        if (jsonError != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonError);
                                if (jsonObject.getString("success").equalsIgnoreCase("0")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("error");
                                    listener.onError(jsonArray.getString(0));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.onError(error.toString());
                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");
                    params.put("Content-Type", "application/json");
                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }

                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {

            if (listener != null && context != null) {
                //Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }

    public void getAPI(String url, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "ON RESPONSE " + response);
                            try {
                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    if (jsonString.equalsIgnoreCase("null")) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }
                                    if (classType == null) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }

                                    JSONObject object = new JSONObject(jsonString);
                                    Object model = gson.fromJson(object.toString(), classType);

                                    if (listener != null) {
                                        listener.onSuccess(model, successMsg);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        error.printStackTrace();
                        if (error.networkResponse != null) {
                            String jsonError = new String(error.networkResponse.data);
                            Log.e("Json Error", "Error: " + error
                                    + "\nStatus Code " + error.networkResponse.statusCode
                                    + "\nCause " + error.getCause()
                                    + "\nnetworkResponse " + jsonError
                                    + "\nmessage" + error.getMessage());

                            if (jsonError != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                    if (jsonObject.getString("success").equalsIgnoreCase("0")) {
                                        JSONArray jsonArray = jsonObject.getJSONArray("error");
                                        listener.onError(jsonArray.getString(0));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                listener.onError(error.toString());
                            }
                        }
                    }
                }


            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            if (listener != null && context != null) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }

    public void getJSONArrayAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "RESPONSE  = " + response);
                            try {

                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    JSONArray jsonArray = new JSONArray(jsonString);
                                    ArrayList arrList = new ArrayList();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
                                        arrList.add(model);
                                    }

                                    if (listener != null) {
                                        listener.onSuccess(arrList, successMsg);
                                    }

                                    //  JSONObject object = new JSONObject(jsonString);
                                    //Object model = gson.fromJson(object.toString(), classType);

                                    //if (listener != null) {
                                    //  listener.onSuccess(model);
                                    //}
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        if (error.networkResponse != null) {
                            String jsonError = new String(error.networkResponse.data);
                            Log.e("Json Error", "Error: " + error
                                    + "\nStatus Code " + error.networkResponse.statusCode
                                    + "\nCause " + error.getCause()
                                    + "\nnetworkResponse " + jsonError
                                    + "\nmessage" + error.getMessage());

                            if (jsonError != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                    if (jsonObject.getString("success").equalsIgnoreCase("0")) {
                                        JSONArray jsonArray = jsonObject.getJSONArray("error");
                                        listener.onError(jsonArray.getString(0));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                listener.onError(error.toString());
                            }
                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {

            if (listener != null && context != null) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }

    public void deleteAPI(String url, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "ON RESPONSE " + response);
                            try {
                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    if (jsonString.equalsIgnoreCase("null")) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }
                                    if (classType == null) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }

                                    JSONObject object = new JSONObject(jsonString);
                                    Object model = gson.fromJson(object.toString(), classType);

                                    if (listener != null) {
                                        listener.onSuccess(model, successMsg);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            // Print Error!
                            Toast.makeText(context, jsonError, Toast.LENGTH_SHORT).show();
                        }
                        listener.onError(error.toString());
                    }
                }


            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            if (listener != null && context != null) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }

    public void putAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "ON RESPONSE " + response);
                            try {
                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    if (jsonString.equalsIgnoreCase("null")) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }
                                    if (classType == null) {
                                        listener.onSuccess(null, successMsg);
                                        return;
                                    }

                                    JSONObject object = new JSONObject(jsonString);
                                    Object model = gson.fromJson(object.toString(), classType);

                                    if (listener != null) {
                                        listener.onSuccess(model, successMsg);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        error.printStackTrace();
                        /*Log.e("Json Error", "Error: " + error
                                + "\nStatus Code " + error.networkResponse.statusCode
                                + "\nCause " + error.getCause()
                                + "\nnetworkResponse " + error.networkResponse.data.toString()
                                + "\nmessage" + error.getMessage());
                        listener.onError(error.toString());*/

                        String jsonError = new String(error.networkResponse.data);
                        Log.e("Json Error", "Error: " + error
                                + "\nStatus Code " + error.networkResponse.statusCode
                                + "\nCause " + error.getCause()
                                + "\nnetworkResponse " + jsonError
                                + "\nmessage" + error.getMessage());

                        if (jsonError != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonError);
                                if (jsonObject.getString("success").equalsIgnoreCase("0")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("error");
                                    listener.onError(jsonArray.getString(0));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.onError(error.toString());
                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");
                    params.put("Content-Type", "application/json");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {
            //Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            if (listener != null) {
            }
        }
    }

    public void putJSONArrayAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "RESPONSE  = " + response);
                            try {

                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    JSONArray jsonArray = new JSONArray(jsonString);
                                    ArrayList arrList = new ArrayList();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
                                        arrList.add(model);
                                    }

                                    if (listener != null) {
                                        listener.onSuccess(arrList, successMsg);
                                    }

                                    //  JSONObject object = new JSONObject(jsonString);
                                    //Object model = gson.fromJson(object.toString(), classType);

                                    //if (listener != null) {
                                    //  listener.onSuccess(model);
                                    //}
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        //listener.onError(error.toString());

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            // Print Error!
                            Toast.makeText(context, jsonError, Toast.LENGTH_SHORT).show();
                        }
                        listener.onError(error.toString());
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {

            if (listener != null && context != null) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }


    /**
     * EXTRA METHODS
     */
    public void getJSONArrayAPIADDRESS(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (ConnectionManager.Connection(context)) {
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "RESPONSE  = " + response);
                            try {

                                if (response.getString("success").equalsIgnoreCase("1")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("data");
                                    String successMsg = response.getString("error");

                                    JSONObject jsonObject = new JSONObject(jsonString);
                                    Object model = gson.fromJson(jsonObject.toString(), classType);

                                    if (listener != null) {
                                        listener.onSuccess(model, successMsg);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("error"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("error"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        listener.onError(error.toString());
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Oc-Merchant-Id", "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM");

                    String sessionID = new AppPreference(context).getSessionID();
                    String appLanguage = new AppPreference(context).getAppLanguage();

                    //if pref has sesssion value
                    if (sessionID != null) {
                        params.put("X-Oc-Session", sessionID);
                    }

                    if (appLanguage != null) {
                        params.put("X-Oc-Merchant-Language", appLanguage);
                    }
                    return params;
                }
            };

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {

            if (listener != null && context != null) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                listener.onError("Please check your internet connection");
            }
        }
    }
}

