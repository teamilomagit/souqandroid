package com.proweb.souq.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.annotation.StyleableRes;
import android.support.v4.content.res.ResourcesCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.proweb.souq.R;

public class CustomEditText extends LinearLayout {

    @StyleableRes
    int index0 = 0;
    @StyleableRes
    int index1 = 1;
    @StyleableRes
    int index2 = 2;
    @StyleableRes
    int index3 = 3;
    @StyleableRes
    int index4 = 6;
    //@StyleableRes
    //int index5 = 5;


    private Context context;
    private EditText edtText;
    private LinearLayout llTitle;
    private TextView txtTitle, txtMandatoryStar;

    public CustomEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        int[] sets = {R.attr.is_mandatory, R.attr.title, R.attr.title_color, R.attr.text_color, R.attr.input_type, R.attr.text_size, R.attr.maxlength};

        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        boolean isMandatory = typedArray.getBoolean(index0, false);
        String title = typedArray.getString(index1);
        int color = typedArray.getColor(index2, getResources().getColor(R.color.colorLightBlack));
        int textColor = typedArray.getColor(index3, getResources().getColor(R.color.colorLightBlack));
        int maxlength = typedArray.getInt(index4, 5);
        //float textSize = typedArray.getInt(index5,4);

        init();

        isMandatory(isMandatory);
        setTitle(title);
        setColor(color);
        setTextColor(textColor);
        setFontFamily();
        setTextSize();
        //setMaxLines(maxlength);
        //setInputType(inputType);
    }

    private void setMaxLines(int maxLines) {
        txtMandatoryStar.setText(maxLines);
    }

    public void init() {
        inflate(context, R.layout.layout_custom_edittext, this);

        edtText = findViewById(R.id.edt);
        llTitle = findViewById(R.id.ll_title);
        txtMandatoryStar = findViewById(R.id.txt_mandatory);
        txtTitle = findViewById(R.id.txt_title);
    }

    public void isMandatory(boolean value) {
        if (value) {
            txtMandatoryStar.setVisibility(VISIBLE);
        } else {
            txtMandatoryStar.setVisibility(GONE);
        }
    }

    public String getText() {
        return edtText.getText().toString();
    }

    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    public void setText(String value) {
        edtText.setText(value);
    }

    public void setColor(int color) {
        txtTitle.setTextColor(color);
    }

    public void setTextColor(int color) {
        edtText.setTextColor(color);
    }

    public void setInputType(int type) {
        edtText.setInputType(type);
    }

    public void setMaxCharacters(int value) {
        edtText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(value)});
    }

    public void setFontFamily() {
        Typeface typeface = ResourcesCompat.getFont(context, R.font.font_regular);
        txtMandatoryStar.setTypeface(typeface);
        edtText.setTypeface(typeface);
    }

    private void setTextSize() {
        edtText.setTextSize(14);
    }
}
