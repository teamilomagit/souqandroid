package com.proweb.souq.utils;

import android.content.Context;

import com.proweb.souq.R;
import com.proweb.souq.model.ContactUsModel;
import com.proweb.souq.model.OnBoardingModel;

import java.util.ArrayList;

public class Dataset {

    public static ArrayList<ContactUsModel> getContactList(Context context){
        ArrayList<ContactUsModel> list = new ArrayList<>();

        ContactUsModel contactUsModel = new ContactUsModel();
        contactUsModel.setZone(context.getString(R.string.zone1));
        contactUsModel.setAddress(context.getString(R.string.address1));
        contactUsModel.setContact(context.getString(R.string.contact));
        contactUsModel.setLatitude(30.397300);
        contactUsModel.setLongitude(47.719680);
        list.add(contactUsModel);

        contactUsModel = new ContactUsModel();
        contactUsModel.setZone(context.getString(R.string.zone2));
        contactUsModel.setAddress(context.getString(R.string.address2));
        contactUsModel.setContact(context.getString(R.string.contact));
        contactUsModel.setLatitude(36.218767);
        contactUsModel.setLongitude(44.011028);
        list.add(contactUsModel);

        contactUsModel = new ContactUsModel();
        contactUsModel.setZone(context.getString(R.string.zone3));
        contactUsModel.setAddress(context.getString(R.string.address3));
        contactUsModel.setContact(context.getString(R.string.contact));
        contactUsModel.setLatitude(31.085840);
        contactUsModel.setLongitude(46.246350);
        list.add(contactUsModel);
        return list;
    }

    public static ArrayList<OnBoardingModel> getOnboardingData(Context context){
        ArrayList<OnBoardingModel> list = new ArrayList<>();

        OnBoardingModel contactUsModel = new OnBoardingModel();
        contactUsModel.setIcon(R.mipmap.ic_search_board);
        contactUsModel.setHeading(context.getString(R.string.heading_screen0));
        contactUsModel.setDescription(context.getString(R.string.description_screen0));
        list.add(contactUsModel);

        contactUsModel = new OnBoardingModel();
        contactUsModel.setIcon(R.mipmap.ic_payment);
        contactUsModel.setHeading(context.getString(R.string.heading_screen1));
        contactUsModel.setDescription(context.getString(R.string.description_screen1));
        list.add(contactUsModel);

        contactUsModel = new OnBoardingModel();
        contactUsModel.setIcon(R.mipmap.ic_payment);
        contactUsModel.setHeading(context.getString(R.string.heading_screen2));
        contactUsModel.setDescription(context.getString(R.string.description_screen2));
        list.add(contactUsModel);

        contactUsModel = new OnBoardingModel();
        contactUsModel.setIcon(R.mipmap.ic_like_board);
        contactUsModel.setHeading(context.getString(R.string.heading_screen3));
        contactUsModel.setDescription(context.getString(R.string.description_screen3));
        list.add(contactUsModel);

        contactUsModel = new OnBoardingModel();
        contactUsModel.setIcon(R.mipmap.ic_computerlist);
        contactUsModel.setHeading(context.getString(R.string.heading_screen4));
        contactUsModel.setDescription(context.getString(R.string.description_screen4));
        list.add(contactUsModel);
        return list;
    }
}
