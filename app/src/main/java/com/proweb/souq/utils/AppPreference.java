package com.proweb.souq.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.proweb.souq.model.GuestModel;

import static android.content.Context.MODE_PRIVATE;

public class AppPreference {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    private final static String PREF_NAME = "Souq366";
    public static String PREF_SESSIONID = "sessionID";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    int PRIVATE_MODE = 0;

    public AppPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.context = context;
        this.editor = sharedPreferences.edit();
    }

    public String getSessionID() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getString(PREF_SESSIONID, null);
    }

    public void setSessionID(String value) {
        editor.putString(PREF_SESSIONID, value);
        editor.commit();
    }


    public String getCategoryList() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getString("category_list", null);
    }

    public void setCategoryList(String value) {
        editor.putString("category_list", value);
        editor.commit();
    }

    public int getCartCount() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getInt("cart_count", 0);
    }

    public void setCartCount(int value) {
        editor.putInt("cart_count", value);
        editor.commit();
    }

    public void setAppLanguage(String language) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putString("getAppLanguage", language);
        editor.commit();
    }

    public String getAppLanguage() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String value = prefs.getString("getAppLanguage", null);
        return value;
    }

    public boolean isFirstTimeLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }


    public GuestModel getGuestModel() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String value = prefs.getString("guest_model", null);
        return new Gson().fromJson(value, GuestModel.class);
    }

    public void setGuestModel(GuestModel guestModel) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit();
        editor.putString("guest_model", new Gson().toJson(guestModel));
        editor.commit();
    }
}
