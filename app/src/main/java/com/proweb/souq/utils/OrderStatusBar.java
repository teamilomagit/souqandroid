package com.proweb.souq.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.proweb.souq.R;

public class OrderStatusBar extends LinearLayout {

    private Context context;
    private ProgressBar progressBar;

    TextView[] txtArr = new TextView[4];


    public OrderStatusBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        init();
    }

    private void init() {
        inflate(context, R.layout.layout_order_status_bar, this);

        setProgressBar();
        addTextViewsToArray();
    }

    private void addTextViewsToArray() {
        TextView txt1 = findViewById(R.id.txt_1);
        TextView txt2 = findViewById(R.id.txt_2);
        TextView txt3 = findViewById(R.id.txt_3);
        TextView txt4 = findViewById(R.id.txt_4);

        txtArr[0] = txt1;
        txtArr[1] = txt2;
        txtArr[2] = txt3;
        txtArr[3] = txt4;
    }

    private void setProgressBar() {
        progressBar = findViewById(R.id.progress_bar);
    }

    public void setProgress(int progress) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, progress);
        animation.setDuration(500);
        animation.setInterpolator(new AccelerateInterpolator());
        animation.start();

        int limit = 0;

        try {
            limit = progress / 25;
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < limit; i++) {
            setTextViewDrawableColor(txtArr[i]);
        }

    }

    private void setTextViewDrawableColor(TextView textView) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), R.color.colorWhite), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
