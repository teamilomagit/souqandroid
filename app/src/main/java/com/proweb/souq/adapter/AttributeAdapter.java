package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.dialog.AttributeOptionsDialog;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.ProductOptionModel;
import com.proweb.souq.model.ProductOptionValueModel;

import java.util.ArrayList;

public class AttributeAdapter extends RecyclerView.Adapter<AttributeAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductOptionModel> list;
    AddressAdapterInterface mListener;

    public interface AddressAdapterInterface {
        void getSelectedPositionVal(ArrayList<ProductOptionModel> list);
    }

    public AttributeAdapter(Context context, ArrayList<ProductOptionModel> list, AddressAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    public void updateAdapter(ArrayList<ProductOptionModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ArrayList<ProductOptionValueModel> getSelectedOptions() {
        ArrayList<ProductOptionValueModel> tempList = new ArrayList<>();

        for (ProductOptionModel productOptionModel : list) {
            ProductOptionValueModel productOptionValueModel = new ProductOptionValueModel();
            productOptionValueModel = productOptionModel.getSelectedOptionVal();
            productOptionValueModel.setSelected_parent_id(productOptionModel.getProduct_option_id());
            tempList.add(productOptionModel.getSelectedOptionVal());
        }

        return tempList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_attribute, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final ProductOptionModel productOptionModel = list.get(position);

        ProductOptionValueModel selectedVal = productOptionModel.getSelectedOptionVal();

        if (selectedVal == null) {
            selectedVal = productOptionModel.getOption_value()[0];
            productOptionModel.setSelectedOptionVal(selectedVal);
        }

        holder.txtAttributeName.setText(productOptionModel.getName() + ":");

        if (!selectedVal.getPrice().equalsIgnoreCase("0")) {
            holder.txtAttributeVal.setText(selectedVal.getName() + " (" + selectedVal.getPrice_prefix()
                    + selectedVal.getPrice_formated() + ")");
        } else {
            holder.txtAttributeVal.setText(selectedVal.getName());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AttributeOptionsDialog dialog = new AttributeOptionsDialog(context, list, new AttributeOptionsDialog.AttributeOptionsDialogInterface() {
                    @Override
                    public void getUpdateList(ArrayList<ProductOptionModel> list) {
                        mListener.getSelectedPositionVal(list);
                    }
                });
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtAttributeName, txtAttributeVal;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAttributeName = itemView.findViewById(R.id.txt_attr_name);
            txtAttributeVal = itemView.findViewById(R.id.txt_attr_val);
        }
    }
}
