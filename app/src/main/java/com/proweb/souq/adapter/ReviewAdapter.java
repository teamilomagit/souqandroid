package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.ReviewDetailModel;
import com.proweb.souq.utils.AppUtils;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    Context context;
    ReviewDetailModel[] reviewArray;

    public ReviewAdapter(Context context, ReviewDetailModel[] reviewArray) {
        this.context = context;
        this.reviewArray = reviewArray;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_review,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            ReviewDetailModel reviewDetailModel = reviewArray[position];

            holder.txtUsername.setText(reviewDetailModel.getAuthor());
            holder.txtReview.setText(reviewDetailModel.getText());
            holder.txtReviewDate.setText(AppUtils.getFormattedDate(reviewDetailModel.getDate_added()));
            holder.txtReviewRate.setText(reviewDetailModel.getRating());
    }

    @Override
    public int getItemCount() {
        return reviewArray.length;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtReviewRate, txtUsername, txtReviewDate, txtReview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtReviewRate = itemView.findViewById(R.id.txt_review_rate);
            txtUsername = itemView.findViewById(R.id.txt_username);
            txtReviewDate = itemView.findViewById(R.id.txt_review_date);
            txtReview = itemView.findViewById(R.id.txt_review);
        }
    }

}
