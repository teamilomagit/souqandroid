package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.proweb.souq.R;
import com.proweb.souq.model.OnBoardingModel;

import java.util.ArrayList;

public class OnBoardingAdapter extends PagerAdapter{

    private ArrayList<OnBoardingModel> list;
    private LayoutInflater inflater;
    private Context context;

    public OnBoardingAdapter(ArrayList<OnBoardingModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_onboarding,container,false);
        ImageView imgIcon = view.findViewById(R.id.img_icon);
        TextView txtHeading = view.findViewById(R.id.txt_heading);
        TextView txtDescription = view.findViewById(R.id.txt_description);

        Glide.with(context).load(list.get(position).getIcon()).thumbnail(0.1f).placeholder(R.drawable.no_image).into(imgIcon);
        txtHeading.setText(list.get(position).getHeading());
        txtDescription.setText(list.get(position).getDescription());
        container.addView(view,0);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }
}
