package com.proweb.souq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.FilterOptionsActivity;
import com.proweb.souq.model.FilterGroupsModel;

import java.util.ArrayList;

public class FilterGroupAdapter extends RecyclerView.Adapter<FilterGroupAdapter.MyViewHolder> {

    Context context;
    ArrayList<FilterGroupsModel> list;
    FilterGroupAdapterInterface mListener;

    public interface FilterGroupAdapterInterface {
        void onGroupSelected(int position);
    }

    public void updateAdapter(ArrayList<FilterGroupsModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public FilterGroupAdapter(Context context, ArrayList<FilterGroupsModel> list, FilterGroupAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_filter_group, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        final FilterGroupsModel filterGroupsModel = list.get(position);
        myViewHolder.txtGroupName.setText(filterGroupsModel.getName());

        String options = filterGroupsModel.getSelectedValue();

        if (options == null) {
            //options = filterGroupsModel.getFilter()[0].getName();
            myViewHolder.txtVarietyName.setVisibility(View.GONE);
        }else {
            myViewHolder.txtVarietyName.setVisibility(View.VISIBLE);
            myViewHolder.txtVarietyName.setText(options);
        }



        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onGroupSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout llGroupBody;
        TextView txtGroupName, txtVarietyName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llGroupBody = itemView.findViewById(R.id.ll_group_body);
            txtGroupName = itemView.findViewById(R.id.txt_group_name);
            txtVarietyName = itemView.findViewById(R.id.txt_variety_name);
        }
    }
}
