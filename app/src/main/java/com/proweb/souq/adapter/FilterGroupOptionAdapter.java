package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.FilterModel;

import java.util.ArrayList;

public class FilterGroupOptionAdapter extends RecyclerView.Adapter<FilterGroupOptionAdapter.MyViewHolder>{

    Context context;
    ArrayList<FilterModel> list;
    String selectedOption;

    public void updateAdapter(ArrayList<FilterModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public FilterGroupOptionAdapter(Context context, ArrayList<FilterModel> list, String selectedOption) {
        this.context = context;
        this.list = list;
        this.selectedOption = selectedOption;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
         return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_filter_option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
        final FilterModel filterModel = list.get(position);
        myViewHolder.txtItemOption.setText(filterModel.getName());

        if (isSelected(filterModel)) {
            myViewHolder.chkOption.setChecked(true);
        } else {
            myViewHolder.chkOption.setChecked(false);
        }

        filterModel.setChecked(myViewHolder.chkOption.isChecked());

        myViewHolder.chkOption.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  filterModel.setChecked(myViewHolder.chkOption.isChecked());
              }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemOption;
        CheckBox chkOption;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemOption = itemView.findViewById(R.id.txt_item_option);
            chkOption = itemView.findViewById(R.id.chk_option);
        }
    }



    public ArrayList<FilterModel> getSelectedOptions() {
        ArrayList<FilterModel> tempArray = new ArrayList<>();

        //filter selected data
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked()) {
                tempArray.add(list.get(i));
            }
        }
        return tempArray;
    }

    private boolean isSelected(FilterModel filterModel) {

        if (selectedOption == null) {
            return false;
        }

        String[] values = selectedOption.split(",");
        for (String val : values) {
            if (val.trim().equalsIgnoreCase(filterModel.getFilter_id())) {
                return true;
            }
        }

        return false;
    }
}
