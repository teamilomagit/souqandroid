package com.proweb.souq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.ItemDetailActivity;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    public ArrayList<ProductModel> list;
    private boolean isHorizontal = false;
    private boolean isLoadingAdded = false;
    public static final int ITEM = 0;
    public static final int LOADING = 1;

    public ItemListAdapter(Context context, ArrayList<ProductModel> list) {
        this.context = context;
        this.list = list;
    }

    public ItemListAdapter(Context context, ArrayList<ProductModel> list, boolean isHorizontal) {
        this.context = context;
        this.list = list;
        this.isHorizontal = true;
    }


    public void  updateViewType(boolean isHorizontal)
    {

    }

    public void updateAdapter(ArrayList<ProductModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    /**
     * OVERRIDE METHODS
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:

                if (isHorizontal) {
                    viewHolder = new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext()).
                            inflate(R.layout.item_product_2, parent, false));
                } else {
                    viewHolder = new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext()).
                            inflate(R.layout.item_product, parent, false));
                }
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                final MyViewHolder myViewHolder = (MyViewHolder) holder;
                final ProductModel productModel = list.get(position);
                //myViewHolder.imgProduct.setImageResource(productModel.getImage());
                if (productModel.getImage() != null) {
                    Picasso.with(context).load(productModel.getImage()).into(myViewHolder.imgProduct);
                } else {
                    Picasso.with(context).load(productModel.getThumb()).into(myViewHolder.imgProduct);
                }
                myViewHolder.productName.setText(productModel.getName());
                myViewHolder.productPrice.setText(productModel.getPrice_formated());
                //new ItemDetailActivity().isFromWishlist = false;
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, ItemDetailActivity.class).putExtra("product_model", new Gson().toJson(productModel))
                        .putExtra("flag",false));
                    }
                });
                break;

            case LOADING:
//                Do nothing
                break;
        }
    }

    @Override
    public int getItemCount() {

        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /**
     * HELPERS
     */

    public void addAll(ArrayList<ProductModel> pmList) {
        for (ProductModel pd : pmList) {
            add(pd);
        }
    }

    public void add(ProductModel userModel) {
        list.add(userModel);
        notifyItemInserted(list.size() - 1);
    }

    public void remove(ProductModel userModel) {
        int position = list.indexOf(userModel);
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        list.clear();
        notifyDataSetChanged();
    }

    public ProductModel getItem(int position) {
        return list.get(position);
    }

    /**
     * ADD AND REMOVE LOADING FOOTER
     **/
    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ProductModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        if (list.size() > 0) {
            int position = list.size() - 1;
            ProductModel item = getItem(position);

            if (item != null) {
                list.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    /**
     * PUBLIC AND PRIVATE CLASSES
     */
    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct;
        TextView productName, productPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.img_product);
            productName = itemView.findViewById(R.id.product_name);
            productPrice = itemView.findViewById(R.id.product_price);
        }
    }
}
