package com.proweb.souq.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.dialog.SpinnerDialog;
import com.proweb.souq.model.WishListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyWishListAdapter extends RecyclerView.Adapter<MyWishListAdapter.MyViewHolder> implements
        AdapterView.OnItemSelectedListener {

    private Context context;
    private ArrayList<WishListModel> list;
    MyWishListAdapterInterface listener;
    CustomAlertDialog dialog;

    public interface MyWishListAdapterInterface{
        void onRemove(WishListModel wishListModel);
        void addToCart(WishListModel wishListModel);
    }

    public MyWishListAdapter(@NonNull Context context, ArrayList<WishListModel> list, MyWishListAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public void updateAdapter(ArrayList<WishListModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyWishListAdapter.MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_whishlist, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final WishListModel wishListModel = list.get(position);
        //Set data
        if(!wishListModel.getThumb().isEmpty()) {
            Picasso.with(context).load(wishListModel.getThumb()).into(holder.imgProduct);
        }
        holder.txtProductName.setText(wishListModel.getName());
        holder.txtProductPrice.setText(wishListModel.getPrice());
        holder.txtInStock.setText(wishListModel.getStock());
        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRemoveAlert(wishListModel);
            }
        });

        holder.rlAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  listener.addToCart(wishListModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView spinner;
        TextView txtProductName, txtProductPrice, txtInStock, txtRemove, txtAddToCart;
        RelativeLayout rlAddCart;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            spinner = itemView.findViewById(R.id.spinner);
            imgProduct = itemView.findViewById(R.id.img_product);
            txtAddToCart = itemView.findViewById(R.id.txt_add_to_cart);
            txtInStock = itemView.findViewById(R.id.txt_in_stock);
            txtProductName = itemView.findViewById(R.id.txt_product_name);
            txtProductPrice = itemView.findViewById(R.id.txt_product_price);
            txtRemove = itemView.findViewById(R.id.txt_remove);
            rlAddCart = itemView.findViewById(R.id.rl_add_cart);
        }
    }

    private void showRemoveAlert(final WishListModel wishListModel) {
        dialog = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                dialog.dismiss();
                listener.onRemove(wishListModel);
            }
        });
        dialog.setCancelable(false);
        dialog.show();
        dialog.setTitle(context.getString(R.string.msg_remove_wishlist));
    }
}




        /*AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.msg_remove_wishlist));
        builder.setNegativeButton(R.string.btn_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.btn_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 listener.onRemove(wishListModel);
            }
        });
        builder.setCancelable(false);
        builder.show();*/