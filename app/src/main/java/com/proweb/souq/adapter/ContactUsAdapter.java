package com.proweb.souq.adapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.proweb.souq.R;
import com.proweb.souq.model.ContactUsModel;
import java.util.ArrayList;

public class ContactUsAdapter extends RecyclerView.Adapter<ContactUsAdapter.MyViewHolder>{

    View view;
    Context context;
    ArrayList<ContactUsModel> list;

    public ContactUsAdapter(Context context, ArrayList<ContactUsModel> list) {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(ArrayList<ContactUsModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        view = LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_contact_us, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final ContactUsModel model = list.get(i);
        myViewHolder.txtZone.setText(model.getZone());
        myViewHolder.txtAddress.setText(model.getAddress());
        myViewHolder.txtContactNumber.setText(model.getContact());
        GoogleMap thisMap = myViewHolder.mapCurrent;
        if(thisMap != null) {
            LatLng cur_Latlng = new LatLng(model.getLatitude(), model.getLongitude());
            thisMap.addMarker(new MarkerOptions().position(cur_Latlng)
                    .title(context.getString(R.string.txt_marker_in)+" "+model.getZone()));
            thisMap.moveCamera(CameraUpdateFactory.newLatLng(cur_Latlng));
            thisMap.animateCamera(CameraUpdateFactory.zoomTo(4));
        }

        /*myViewHolder.txtContactNumber.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(model.getContact()));
                context.startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        TextView txtZone, txtAddress, txtContactNumber;
        GoogleMap mapCurrent;
        MapView map;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtZone = itemView.findViewById(R.id.txt_zone);
            txtAddress = itemView.findViewById(R.id.txt_address);
            txtContactNumber = itemView.findViewById(R.id.txt_contact_number);
            map = itemView.findViewById(R.id.map);
            if (map != null)
            {
                map.onCreate(null);
                map.onResume();
                map.getMapAsync(this);
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            mapCurrent = googleMap;
            notifyDataSetChanged();
        }
    }
}
