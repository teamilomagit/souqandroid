package com.proweb.souq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.AddAddressActivity;
import com.proweb.souq.model.AddressModel;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder>{

    Context context;
    ArrayList<AddressModel> list;
    AddressAdapterInterface mListener;

    public interface AddressAdapterInterface{
        void onDeleteItem(AddressModel addressModel);
        void onEditItem(AddressModel addressModel);
    }

    public AddressAdapter(Context context, ArrayList<AddressModel> list, AddressAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    public void updateAdapter(ArrayList<AddressModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_address, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final AddressModel addressModel = list.get(i);
        myViewHolder.addressType.setText(addressModel.getFirstname()+" "+addressModel.getLastname());
        myViewHolder.txtAddress.setText(addressModel.getAddress_1() +" "+addressModel.getAddress_2());
        myViewHolder.txtContactNumber.setText(addressModel.getPostcode());

        myViewHolder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onEditItem(addressModel);
            }
        });
        myViewHolder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 mListener.onDeleteItem(addressModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView addressType,txtAddress, txtContactNumber, txtEdit, txtDelete, txtDefault;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            addressType = itemView.findViewById(R.id.address_type);
            txtAddress = itemView.findViewById(R.id.txt_address);
            txtContactNumber = itemView.findViewById(R.id.txt_contact_number);
            txtEdit = itemView.findViewById(R.id.txt_edit);
            txtDelete = itemView.findViewById(R.id.txt_delete);
            txtDefault = itemView.findViewById(R.id.txt_default);
        }
    }
}
