package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.ProductOptionModel;
import com.proweb.souq.model.ProductOptionValueModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AttributeAdapter2 extends RecyclerView.Adapter<AttributeAdapter2.MyViewHolder> {

    Context context;
    ArrayList<ProductOptionModel> list;
    AttributeAdapter2Interface mListener;

    public interface AttributeAdapter2Interface {
        void getUpdateList(ArrayList<ProductOptionModel> list);
    }

    public AttributeAdapter2(Context context, ArrayList<ProductOptionModel> list, AttributeAdapter2Interface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    public void updateAdapter(ArrayList<ProductOptionModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_attr_option_val, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final ProductOptionModel productOptionModel = list.get(position);


        holder.txtAttributeName.setText(productOptionModel.getName() + ":");

        holder.txtAttributeVal.setText(productOptionModel.getSelectedOptionVal().getName());
        /*if (!productOptionModel.getSelectedOptionVal().getPrice().equalsIgnoreCase("0")) {
            holder.txtAttributeVal.setText(productOptionModel.getSelectedOptionVal().getName() + " (" + productOptionModel.getSelectedOptionVal().getPrice_prefix()
                    + productOptionModel.getSelectedOptionVal().getPrice_formated() + ")");
        } else {
            holder.txtAttributeVal.setText(productOptionModel.getSelectedOptionVal().getName());
        }*/

        final List<ProductOptionValueModel> tempList = Arrays.asList(productOptionModel.getOption_value());

        final OptionValueAdapter optionValueAdapter = (OptionValueAdapter) holder.recyclerView.getAdapter();
        optionValueAdapter.updateAdapter(productOptionModel.getSelectedOptionVal().getOption_value_id(), tempList, new OptionValueAdapter.OptionValueAdapterInterface() {
            @Override
            public void getSelectedOption(ProductOptionValueModel productOptionValueModel) {
//                Toast.makeText(context, "Selected value -> " + productOptionValueModel.getName(), Toast.LENGTH_SHORT).show();
                productOptionModel.setSelectedOptionVal(productOptionValueModel);
                optionValueAdapter.updateOptionId(productOptionValueModel.getOption_value_id());
                mListener.getUpdateList(list);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView txtAttributeName, txtAttributeVal;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAttributeName = itemView.findViewById(R.id.txt_attr_name);
            txtAttributeVal = itemView.findViewById(R.id.txt_attr_val);
            recyclerView = itemView.findViewById(R.id.recycler_view);

            setRecyclerView(recyclerView);
        }
    }

    private void setRecyclerView(RecyclerView recyclerView) {
        OptionValueAdapter mAdapter = new OptionValueAdapter(context, new ArrayList<ProductOptionValueModel>());
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(mAdapter);
    }

}
