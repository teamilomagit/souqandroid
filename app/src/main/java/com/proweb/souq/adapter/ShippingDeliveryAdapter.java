package com.proweb.souq.adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.proweb.souq.R;
import com.proweb.souq.model.ShippingDeliveryModel;
import java.util.ArrayList;

public class ShippingDeliveryAdapter extends RecyclerView.Adapter<ShippingDeliveryAdapter.MyViewHolder> {

    Context context;
    ArrayList<ShippingDeliveryModel> list;
    ShippingDeliveryAdapterInterface mListener;

    public interface ShippingDeliveryAdapterInterface{
        void onItemSelected(ShippingDeliveryModel shippingDeliveryModel);
    }

    public void updateAdapter(ArrayList<ShippingDeliveryModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public ShippingDeliveryAdapter(Context context, ArrayList<ShippingDeliveryModel> list, ShippingDeliveryAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_shipping_delivery,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
         final ShippingDeliveryModel shippingDeliveryModel = list.get(position);
         myViewHolder.txtZone.setText(shippingDeliveryModel.getName());
         myViewHolder.txtZoneCost.setText("$"+shippingDeliveryModel.getShipping_flat_cost());
         /*myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 mListener.onItemSelected(shippingDeliveryModel);
             }
         });*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txtZone, txtZoneCost;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtZone = itemView.findViewById(R.id.txt_zone);
            txtZoneCost = itemView.findViewById(R.id.txt_zone_cost);
        }
    }
}
