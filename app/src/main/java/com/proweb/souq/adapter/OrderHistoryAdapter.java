package com.proweb.souq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.ItemDetailActivity;
import com.proweb.souq.activity.OrderTrackActivity;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.model.OrderHistoryModel;
import com.proweb.souq.utils.AppUtils;
import com.proweb.souq.utils.OrderStatusBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements  AdapterView.OnItemSelectedListener{

    private Context context;
    public ArrayList<OrderHistoryModel> list;
    String[] quantity = { "01", "02", "03", "04", "05","06","07","08","09","10"};
    private boolean isLoadingAdded = false;
    public static final int ITEM = 0;
    public static final int LOADING = 1;
    
    public OrderHistoryAdapter(Context context, ArrayList<OrderHistoryModel> list)
    {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(ArrayList<OrderHistoryModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order_history, parent,false));

            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        
        switch (getItemViewType(position)) {
            case ITEM:
                final MyViewHolder myViewHolder = (MyViewHolder) holder;
                final OrderHistoryModel orderHistoryModel = list.get(position);
                myViewHolder.txtOrderNo.setText(context.getString(R.string.txt_order)+" "+orderHistoryModel.getOrder_id());
                myViewHolder.txtPlacedDate.setText(context.getString(R.string.txt_placed_on)+" "+ AppUtils.getFormattedDate(orderHistoryModel.getDate_added()));
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, OrderTrackActivity.class).putExtra("order_history_model",new Gson().toJson(orderHistoryModel)));
                    }
                });

                if (orderHistoryModel.getStatus().equalsIgnoreCase(context.getString(R.string.status_pending))){
                    myViewHolder.orderStatusBar.setProgress(25);
                }else if (orderHistoryModel.getStatus().equalsIgnoreCase(context.getString(R.string.status_processing))){
                    myViewHolder.orderStatusBar.setProgress(50);
                }else if (orderHistoryModel.getStatus().equalsIgnoreCase(context.getString(R.string.status_shipped))){
                    myViewHolder.orderStatusBar.setProgress(75);
                }else if (orderHistoryModel.getStatus().equalsIgnoreCase(context.getString(R.string.status_complete))){
                    myViewHolder.orderStatusBar.setProgress(100);
                }
                break;

            case LOADING:
//                Do nothing
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /**
     *ON ITEM SELECTED METHODS
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    

    /**
     * HELPERS
     */

    public void addAll(ArrayList<OrderHistoryModel> pmList) {
        for (OrderHistoryModel pd : pmList) {
            add(pd);
        }
    }

    public void add(OrderHistoryModel orderHistoryModel) {
        list.add(orderHistoryModel);
        notifyItemInserted(list.size() - 1);
    }

    public void remove(OrderHistoryModel orderHistoryModel) {
        int position = list.indexOf(orderHistoryModel);
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        list.clear();
        notifyDataSetChanged();
    }

    public OrderHistoryModel getItem(int position) {
        return list.get(position);
    }

    /**
     * ADD AND REMOVE LOADING FOOTER
     **/
    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new OrderHistoryModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        if (list.size() > 0) {
            int position = list.size() - 1;
            OrderHistoryModel item = getItem(position);

            if (item != null) {
                list.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    /**
     * PUBLIC AND PRIVATE CLASSES
     */
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView spinner;
        ImageView imgArrow;
        TextView txtOrderNo, txtPlacedDate;
        OrderStatusBar orderStatusBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            spinner = itemView.findViewById(R.id.spinner);
            imgArrow = itemView.findViewById(R.id.img_arrow);
            txtOrderNo = itemView.findViewById(R.id.txt_order_no);
            txtPlacedDate = itemView.findViewById(R.id.txt_placed_date);
            orderStatusBar = itemView.findViewById(R.id.order_status_bar);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
}




/*myViewHolder.spinner.setOnItemSelectedListener(this);
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,quantity);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.layout_spinner_item,quantity);
        myViewHolder.spinner.setAdapter(adapter);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  context.startActivity(new Intent(context, OrderTrackActivity.class));
            }
        });*/
