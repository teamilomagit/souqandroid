package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.AreaAddressModel;

import java.util.ArrayList;

public class AreaAddressAdapter extends RecyclerView.Adapter<AreaAddressAdapter.MyViewHolder> {

    Context context;
    ArrayList<AddressModel> list;
    AreaAddressAdapterInterface mListener;
    AddressModel referenceModel;
    String address;

    private String defaultAddress;

    public interface AreaAddressAdapterInterface {
        void onItemSelected(AddressModel addressModel);
    }

    public AreaAddressAdapter(Context context, ArrayList<AddressModel> list, AreaAddressAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    public void updateAdapter(String defaultAddress, ArrayList<AddressModel> list) {
        this.list = list;
        this.defaultAddress = defaultAddress;
        setSelectedModel();
        notifyDataSetChanged();
    }

    private void setSelectedModel() {
        for (AddressModel addressModel : list) {
            if (addressModel.getAddress_id().equalsIgnoreCase(defaultAddress)) {
                addressModel.setChecked(true);
                referenceModel = addressModel;
                mListener.onItemSelected(addressModel);
                break;
            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_select_address, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        final AddressModel addressModel = list.get(i);
        if (referenceModel == null) {
            referenceModel = addressModel;
        }


        address = addressModel.getAddress_1() + " " + addressModel.getAddress_2() + ", " + addressModel.getCity() + " - " + addressModel.getPostcode();
        myViewHolder.txtFullName.setText(addressModel.getFirstname() + " " + addressModel.getLastname());
        myViewHolder.txtAddress.setText(address);

        myViewHolder.selected.setChecked(addressModel.isChecked());

        myViewHolder.selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referenceModel.setChecked(false);
                addressModel.setChecked(true);
                referenceModel = addressModel;
                mListener.onItemSelected(addressModel);
                notifyDataSetChanged();
            }
        });

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referenceModel.setChecked(false);
                addressModel.setChecked(true);
                referenceModel = addressModel;
                mListener.onItemSelected(addressModel);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtAddress, txtFullName;
        RadioButton selected;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAddress = itemView.findViewById(R.id.txt_address);
            txtFullName = itemView.findViewById(R.id.txt_full_name);
            selected = itemView.findViewById(R.id.rdbtn);
        }
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}
