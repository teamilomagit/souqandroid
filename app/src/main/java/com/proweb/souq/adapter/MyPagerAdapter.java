package com.proweb.souq.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.proweb.souq.R;
import com.proweb.souq.model.BannerTypeModel;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class MyPagerAdapter extends PagerAdapter{

    private ArrayList<String> images;
    private LayoutInflater inflater;
    private Context context;

    public MyPagerAdapter(Context context, ArrayList<String> xmenArray) {
        this.context = context;
        this.images = xmenArray;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.slide,container,false);
        ImageView img = view.findViewById(R.id.image);
       /* Uri uri = Uri.parse(images.get(position).getName());
        img.setImageURI(uri);*/
        //Picasso.with(context).load(images.get(position)).into(img);
        Glide.with(context).load(images.get(position)).thumbnail(0.1f).placeholder(R.drawable.no_image).into(img);
        container.addView(view,0);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }
}
