package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.proweb.souq.R;
import com.proweb.souq.model.AreaModel;
import java.util.ArrayList;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.MyViewHolder>{

    Context context;
    ArrayList<AreaModel> list;
    PlaceAdapterInterface mListener;

    public interface PlaceAdapterInterface{
        void onItemSelected(AreaModel areaModel);
    }

    public void updateAdapter(ArrayList<AreaModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public PlaceAdapter(Context context, ArrayList<AreaModel> list, PlaceAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_spinner,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        final AreaModel areaModel = list.get(position);
        myViewHolder.txtItemName.setText(areaModel.getName());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemSelected(areaModel);
            }
        });
        // myViewHolder.s;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtItemName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemName = itemView.findViewById(R.id.txt_item_name);
        }
    }
}
