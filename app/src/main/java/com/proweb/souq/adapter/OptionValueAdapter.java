package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.AddressModel;
import com.proweb.souq.model.ProductOptionModel;
import com.proweb.souq.model.ProductOptionValueModel;

import java.util.ArrayList;
import java.util.List;

public class OptionValueAdapter extends RecyclerView.Adapter<OptionValueAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ProductOptionValueModel> list;
    private OptionValueAdapterInterface mListener;

    private String selectedOptionId;

    public interface OptionValueAdapterInterface {
        void getSelectedOption(ProductOptionValueModel productOptionValueModel);
    }

    public OptionValueAdapter(Context context, ArrayList<ProductOptionValueModel> list) {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(String selectedOptionId, List<ProductOptionValueModel> list, OptionValueAdapterInterface mListener) {
        this.list.clear();
        this.list.addAll(list);
        this.selectedOptionId = selectedOptionId;
        this.mListener = mListener;
        notifyDataSetChanged();
    }

    public void updateOptionId(String selectedOptionId) {
        this.selectedOptionId = selectedOptionId;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_option_val, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ProductOptionValueModel productOptionValueModel = list.get(position);

        if (productOptionValueModel.getOption_value_id().equalsIgnoreCase(selectedOptionId)) {
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_attr_bg_blue));
        } else {
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_attr_bg_white));
        }

        if (!productOptionValueModel.getPrice().equalsIgnoreCase("0")) {
            holder.txtOption.setText(productOptionValueModel.getName() + " ("+productOptionValueModel.getPrice_prefix()
                    + productOptionValueModel.getPrice_formated()+")" );
        } else {
            holder.txtOption.setText(productOptionValueModel.getName());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.getSelectedOption(productOptionValueModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlItem;
        TextView txtOption;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_item);
            txtOption = itemView.findViewById(R.id.txt_option);
        }
    }
}
