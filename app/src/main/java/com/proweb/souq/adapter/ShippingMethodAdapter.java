package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.FlatModel;

import org.json.JSONObject;

import java.util.ArrayList;

public class ShippingMethodAdapter extends RecyclerView.Adapter<ShippingMethodAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<FlatModel> list;
    private FlatModel selectedModel;
    private ShippingMethodAdapterInterface listener;

    public ShippingMethodAdapter(Context context, ArrayList<FlatModel> list, ShippingMethodAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public interface ShippingMethodAdapterInterface {
        void onClicked(FlatModel flatModel);
    }

    @NonNull
    @Override
    public ShippingMethodAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shipping_method, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShippingMethodAdapter.MyViewHolder holder, int position) {
        final FlatModel flatModel = list.get(position);

        if (selectedModel == null) {
            selectedModel = flatModel;
        }

        holder.rb.setChecked(flatModel.isChecked());

        holder.txtShippingName.setText(flatModel.getTitle());
        holder.txtShippingPrice.setText(flatModel.getText());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedModel.setChecked(false);
                flatModel.setChecked(true);

                selectedModel = flatModel;

                notifyDataSetChanged();

                listener.onClicked(flatModel);
            }
        });

        holder.rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedModel.setChecked(false);
                flatModel.setChecked(true);

                selectedModel = flatModel;

                notifyDataSetChanged();

                listener.onClicked(flatModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RadioButton rb;
        TextView txtShippingName, txtShippingPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rb = itemView.findViewById(R.id.rb);
            txtShippingName = itemView.findViewById(R.id.txt_shipping_method);
            txtShippingPrice = itemView.findViewById(R.id.txt_shipping_price);
        }
    }
}
