package com.proweb.souq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.ItemDetailActivity;
import com.proweb.souq.activity.ItemListActivity;
import com.proweb.souq.model.CategoryModel;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<CategoryModel> list;

    public HomeListAdapter(Context context, ArrayList<CategoryModel> menuList) {
        this.context = context;
        this.list = menuList;
    }

    public void updateAdapter(ArrayList<CategoryModel> menuList) {
        this.list = menuList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView txtTitle, txtViewAll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtViewAll = itemView.findViewById(R.id.txt_view_all);
            recyclerView = itemView.findViewById(R.id.recycler_view_item);

            setRecyclerView(recyclerView);
        }
    }

    private void setRecyclerView(RecyclerView recyclerView) {
        ItemListAdapter mItemListAdapter = new ItemListAdapter(context, new ArrayList<ProductModel>(), true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(mItemListAdapter);
    }

    @NonNull
    @Override
    public HomeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_home, parent, false);
        return new HomeListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.MyViewHolder holder, final int position) {

        final CategoryModel categoryModel = list.get(position);

        holder.txtTitle.setText(categoryModel.getName());

        holder.txtViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    context.startActivity(new Intent(context, ItemListActivity.class).putExtra("menu_model", new Gson().toJson(categoryModel)));
            }
        });

        /*if (position == 0) {
            holder.txtViewAll.setVisibility(View.GONE);
        } else {
            holder.txtViewAll.setVisibility(View.VISIBLE);
        }*/
        holder.txtViewAll.setVisibility(View.VISIBLE);


        if (categoryModel.getProductList() == null) {
            if (position == 0) {
                getDailyDeals(holder);
            } else {
                getProductsByCategoryAPI(holder, position);
            }
        } else {
            setAdapter(categoryModel.getProductList(), holder);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.startActivity(new Intent(context , ItemDetailActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    /**
     * DATA MANIPULATION
     **/

    private void getDailyDeals(final MyViewHolder holder) {
        String url = AppConstant.GET_DEAL_OF_DAY + "/limit/" + 5;
        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                //loader.dismiss();

                ArrayList<ProductModel> tempList = (ArrayList<ProductModel>) resultObj;
                list.get(0).setProductList(tempList);
                setAdapter(tempList, holder);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProductsByCategoryAPI(final MyViewHolder holder, final int position) {

        String url = AppConstant.GET_PRODUCTS_BY_CATEGORY_API + list.get(position).getCategory_id() + "/limit/" + 5 + "/page/" + 1;
        new APIManager().getJSONArrayAPI(url, null, ProductModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                ArrayList<ProductModel> tempList = (ArrayList<ProductModel>) resultObj;
                list.get(position).setProductList(tempList);

                if (list.get(position).getProductList().size() > 0) {
                    holder.itemView.setVisibility(View.VISIBLE);
                    setAdapter(tempList, holder);
                } else {
                    holder.itemView.setVisibility(View.GONE);
                    shouldItemShow(false, holder);
                }
            }

            @Override
            public void onError(String error) {

                shouldItemShow(false, holder);
//                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //makes items disappear if there are no products of the category
    private void shouldItemShow(boolean val, MyViewHolder holder) {
        ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();

        if (val) {
            holder.itemView.setLayoutParams(params);
        } else {
            params.height = 0;
            params.width = 0;
            holder.itemView.setLayoutParams(params);
        }

    }

    private void setAdapter(ArrayList<ProductModel> tempList, MyViewHolder holder) {
        ItemListAdapter myAdapter = (ItemListAdapter) holder.recyclerView.getAdapter();

        if (myAdapter != null) {
            myAdapter.updateAdapter(tempList);
        }
    }


}
