package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.FilterModel;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FilterModel> list;
    private FilterModel selectedModel;
    private SortAdapterInterface listener;

    public SortAdapter(Context context, ArrayList<FilterModel> list, SortAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public interface SortAdapterInterface
    {
        void onClicked(FilterModel filterModel);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sort_option, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final FilterModel filterModel = list.get(position);

        if (filterModel.isChecked() && selectedModel == null) {
            selectedModel = filterModel;
        }

        holder.chkOption.setChecked(filterModel.isChecked());

        holder.txtItemOption.setText(filterModel.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedModel.setChecked(false);
                filterModel.setChecked(true);
                selectedModel = filterModel;
                holder.chkOption.setChecked(!holder.chkOption.isChecked());

                listener.onClicked(filterModel);
                AppConstant.selectedSort = filterModel;
                notifyDataSetChanged();
            }
        });

        holder.chkOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chkOption.setChecked(holder.chkOption.isChecked());
                selectedModel.setChecked(false);
                filterModel.setChecked(true);
                selectedModel = filterModel;

                listener.onClicked(filterModel);
                AppConstant.selectedSort = filterModel;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemOption;
        RadioButton chkOption;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemOption = itemView.findViewById(R.id.txt_item_option);
            chkOption = itemView.findViewById(R.id.chk_option);
        }
    }
}
