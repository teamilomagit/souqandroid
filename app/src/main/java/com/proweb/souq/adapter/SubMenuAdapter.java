package com.proweb.souq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.proweb.souq.R;
import com.proweb.souq.activity.AddressActivity;
import com.proweb.souq.activity.ChangePasswordActivity;
import com.proweb.souq.activity.EditAccountActivity;
import com.proweb.souq.activity.MyWishlistActivity;
import com.proweb.souq.activity.OrderHistoryActivity;
import com.proweb.souq.model.CategoryModel;
import java.util.ArrayList;

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.MyViewHolder>  {

    Context context;
    ArrayList<CategoryModel> list;

    public SubMenuAdapter(Context context, ArrayList<CategoryModel> list) {
        this.context = context;
        this.list = list;
    }

    public void updateAdapter(ArrayList<CategoryModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SubMenuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new SubMenuAdapter.MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.item_account_options, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final SubMenuAdapter.MyViewHolder myViewHolder, final int position) {
        final CategoryModel model = list.get(position);
        myViewHolder.txtMenuName.setText(model.getName());
        if (position == 0) {
            myViewHolder.imgIcon.setImageResource(R.mipmap.ic_edit);
        }else if (position == 1){
            myViewHolder.imgIcon.setImageResource(R.mipmap.ic_password);
        }else if (position == 2){
            myViewHolder.imgIcon.setImageResource(R.mipmap.ic_wishlist);
        }else if (position == 3){
            myViewHolder.imgIcon.setImageResource(R.mipmap.ic_order);
        }else if (position == 4){
            myViewHolder.imgIcon.setImageResource(R.mipmap.ic_address);
        }

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    context.startActivity(new Intent(context, EditAccountActivity.class));
                }else if (position == 1){
                    context.startActivity(new Intent(context, ChangePasswordActivity.class));
                }else if (position == 2){
                    context.startActivity(new Intent(context, MyWishlistActivity.class));
                }else if (position == 3){
                    context.startActivity(new Intent(context, OrderHistoryActivity.class));
                }else if (position == 4){
                    context.startActivity(new Intent(context, AddressActivity.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtMenuName;
        ImageView imgIcon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtMenuName = itemView.findViewById(R.id.txt_menu_name);
            imgIcon = itemView.findViewById(R.id.img_icon);
        }
    }
}
