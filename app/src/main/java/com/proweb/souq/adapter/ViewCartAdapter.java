package com.proweb.souq.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.activity.OrderReviewActivity;
import com.proweb.souq.activity.OrderTrackActivity;
import com.proweb.souq.activity.ViewCartActivity;
import com.proweb.souq.dialog.CustomAlertDialog;
import com.proweb.souq.dialog.RatingReviewDialog;
import com.proweb.souq.dialog.SpinnerDialog;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.AppPreference;
import com.proweb.souq.utils.CustomProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewCartAdapter extends RecyclerView.Adapter<ViewCartAdapter.MyViewHolder> {
    @NonNull
    private Context context;
    private ArrayList<ProductModel> list;
    ViewCartAdapterInterface listener;
    CustomAlertDialog dialog;
    RatingReviewDialog ratingReviewDialog;
    CustomProgressBar customProgressBar;

    private boolean shouldShowStock;

    public interface ViewCartAdapterInterface {
        void getDeleteSuccess();

        void onItemSelected(String item, int position);

        void setItemQuantity(TextView itemView, int position);

        void isAnyItemOutOfStock();
    }

    public void updateAdapter(ArrayList<ProductModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ViewCartAdapter(@NonNull Context context, ArrayList<ProductModel> list, ViewCartAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        customProgressBar = new CustomProgressBar(context);
    }

    public ViewCartAdapter(@NonNull Context context, ArrayList<ProductModel> list, boolean shouldShowStock, ViewCartAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.shouldShowStock = shouldShowStock;
        customProgressBar = new CustomProgressBar(context);
    }

    @Override
    public ViewCartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewCartAdapter.MyViewHolder(LayoutInflater.from(parent.getRootView().getContext()).inflate(R.layout.item_view_cart, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewCartAdapter.MyViewHolder holder, final int position) {

        final ProductModel model = list.get(position);
        holder.txtItemName.setText(model.getName());
        holder.txtPrise.setText(model.getPrice());

        if (model.getThumb() != null) {
            if (!model.getThumb().equalsIgnoreCase("")) {
                Picasso.with(context)
                        .load(model.getThumb())
                        .into(holder.imgItem);
            }
        } else {
            if (model.getImage() != null && !model.getImage().equalsIgnoreCase("")) {
                Picasso.with(context)
                        .load(model.getImage())
                        .into(holder.imgItem);
            }
        }


        holder.rlSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog(holder, position);
            }
        });

        holder.spinner.setText(String.valueOf(model.getQuantity()));

        //Remove item
        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteAlert(model, position);
            }
        });

        if (context instanceof ViewCartActivity) {
            holder.txtRemove.setVisibility(View.VISIBLE);
            holder.rlSpinnerItem.setVisibility(View.VISIBLE);
            holder.llEstimatedInfo.setVisibility(View.GONE);
        } else {
            holder.txtRemove.setVisibility(View.GONE);
            holder.rlSpinnerItem.setVisibility(View.GONE);
            holder.rlExtra.setVisibility(View.GONE);
            //holder.llEstimatedInfo.setVisibility(View.GONE);
            listener.setItemQuantity(holder.txtItemQuantity, position);
        }

        if (context instanceof OrderTrackActivity) {
            holder.txtWriteReview.setVisibility(View.VISIBLE);
            holder.txtWriteReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRateReviewDialog(model);
                }
            });
        } else {
            holder.txtWriteReview.setVisibility(View.GONE);
        }


        if (shouldShowStock) {
            if (model.isStock()) {
                holder.txtStockStatus.setVisibility(View.GONE);
            } else {
                listener.isAnyItemOutOfStock();
                holder.txtStockStatus.setVisibility(View.VISIBLE);
            }
        }

    }

    private void showRateReviewDialog(ProductModel model) {
        ratingReviewDialog = new RatingReviewDialog(context, model);
        ratingReviewDialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    /**** API *****/
    private void deleteCartAPI(ProductModel productModel, final int position) {
        customProgressBar.show();
        final AppPreference appPreference = new AppPreference(context);
        String url = AppConstant.GET_CART_API + "/" + productModel.getKey();

        new APIManager().deleteAPI(url, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                customProgressBar.dismiss();
                int val = appPreference.getCartCount();
                appPreference.setCartCount(--val);
                Toast.makeText(context, context.getString(R.string.msg_success_delete_cart), Toast.LENGTH_SHORT).show();
                list.remove(position);
                updateAdapter(list);
                if (listener != null) {
                    listener.getDeleteSuccess();
                }
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
            }
        });
    }

    /**
     * DIALOG
     */
    private void showDeleteAlert(final ProductModel model, final int position) {
        dialog = new CustomAlertDialog(context, new CustomAlertDialog.CustomAlertDialogInterface() {
            @Override
            public void onPositive() {
                dialog.dismiss();
                deleteCartAPI(model, position);
            }
        });
        dialog.setCancelable(false);
        dialog.show();
        dialog.setTitle(context.getString(R.string.msg_delete_cart));
    }

    private void showSpinnerDialog(final MyViewHolder holder, final int position) {
        SpinnerDialog spinnerDialog = new SpinnerDialog(context, context.getString(R.string.lbl_quantity), new SpinnerDialog.SpinnerDialogInterface() {
            @Override
            public void onItemSelected(String item) {
                listener.onItemSelected(item, position);
                holder.spinner.setText(item);
            }
        });
        spinnerDialog.setCancelable(false);
        spinnerDialog.show();
    }

    /**
     * PRIVATE PUBLIC CLASS
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtItemName, txtPrise, txtRemove, txtItemQuantity, txtEstimatedDelivery, txtWriteReview, txtStockStatus;
        ImageView imgItem;
        TextView spinner;
        LinearLayout llEstimatedInfo;
        RelativeLayout rlSpinnerItem;
        RelativeLayout rlSpinner, rlExtra;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItem = itemView.findViewById(R.id.img_item);
            txtItemName = itemView.findViewById(R.id.txt_item_name);
            txtPrise = itemView.findViewById(R.id.txt_item_prise);
            txtRemove = itemView.findViewById(R.id.txt_remove);
            txtStockStatus = itemView.findViewById(R.id.txt_item_status);
            llEstimatedInfo = itemView.findViewById(R.id.ll_estimated_info);
            txtItemQuantity = itemView.findViewById(R.id.txt_item_quantity);
            txtEstimatedDelivery = itemView.findViewById(R.id.txt_estimated_delivery);
            rlSpinner = itemView.findViewById(R.id.rl_spinner);
            rlSpinnerItem = itemView.findViewById(R.id.rl_spinner_item);
            rlExtra = itemView.findViewById(R.id.rl_extra);
            spinner = itemView.findViewById(R.id.spinner);
            txtWriteReview = itemView.findViewById(R.id.txt_write_review);
        }
    }
}


//Creating the ArrayAdapter instance having the country list
        /*ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,quantity);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.layout_spinner_item,quantity);
        holder.spinner.setAdapter(adapter);

        if (model.getQuantity() >= 10){
            holder.spinner.setSelection(9);
        }else {
            holder.spinner.setSelection(model.getQuantity()-1);
        }

        //Spinner item selected
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                listener.onItemSelected(parent.getItemAtPosition(i).toString(),position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/