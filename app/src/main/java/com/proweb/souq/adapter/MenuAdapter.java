package com.proweb.souq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.proweb.souq.R;
import com.proweb.souq.activity.ItemListActivity;
import com.proweb.souq.model.CategoryModel;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {
    Context context;
    ArrayList<CategoryModel>list;
    MenuAdapterInterface listener;

    public interface MenuAdapterInterface
    {
        void getItemClicked();
    }

    public MenuAdapter(Context context, ArrayList<CategoryModel> list, MenuAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public void updateAdapter(ArrayList<CategoryModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MenuAdapter.MyViewHolder(LayoutInflater.from(parent.getRootView().getContext())
                .inflate(R.layout.list_item_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        final CategoryModel model = list.get(position);
        myViewHolder.txtMenuName.setText(model.getName());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.getItemClicked();
                context.startActivity(new Intent(context,ItemListActivity.class).putExtra("menu_model",new Gson().toJson(model)));
               // ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtMenuName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtMenuName = itemView.findViewById(R.id.txt_menu_name);
        }
    }
}
