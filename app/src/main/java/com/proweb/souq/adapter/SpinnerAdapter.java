package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.model.SpinnerModel;

import java.util.ArrayList;

public class SpinnerAdapter extends RecyclerView.Adapter<SpinnerAdapter.MyViewHolder>{

    Context context;
    ArrayList<SpinnerModel> list;
    SpinnerAdapterInterface mListener;

    public interface SpinnerAdapterInterface{
        void onItemSelected(int position);
    }

    public void updateAdapter(ArrayList<SpinnerModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public SpinnerAdapter(Context context, ArrayList<SpinnerModel> list, SpinnerAdapterInterface mListener) {
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_spinner,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        SpinnerModel spinnerModel = list.get(position);
        myViewHolder.txtItemName.setText(spinnerModel.getName());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemSelected(position);
            }
        });
       // myViewHolder.s;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtItemName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemName = itemView.findViewById(R.id.txt_item_name);
        }
    }
}
