package com.proweb.souq.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.proweb.souq.R;
import com.proweb.souq.model.GalleryModel;

import java.util.ArrayList;

public class ImagePagerAdapter extends PagerAdapter {

    ArrayList<String> list;
    private LayoutInflater inflater;
    private Context context;

    public ImagePagerAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.item_slide_fullimage, view, false);
        ZoomageView myImage = myImageLayout
                .findViewById(R.id.image);

        //String imgPath = AppConstant.API_BASE_URL + list.get(position).getImage_url();
        String imgPath = list.get(position);
        Glide.with(context).load(imgPath).thumbnail(0.4f).into(myImage);
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    /*public void deleteImage(ArrayList<GalleryModel> list)
    {
        this.list = list;
        notifyDataSetChanged();
    }*/

}
