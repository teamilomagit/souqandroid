package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.model.ProductModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;
import com.proweb.souq.utils.CustomProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

public class RatingReviewDialog extends Dialog {

    Context context;
    TextView btnSubmit;
    EditText edtWriteReview, edtName;
    RatingBar rateBar;
    CustomProgressBar customProgressBar;
    ProductModel productModel;

    interface RatingReviewDialogInterface{
        void onSubmit();
    }

    public RatingReviewDialog(@NonNull Context context, ProductModel model) {
        super(context);
        this.context = context;
        this.productModel = model;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_rating_review);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        //Method calling
        init();
        setListeners();
    }

    private void init() {
        btnSubmit = findViewById(R.id.btn_submit);
        edtName = findViewById(R.id.edt_name);
        edtWriteReview = findViewById(R.id.edt_write_review);
        rateBar = findViewById(R.id.rate_bar);
        customProgressBar = new CustomProgressBar(context);
    }

    private void setListeners() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                     postReviewAPI();
                }
            }
        });
    }

    private void postReviewAPI() {
        customProgressBar.show();
        JSONObject params = new JSONObject();
        try {
            params.put("name",edtName.getText().toString());
            params.put("text",edtWriteReview.getText().toString());
            params.put("rating",rateBar.getRating());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = AppConstant.GET_PRODUCT_DETAIL_API+"/"+productModel.getProduct_id()+"/review";
        new APIManager().postJSONArrayAPI(url, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                dismiss();
                customProgressBar.dismiss();
                Toast.makeText(context,context.getString(R.string.msg_success_rated), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String error) {
                customProgressBar.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isValid() {
        /*if (edtName.getText().toString().isEmpty()){
            Toast.makeText(context, context.getString(R.string.err_name), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtWriteReview.getText().toString().length() < 3 || edtWriteReview.getText().toString().length() > 25){
            Toast.makeText(context, context.getString(R.string.err_write_25), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtWriteReview.getText().toString().isEmpty()){
            Toast.makeText(context, context.getString(R.string.err_write_review), Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtWriteReview.getText().toString().length() < 25 || edtWriteReview.getText().toString().length() > 1000){
            Toast.makeText(context, context.getString(R.string.err_write_25), Toast.LENGTH_SHORT).show();
            return false;
        }else */
        if (rateBar.getRating() == 0){
            Toast.makeText(context, context.getString(R.string.err_rate_0), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
