package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.adapter.ShippingDeliveryAdapter;
import com.proweb.souq.model.ShippingDeliveryModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;

public class ShippingDeliveryDialog extends Dialog {

    Context context;
    ArrayList<ShippingDeliveryModel> list;
    TextView txtTitle, txtNoData;
    ImageView imgClose;
    RecyclerView recyclerView;
    ProgressBar progressbar;
    ShippingDeliveryAdapter mAdapter;
    ShippingDeliveryDialogInterface mListener;

    public interface ShippingDeliveryDialogInterface{
        void onItemSelected(ShippingDeliveryModel shippingDeliveryModel);
    }

    public ShippingDeliveryDialog(@NonNull Context context, ShippingDeliveryDialogInterface mListener) {
        super(context);
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_shipping_delivery);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        init();
    }

    private void init() {
        txtTitle = findViewById(R.id.txt_title);
        imgClose = findViewById(R.id.img_close);
        recyclerView = findViewById(R.id.recycler_view);
        progressbar = findViewById(R.id.progressbar);
        txtNoData = findViewById(R.id.txt_no_data);
        list = new ArrayList<>();
        txtTitle.setText(context.getString(R.string.title_shipping_delivery));

        //Method calling
        setListeners();
        setUpRecyclerview();
        getShippingDeliveryList();
    }

    private void setUpRecyclerview() {
        mAdapter = new ShippingDeliveryAdapter(context, list, new ShippingDeliveryAdapter.ShippingDeliveryAdapterInterface() {
            @Override
            public void onItemSelected(ShippingDeliveryModel shippingDeliveryModel) {
                mListener.onItemSelected(shippingDeliveryModel);
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void setListeners() {
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    /**
     * API CALLING
     */
    private void getShippingDeliveryList() {
        progressbar.setVisibility(View.VISIBLE);
        new APIManager().getJSONArrayAPI(AppConstant.GET_ZONE_SHIPPING, null,ShippingDeliveryModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressbar.setVisibility(View.GONE);
                list = (ArrayList<ShippingDeliveryModel>) resultObj;

                if (list.size()>0){
                    recyclerView.setVisibility(View.VISIBLE);
                    txtNoData.setVisibility(View.GONE);
                    mAdapter.updateAdapter(list);
                }else {
                    recyclerView.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(String error) {
                progressbar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                txtNoData.setVisibility(View.VISIBLE);
                Log.e("TAG",error);
            }
        });
    }
}
