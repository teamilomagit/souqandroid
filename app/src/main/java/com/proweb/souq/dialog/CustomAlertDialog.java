package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proweb.souq.R;

import java.util.ArrayList;

public class CustomAlertDialog extends Dialog {

    Context context;
    public TextView txtMessage, txtNegative, txtPositive;
    CustomAlertDialogInterface mListener;

    public interface CustomAlertDialogInterface{
        void onPositive();
    }

    public CustomAlertDialog(@NonNull Context context, CustomAlertDialogInterface mListener) {
        super(context);
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_alert);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        init();
    }

    private void init() {
        txtMessage = findViewById(R.id.txt_message);
        txtNegative = findViewById(R.id.txt_negative);
        txtPositive = findViewById(R.id.txt_positive);

        //Method calling
        setListeners();
    }

    private void setListeners() {
        txtNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        txtPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPositive();
            }
        });
    }

    public void setTitle(String title){
        txtMessage.setText(title);
    }
}
