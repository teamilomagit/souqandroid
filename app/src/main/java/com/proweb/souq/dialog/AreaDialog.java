package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.adapter.PlaceAdapter;
import com.proweb.souq.adapter.SpinnerAdapter;
import com.proweb.souq.model.AreaModel;
import com.proweb.souq.model.CountryModel;
import com.proweb.souq.model.SpinnerModel;
import com.proweb.souq.model.ZoneModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;

public class AreaDialog extends Dialog {

    Context context;
    PlaceAdapter mAdapter;
    TextView txtTitle, txtNoData;
    ImageView imgClose;
    RecyclerView recyclerView;
    String title;
    AreaDialogInterface mListener;
    int country_id;
    ArrayList<ZoneModel> zoneList;
    ProgressBar progressBar;

    public interface AreaDialogInterface {
        void onItemSelected(AreaModel areaModel);
    }

    public AreaDialog(@NonNull Context context, int cid, String title, AreaDialogInterface mListener) {
        super(context);
        this.context = context;
        this.mListener = mListener;
        this.title = title;
        this.country_id = cid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_area);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        init();
//        setUpRecyclerView();
    }

    private void init() {
        txtTitle = findViewById(R.id.txt_title);
        imgClose = findViewById(R.id.img_close);
//        txtNoData = findViewById(R.id.txt_no_data);

        progressBar = findViewById(R.id.progressbar);
        txtTitle.setText(title);

        //Method calling
        setListeners();
        setUpRecyclerView();
    }

    private void setListeners() {
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setUpRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);

        mAdapter = new PlaceAdapter(context, new ArrayList<AreaModel>(), new PlaceAdapter.PlaceAdapterInterface() {
            @Override
            public void onItemSelected(AreaModel areaModel) {
                mListener.onItemSelected(areaModel);
                country_id = Integer.parseInt(areaModel.getId());
                dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter);

        if (title.equalsIgnoreCase(context.getString(R.string.lbl_select_country))) {
            getCountries();
        } else {
            getZone();
        }
    }

    /**
     * API CALLING
     */
    private void getCountries() {
        progressBar.setVisibility(View.VISIBLE);
        new APIManager().getJSONArrayAPI(AppConstant.GET_COUNTRIES, null, CountryModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressBar.setVisibility(View.GONE);
                ArrayList<CountryModel> list = (ArrayList<CountryModel>) resultObj;
                ArrayList<AreaModel> tempList = new ArrayList<>();

                for (CountryModel country : list) {
                    AreaModel areaModel = new AreaModel();
                    areaModel.setId(country.getCountry_id());
                    areaModel.setName(country.getName());
                    tempList.add(areaModel);
                }
                if (tempList.size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
//                    txtNoData.setVisibility(View.GONE);
                    mAdapter.updateAdapter(tempList);
                } else {
                    recyclerView.setVisibility(View.GONE);
//                    txtNoData.setVisibility(View.VISIBLE);
                }
                //mAdapter.updateAdapter(tempList);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
//                txtNoData.setVisibility(View.VISIBLE);
                Log.e("TAG", error);
            }
        });
    }

    private void getZone() {
        progressBar.setVisibility(View.VISIBLE);
        String url = AppConstant.GET_ZONES + "/" + country_id;
        new APIManager().getAPI(url, CountryModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj, String msg) {
                progressBar.setVisibility(View.GONE);
                CountryModel countryModel = (CountryModel) resultObj;
                zoneList = countryModel.getZone();
                ArrayList<AreaModel> tempList = new ArrayList<>();
                for (ZoneModel zone : zoneList) {
                    AreaModel areaModel = new AreaModel();
                    areaModel.setId(zone.getZone_id());
                    areaModel.setName(zone.getName());
                    tempList.add(areaModel);
                }
                if (tempList.size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
//                    txtNoData.setVisibility(View.GONE);
                    mAdapter.updateAdapter(tempList);
                } else {
                    recyclerView.setVisibility(View.GONE);
//                    txtNoData.setVisibility(View.VISIBLE);
                }
                //mAdapter.updateAdapter(tempList);
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
//                txtNoData.setVisibility(View.VISIBLE);
                Log.e("TAG", error);
            }
        });
    }
}

