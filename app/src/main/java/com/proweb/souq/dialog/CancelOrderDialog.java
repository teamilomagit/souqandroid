package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.proweb.souq.R;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

public class CancelOrderDialog extends Dialog {

    Context context;
    TextView txtOrderNo, btnSubmit;
    EditText edtWriteReason;
    private CancelOrderDialogInterface listener;

    private String orderId = "";


    public CancelOrderDialog(@NonNull Context context, String orderId, CancelOrderDialogInterface listener) {
        super(context);
        this.context = context;
        this.orderId = orderId;
        this.listener = listener;
    }

    public interface CancelOrderDialogInterface {
        void clickedCancel(String comment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cancel_order);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        init();
    }

    private void init() {
        txtOrderNo = findViewById(R.id.txt_order_no);
        edtWriteReason = findViewById(R.id.edt_write_reason);
        btnSubmit = findViewById(R.id.btn_submit);

        txtOrderNo.setText("Order Id : " + orderId);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtWriteReason.getText().toString().length() == 0) {
                    Toast.makeText(context, "Please mention your reason for cancelling order.", Toast.LENGTH_SHORT).show();
                } else {
                    dismiss();
                    listener.clickedCancel(edtWriteReason.getText().toString());
                }
            }
        });
    }


}
