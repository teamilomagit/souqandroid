package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.activity.HomeActivity;
import com.proweb.souq.activity.OrderReviewActivity;
import com.proweb.souq.activity.PaymentAddressActivity;
import com.proweb.souq.activity.ViewCartActivity;
import com.squareup.picasso.Picasso;

public class AddtoCartDialog extends Dialog {
    Context context;
    private TextView txtCheckout, txtContinueShopping, txtItemName, txtItemPrise;
    private ImageView imgItem;
    String itemName, itemPrise, imgPath;
    private View viewBg;


    public AddtoCartDialog(Context context, String itemName, String itemPrise, String imgPath) {
        super(context);
        this.imgPath = imgPath;
        this.itemPrise = itemPrise;
        this.itemName = itemName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_to_cart);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        init();
    }

    private void init() {
        context = getContext();
        setCancelable(false);
        txtCheckout = findViewById(R.id.txt_checkout);
        txtItemName = findViewById(R.id.txt_item_name);
        txtItemPrise = findViewById(R.id.txt_item_prise);
        viewBg = findViewById(R.id.view_bg);
        imgItem = findViewById(R.id.img_item);
        txtContinueShopping = findViewById(R.id.txt_continue_shopping);
        setClickListener();

        txtItemName.setText(itemName);
        txtItemPrise.setText(itemPrise);
        Picasso.with(context)
                .load(imgPath)
                .into(imgItem);

    }

    private void setClickListener() {
        txtContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        txtCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //context.startActivity(new Intent(context, OrderReviewActivity.class));
                context.startActivity(new Intent(context, ViewCartActivity.class));
                dismiss();
            }
        });
        viewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
