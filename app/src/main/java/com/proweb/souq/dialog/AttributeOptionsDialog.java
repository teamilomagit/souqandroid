package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.adapter.AttributeAdapter2;
import com.proweb.souq.adapter.PlaceAdapter;
import com.proweb.souq.model.AreaModel;
import com.proweb.souq.model.CountryModel;
import com.proweb.souq.model.ProductOptionModel;
import com.proweb.souq.model.ZoneModel;
import com.proweb.souq.utils.APIManager;
import com.proweb.souq.utils.AppConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AttributeOptionsDialog extends Dialog {

    private Context context;
    private TextView txtDone;
    private ArrayList<ProductOptionModel> list;
    private AttributeOptionsDialogInterface listener;

    public interface AttributeOptionsDialogInterface {
        void getUpdateList(ArrayList<ProductOptionModel> list);
    }

    public AttributeOptionsDialog(@NonNull Context context, ArrayList<ProductOptionModel> list, AttributeOptionsDialogInterface listener) {
        super(context);
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_attr_options);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        init();
    }

    private void init() {
        txtDone = findViewById(R.id.txt_done);

        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setUpRecyclerView();
    }


    private void setUpRecyclerView() {

        AttributeAdapter2 mAdapter = new AttributeAdapter2(context, list, new AttributeAdapter2.AttributeAdapter2Interface() {
            @Override
            public void getUpdateList(ArrayList<ProductOptionModel> list) {
                listener.getUpdateList(list);
            }
        });

        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }


}

