package com.proweb.souq.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proweb.souq.R;
import com.proweb.souq.activity.AddAddressActivity;
import com.proweb.souq.adapter.SpinnerAdapter;
import com.proweb.souq.model.SpinnerModel;

import java.util.ArrayList;

public class SpinnerDialog extends Dialog {

    Context context;
    SpinnerAdapter mAdapter;
    TextView txtTitle, txtNoData;
    ImageView imgClose;
    RecyclerView recyclerView;
    ArrayList<SpinnerModel> list;
    ArrayList<String> genderList;
    String title;
    SpinnerDialogInterface mListener;
    SpinnerModel spinnerModel = null;

    public interface SpinnerDialogInterface{
        void onItemSelected(String item);
    }

    public SpinnerDialog(@NonNull Context context,String title, SpinnerDialogInterface mListener) {
        super(context);
        this.context = context;
        this.mListener = mListener;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_spinner);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        init();
    }

    private void init() {
        txtTitle = findViewById(R.id.txt_title);
        txtNoData = findViewById(R.id.txt_no_data);
        imgClose = findViewById(R.id.img_close);
        recyclerView = findViewById(R.id.recycler_view);
        list = new ArrayList<>();
        genderList = new ArrayList<>();
        txtTitle.setText(title);

        //Method calling
        setListeners();
        setUpRecyclerview();
        if (title.equalsIgnoreCase(context.getString(R.string.lbl_quantity))) {
            getSpinnerList();
        }else {
            getGender();
        }
    }

    private void setListeners() {
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setUpRecyclerview() {
        mAdapter = new SpinnerAdapter(context, list, new SpinnerAdapter.SpinnerAdapterInterface() {
            @Override
            public void onItemSelected(int position) {
                    mListener.onItemSelected(list.get(position).getName());
                    dismiss();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    /**
     * API CALLING
     */
    private void getSpinnerList() {
        for (int i=1;i<=10;i++){
            spinnerModel = new SpinnerModel();
            spinnerModel.setName(String.valueOf(i));
            list.add(spinnerModel);
        }
        mAdapter.updateAdapter(list);
    }

    private void getGender() {
        spinnerModel = new SpinnerModel();
        spinnerModel.setName(context.getString(R.string.male));
        list.add(spinnerModel);

        spinnerModel = new SpinnerModel();
        spinnerModel.setName(context.getString(R.string.female));
        list.add(spinnerModel);
        mAdapter.updateAdapter(list);
    }
}
