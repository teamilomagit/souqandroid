package com.ilomatech.crosstwopageindicator;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class CrossTwoPageIndicator extends LinearLayout {

    private Context context;
    private RecyclerView mRecyclerView;
    private PageControlAdapter mAdapter;

    public CrossTwoPageIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        init();
    }

    private void init() {
        inflate(context, R.layout.layout_page_indicator, this);

        mRecyclerView = findViewById(R.id.recycler_view);
        mAdapter = new PageControlAdapter(context);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }


    public void setPosition(int position) {
        mAdapter.updatePosititon(position);
    }

    public void setIndicatorCount(int size) {
        mAdapter.updateCount(size);
    }

}
