package com.ilomatech.crosstwopageindicator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PageControlAdapter extends RecyclerView.Adapter<PageControlAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition;
    private  int size;

    public PageControlAdapter(Context context)
    {
        this.context = context;
    }

    public void updatePosititon(int selectedPosition)
    {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public void updateCount(int size)
    {
        this.size = size;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PageControlAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_page_control, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PageControlAdapter.MyViewHolder holder, int position) {

        if (position == selectedPosition)
        {
            holder.txtItem.setBackground(ContextCompat.getDrawable(context, R.drawable.selected_control_bg));
        }else
        {
            holder.txtItem.setBackground(ContextCompat.getDrawable(context, R.drawable.deselected_control_bg));
        }

    }

    @Override
    public int getItemCount() {
        return size;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItem;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItem = itemView.findViewById(R.id.item);
        }
    }
}
